<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.5.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.025" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="74xx-us" urn="urn:adsk.eagle:library:88">
<description>&lt;b&gt;TTL Devices, 74xx Series with US Symbols&lt;/b&gt;&lt;p&gt;
Based on the following sources:
&lt;ul&gt;
&lt;li&gt;Texas Instruments &lt;i&gt;TTL Data Book&lt;/i&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Volume 1, 1996.
&lt;li&gt;TTL Data Book, Volume 2 , 1993
&lt;li&gt;National Seminconductor Databook 1990, ALS/LS Logic
&lt;li&gt;ttl 74er digital data dictionary, ECA Electronic + Acustic GmbH, ISBN 3-88109-032-0
&lt;li&gt;http://icmaster.com/ViewCompare.asp
&lt;/ul&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL14" urn="urn:adsk.eagle:footprint:2523/1" library_version="1">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="8.89" y1="2.921" x2="-8.89" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="-2.921" x2="8.89" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="8.89" y1="2.921" x2="8.89" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="2.921" x2="-8.89" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="-2.921" x2="-8.89" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="1.016" x2="-8.89" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-7.62" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-5.08" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="7.62" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="7.62" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-2.54" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="0" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="5.08" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="2.54" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="5.08" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="2.54" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="0" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="-2.54" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-5.08" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-7.62" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-9.271" y="-3.048" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-6.731" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="DIL14" urn="urn:adsk.eagle:package:2921/1" type="box" library_version="1">
<description>Dual In Line Package</description>
</package3d>
</packages3d>
<symbols>
<symbol name="74386" urn="urn:adsk.eagle:symbol:2733/1" library_version="1">
<wire x1="-1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-6.35" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-6.35" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.2446" y1="-5.0678" x2="7.5439" y2="0.0507" width="0.4064" layer="94" curve="60.147106" cap="flat"/>
<wire x1="-1.2446" y1="5.0678" x2="7.5442" y2="-0.0505" width="0.4064" layer="94" curve="-60.148802" cap="flat"/>
<wire x1="-6.604" y1="5.08" x2="-6.604" y2="-5.08" width="0.4064" layer="94" curve="-77.319617" cap="flat"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.4064" layer="94" curve="-77.319617" cap="flat"/>
<text x="-7.62" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="I0" x="-12.7" y="2.54" visible="pad" length="middle" direction="in" swaplevel="1"/>
<pin name="I1" x="-12.7" y="-2.54" visible="pad" length="middle" direction="in" swaplevel="1"/>
<pin name="O" x="12.7" y="0" visible="pad" length="middle" direction="out" rot="R180"/>
</symbol>
<symbol name="PWRN" urn="urn:adsk.eagle:symbol:2522/1" library_version="1">
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-7.62" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="5.08" size="1.27" layer="95" rot="R90">VCC</text>
<pin name="GND" x="0" y="-10.16" visible="pad" direction="pwr" rot="R90"/>
<pin name="VCC" x="0" y="10.16" visible="pad" direction="pwr" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="74*386" urn="urn:adsk.eagle:component:3009/1" prefix="IC" library_version="1">
<description>Quadruple 2-input &lt;b&gt;EXCLUSIVE-OR (XOR)&lt;/b&gt; gate</description>
<gates>
<gate name="A" symbol="74386" x="17.78" y="7.62" swaplevel="1"/>
<gate name="B" symbol="74386" x="17.78" y="-7.62" swaplevel="1"/>
<gate name="C" symbol="74386" x="43.18" y="7.62" swaplevel="1"/>
<gate name="D" symbol="74386" x="43.18" y="-7.62" swaplevel="1"/>
<gate name="P" symbol="PWRN" x="-5.08" y="0" addlevel="request"/>
</gates>
<devices>
<device name="N" package="DIL14">
<connects>
<connect gate="A" pin="I0" pad="1"/>
<connect gate="A" pin="I1" pad="2"/>
<connect gate="A" pin="O" pad="3"/>
<connect gate="B" pin="I0" pad="5"/>
<connect gate="B" pin="I1" pad="6"/>
<connect gate="B" pin="O" pad="4"/>
<connect gate="C" pin="I0" pad="8"/>
<connect gate="C" pin="I1" pad="9"/>
<connect gate="C" pin="O" pad="10"/>
<connect gate="D" pin="I0" pad="12"/>
<connect gate="D" pin="I1" pad="13"/>
<connect gate="D" pin="O" pad="11"/>
<connect gate="P" pin="GND" pad="7"/>
<connect gate="P" pin="VCC" pad="14"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2921/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="IC1" library="74xx-us" library_urn="urn:adsk.eagle:library:88" deviceset="74*386" device="N" package3d_urn="urn:adsk.eagle:package:2921/1"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="13.335" y="92.075" size="3.556" layer="95">+</text>
<wire x1="12.7" y1="100.965" x2="12.7" y2="85.725" width="0.1524" layer="94"/>
<wire x1="12.7" y1="85.725" x2="17.78" y2="88.265" width="0.1524" layer="94"/>
<wire x1="17.78" y1="88.265" x2="17.78" y2="98.425" width="0.1524" layer="94"/>
<wire x1="17.78" y1="98.425" x2="12.7" y2="100.965" width="0.1524" layer="94"/>
<wire x1="26.67" y1="92.075" x2="26.67" y2="85.725" width="0.1524" layer="94"/>
<wire x1="26.67" y1="85.725" x2="39.37" y2="85.725" width="0.1524" layer="94"/>
<wire x1="39.37" y1="85.725" x2="39.37" y2="92.075" width="0.1524" layer="94"/>
<wire x1="39.37" y1="92.075" x2="26.67" y2="92.075" width="0.1524" layer="94"/>
<text x="30.48" y="88.265" size="1.778" layer="95">ADJ6</text>
<wire x1="52.07" y1="90.805" x2="52.07" y2="85.725" width="0.1524" layer="94"/>
<wire x1="52.07" y1="85.725" x2="57.15" y2="85.725" width="0.1524" layer="94"/>
<wire x1="57.15" y1="85.725" x2="57.15" y2="90.805" width="0.1524" layer="94"/>
<wire x1="57.15" y1="90.805" x2="52.07" y2="90.805" width="0.1524" layer="94"/>
<text x="54.61" y="88.265" size="1.778" layer="95" align="center">PC</text>
<wire x1="0" y1="95.25" x2="-15.24" y2="95.25" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="95.25" x2="-12.7" y2="90.17" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="90.17" x2="-2.54" y2="90.17" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="90.17" x2="0" y2="95.25" width="0.1524" layer="94"/>
<wire x1="80.645" y1="93.345" x2="75.565" y2="93.345" width="0.1524" layer="94" curve="-180"/>
<wire x1="75.565" y1="93.345" x2="75.565" y2="97.155" width="0.1524" layer="94"/>
<wire x1="75.565" y1="97.155" x2="80.645" y2="97.155" width="0.1524" layer="94"/>
<wire x1="80.645" y1="97.155" x2="80.645" y2="93.345" width="0.1524" layer="94"/>
<wire x1="89.535" y1="97.155" x2="85.725" y2="97.155" width="0.1524" layer="94" curve="-180"/>
<wire x1="87.63" y1="90.805" x2="85.725" y2="92.71" width="0.1524" layer="94" curve="-90"/>
<wire x1="85.725" y1="92.71" x2="85.725" y2="97.155" width="0.1524" layer="94"/>
<wire x1="87.63" y1="90.805" x2="89.535" y2="92.71" width="0.1524" layer="94" curve="90"/>
<wire x1="89.535" y1="92.71" x2="89.535" y2="97.155" width="0.1524" layer="94"/>
<wire x1="67.945" y1="97.155" x2="69.215" y2="95.25" width="0.1524" layer="94"/>
<wire x1="69.215" y1="95.25" x2="70.485" y2="97.155" width="0.1524" layer="94"/>
<wire x1="70.485" y1="97.155" x2="67.945" y2="97.155" width="0.1524" layer="94"/>
<circle x="69.215" y="94.615" radius="0.635" width="0.1524" layer="94"/>
<wire x1="-90.17" y1="27.94" x2="-90.17" y2="-93.98" width="0.1524" layer="94"/>
<wire x1="-90.17" y1="-93.98" x2="-74.93" y2="-93.98" width="0.1524" layer="94"/>
<wire x1="-74.93" y1="-93.98" x2="-74.93" y2="27.94" width="0.1524" layer="94"/>
<wire x1="-74.93" y1="27.94" x2="-90.17" y2="27.94" width="0.1524" layer="94"/>
<wire x1="-110.49" y1="-66.04" x2="-151.13" y2="-66.04" width="0.1524" layer="94"/>
<wire x1="-151.13" y1="-66.04" x2="-151.13" y2="-93.98" width="0.1524" layer="94"/>
<wire x1="-151.13" y1="-93.98" x2="-110.49" y2="-93.98" width="0.1524" layer="94"/>
<wire x1="-110.49" y1="-93.98" x2="-110.49" y2="-66.04" width="0.1524" layer="94"/>
<text x="-138.43" y="-81.28" size="1.778" layer="95">instruction cache</text>
<text x="-139.7" y="-16.51" size="1.778" layer="95" rot="R90">PC_out</text>
<text x="-118.11" y="-53.34" size="1.778" layer="95">instruction_cache_out</text>
<wire x1="-133.35" y1="0" x2="-143.51" y2="0" width="0.1524" layer="94"/>
<wire x1="-143.51" y1="0" x2="-143.51" y2="10.16" width="0.1524" layer="94"/>
<wire x1="-143.51" y1="10.16" x2="-133.35" y2="10.16" width="0.1524" layer="94"/>
<wire x1="-133.35" y1="10.16" x2="-133.35" y2="0" width="0.1524" layer="94"/>
<text x="-139.7" y="1.27" size="1.778" layer="95">PC
</text>
<wire x1="-171.45" y1="7.62" x2="-171.45" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-171.45" y1="-2.54" x2="-181.61" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-181.61" y1="-2.54" x2="-181.61" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-181.61" y1="7.62" x2="-171.45" y2="7.62" width="0.1524" layer="94"/>
<text x="-177.8" y="-1.27" size="1.778" layer="95">+2
</text>
<wire x1="-153.67" y1="12.7" x2="-153.67" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-153.67" y1="-2.54" x2="-148.59" y2="0" width="0.1524" layer="94"/>
<wire x1="-148.59" y1="0" x2="-148.59" y2="10.16" width="0.1524" layer="94"/>
<wire x1="-148.59" y1="10.16" x2="-153.67" y2="12.7" width="0.1524" layer="94"/>
<text x="-83.82" y="16.51" size="1.778" layer="95">IR
PC</text>
<wire x1="17.78" y1="27.94" x2="17.78" y2="-93.98" width="0.1524" layer="94"/>
<wire x1="17.78" y1="-93.98" x2="33.02" y2="-93.98" width="0.1524" layer="94"/>
<wire x1="33.02" y1="-93.98" x2="33.02" y2="27.94" width="0.1524" layer="94"/>
<wire x1="33.02" y1="27.94" x2="17.78" y2="27.94" width="0.1524" layer="94"/>
<text x="19.05" y="-6.35" size="1.778" layer="95">CTRL WD:
Read
Write
ALUop
WBbyteonly
WB_PC
Opcode
WBLoad
Loadcc
ALU_sel1
ALU_sel2
Stall
STI/LDI</text>
<wire x1="-39.37" y1="-28.575" x2="-13.97" y2="-28.575" width="0.1524" layer="94"/>
<wire x1="-13.97" y1="-28.575" x2="-13.97" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="-13.97" y1="-17.78" x2="-39.37" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="-39.37" y1="-17.78" x2="-39.37" y2="-28.575" width="0.1524" layer="94"/>
<text x="-27.305" y="-22.86" size="1.778" layer="95" align="center">Decode logic</text>
<text x="19.05" y="-22.86" size="1.778" layer="95">PC
Reg1
Reg2
offset16
DR</text>
<wire x1="-39.37" y1="26.67" x2="-39.37" y2="-13.97" width="0.1524" layer="94"/>
<wire x1="-13.97" y1="-13.97" x2="-13.97" y2="26.67" width="0.1524" layer="94"/>
<wire x1="-13.97" y1="26.67" x2="-39.37" y2="26.67" width="0.1524" layer="94"/>
<text x="-26.67" y="6.35" size="2.54" layer="95" align="center">Regfile</text>
<wire x1="-39.37" y1="-13.97" x2="-13.97" y2="-13.97" width="0.1524" layer="94"/>
<text x="-54.61" y="-34.29" size="1.778" layer="95">offset11</text>
<text x="-59.69" y="20.32" size="1.778" layer="95">SR1</text>
<text x="-55.88" y="8.89" size="1.778" layer="95">SR2</text>
<text x="-2.54" y="20.32" size="1.778" layer="95">Reg1</text>
<text x="-2.54" y="10.16" size="1.778" layer="95">Reg2</text>
<wire x1="64.77" y1="12.7" x2="64.77" y2="7.62" width="0.1524" layer="95"/>
<wire x1="64.77" y1="7.62" x2="68.58" y2="5.08" width="0.1524" layer="95"/>
<wire x1="64.77" y1="-5.08" x2="64.77" y2="-10.16" width="0.1524" layer="95"/>
<wire x1="68.58" y1="-2.54" x2="68.58" y2="5.08" width="0.1524" layer="95"/>
<wire x1="64.77" y1="-10.16" x2="73.66" y2="-5.08" width="0.1524" layer="95"/>
<wire x1="64.77" y1="12.7" x2="73.66" y2="7.62" width="0.1524" layer="95"/>
<wire x1="73.66" y1="-5.08" x2="73.66" y2="7.62" width="0.1524" layer="95"/>
<wire x1="64.77" y1="-5.08" x2="68.58" y2="-2.54" width="0.1524" layer="95"/>
<text x="38.1" y="26.67" size="1.778" layer="95">aluop</text>
<wire x1="-49.53" y1="12.7" x2="-49.53" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-49.53" y1="-2.54" x2="-44.45" y2="0" width="0.1524" layer="94"/>
<wire x1="-44.45" y1="0" x2="-44.45" y2="10.16" width="0.1524" layer="94"/>
<wire x1="-44.45" y1="10.16" x2="-49.53" y2="12.7" width="0.1524" layer="94"/>
<text x="-44.45" y="6.35" size="1.778" layer="95">SR2</text>
<text x="-55.88" y="3.81" size="1.778" layer="95">DR</text>
<wire x1="54.61" y1="17.78" x2="54.61" y2="2.54" width="0.1524" layer="94"/>
<wire x1="54.61" y1="2.54" x2="59.69" y2="5.08" width="0.1524" layer="94"/>
<wire x1="59.69" y1="5.08" x2="59.69" y2="15.24" width="0.1524" layer="94"/>
<wire x1="59.69" y1="15.24" x2="54.61" y2="17.78" width="0.1524" layer="94"/>
<wire x1="54.61" y1="0" x2="54.61" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="54.61" y1="-15.24" x2="59.69" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="59.69" y1="-12.7" x2="59.69" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="59.69" y1="-2.54" x2="54.61" y2="0" width="0.1524" layer="94"/>
<text x="41.91" y="6.35" size="1.778" layer="95">PC
</text>
<text x="41.91" y="13.97" size="1.778" layer="95">Reg1</text>
<text x="41.91" y="-3.81" size="1.778" layer="95">Reg2</text>
<text x="41.91" y="-8.89" size="1.778" layer="95">offset16</text>
<text x="-29.21" y="-68.58" size="1.778" layer="95">PC
</text>
<wire x1="-39.37" y1="-43.815" x2="-13.97" y2="-43.815" width="0.1524" layer="94"/>
<wire x1="-13.97" y1="-43.815" x2="-13.97" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="-39.37" y1="-33.02" x2="-39.37" y2="-43.815" width="0.1524" layer="94"/>
<text x="-27.305" y="-38.1" size="1.778" layer="95" align="center">offset magic</text>
<wire x1="-13.97" y1="-33.02" x2="-39.37" y2="-33.02" width="0.1524" layer="94"/>
<text x="-54.61" y="-43.18" size="1.778" layer="95">OPcode</text>
<text x="-2.54" y="-36.83" size="1.778" layer="95">offset16</text>
<wire x1="92.71" y1="27.94" x2="92.71" y2="-93.98" width="0.1524" layer="94"/>
<wire x1="92.71" y1="-93.98" x2="107.95" y2="-93.98" width="0.1524" layer="94"/>
<wire x1="107.95" y1="-93.98" x2="107.95" y2="27.94" width="0.1524" layer="94"/>
<wire x1="107.95" y1="27.94" x2="92.71" y2="27.94" width="0.1524" layer="94"/>
<text x="93.98" y="-20.32" size="1.778" layer="95">PC
DReg1
DReg2
DR</text>
<wire x1="179.07" y1="-29.21" x2="138.43" y2="-29.21" width="0.1524" layer="94"/>
<wire x1="138.43" y1="-29.21" x2="138.43" y2="-57.15" width="0.1524" layer="94"/>
<wire x1="138.43" y1="-57.15" x2="179.07" y2="-57.15" width="0.1524" layer="94"/>
<wire x1="179.07" y1="-57.15" x2="179.07" y2="-29.21" width="0.1524" layer="94"/>
<text x="151.13" y="-44.45" size="1.778" layer="95">data cache</text>
<text x="-54.61" y="-21.59" size="1.778" layer="95">IR</text>
<text x="-1.27" y="-21.59" size="1.778" layer="95">CTRL_WD</text>
<text x="60.96" y="-87.63" size="1.778" layer="95">PC
</text>
<text x="58.42" y="-90.17" size="1.778" layer="95">CTRL_WD</text>
<wire x1="207.01" y1="27.94" x2="207.01" y2="-93.98" width="0.1524" layer="94"/>
<wire x1="207.01" y1="-93.98" x2="222.25" y2="-93.98" width="0.1524" layer="94"/>
<wire x1="222.25" y1="-93.98" x2="222.25" y2="27.94" width="0.1524" layer="94"/>
<wire x1="222.25" y1="27.94" x2="207.01" y2="27.94" width="0.1524" layer="94"/>
<text x="208.28" y="-21.59" size="1.778" layer="95">PC
DReg1
DReg2
DR</text>
<text x="229.87" y="21.59" size="1.778" layer="95">DR</text>
<text x="229.87" y="17.78" size="1.778" layer="95">DReg1</text>
<text x="229.87" y="13.97" size="1.778" layer="95">WBLoad</text>
<text x="148.59" y="-90.17" size="1.778" layer="95">CTRL_WD</text>
<text x="-22.86" y="27.94" size="1.778" layer="95" rot="R90">dest</text>
<text x="-30.48" y="30.48" size="1.778" layer="95" rot="R270">IN</text>
<text x="-36.83" y="33.02" size="1.778" layer="95" rot="R270">Load</text>
<text x="-149.86" y="22.86" size="1.778" layer="95" rot="R270">Load_sel</text>
<text x="39.37" y="21.59" size="1.778" layer="95">ALU_sel1</text>
<text x="39.37" y="-17.78" size="1.778" layer="95">ALU_sel2</text>
<text x="59.69" y="-26.67" size="1.778" layer="95">DReg2</text>
<wire x1="-52.07" y1="-3.81" x2="-52.07" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="-52.07" y1="-8.89" x2="-64.77" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="-64.77" y1="-8.89" x2="-64.77" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-64.77" y1="-3.81" x2="-52.07" y2="-3.81" width="0.1524" layer="94"/>
<text x="-58.42" y="-6.35" size="1.27" layer="95" align="center">op=Store</text>
<wire x1="-21.59" y1="-86.36" x2="-38.1" y2="-86.36" width="0.1524" layer="94"/>
<wire x1="-38.1" y1="-86.36" x2="-38.1" y2="-96.52" width="0.1524" layer="94"/>
<wire x1="-38.1" y1="-96.52" x2="-21.59" y2="-96.52" width="0.1524" layer="94"/>
<wire x1="-21.59" y1="-96.52" x2="-21.59" y2="-86.36" width="0.1524" layer="94"/>
<text x="-30.48" y="-92.71" size="1.778" layer="95">CC</text>
<text x="19.05" y="-25.4" size="1.778" layer="95">CC</text>
<wire x1="-49.53" y1="-44.45" x2="-49.53" y2="-59.69" width="0.1524" layer="94"/>
<wire x1="-49.53" y1="-59.69" x2="-44.45" y2="-57.15" width="0.1524" layer="94"/>
<wire x1="-44.45" y1="-57.15" x2="-44.45" y2="-46.99" width="0.1524" layer="94"/>
<wire x1="-44.45" y1="-46.99" x2="-49.53" y2="-44.45" width="0.1524" layer="94"/>
<text x="-39.37" y="-50.8" size="1.778" layer="95">DR</text>
<wire x1="-52.07" y1="-60.96" x2="-52.07" y2="-66.04" width="0.1524" layer="94"/>
<wire x1="-52.07" y1="-66.04" x2="-64.77" y2="-66.04" width="0.1524" layer="94"/>
<wire x1="-64.77" y1="-66.04" x2="-64.77" y2="-60.96" width="0.1524" layer="94"/>
<wire x1="-64.77" y1="-60.96" x2="-52.07" y2="-60.96" width="0.1524" layer="94"/>
<text x="-58.42" y="-63.5" size="1.27" layer="95" align="center">JSR/JSRR/TRAP</text>
<text x="-58.42" y="-55.88" size="1.27" layer="95" align="center">3'b111</text>
<text x="-67.31" y="-48.26" size="1.778" layer="95">DR from IR</text>
<text x="80.01" y="2.54" size="1.778" layer="95">DReg1</text>
<text x="19.05" y="-27.94" size="1.778" layer="95">NZP</text>
<wire x1="-21.59" y1="-72.39" x2="-38.1" y2="-72.39" width="0.1524" layer="94"/>
<wire x1="-38.1" y1="-72.39" x2="-38.1" y2="-82.55" width="0.1524" layer="94"/>
<wire x1="-38.1" y1="-82.55" x2="-21.59" y2="-82.55" width="0.1524" layer="94"/>
<wire x1="-21.59" y1="-82.55" x2="-21.59" y2="-72.39" width="0.1524" layer="94"/>
<text x="-36.83" y="-78.74" size="1.778" layer="95">opcode-&gt;NZP</text>
<wire x1="73.66" y1="-40.64" x2="57.15" y2="-40.64" width="0.1524" layer="94"/>
<wire x1="57.15" y1="-40.64" x2="57.15" y2="-50.8" width="0.1524" layer="94"/>
<wire x1="57.15" y1="-50.8" x2="73.66" y2="-50.8" width="0.1524" layer="94"/>
<wire x1="73.66" y1="-50.8" x2="73.66" y2="-40.64" width="0.1524" layer="94"/>
<text x="59.69" y="-46.99" size="1.778" layer="95">CC checker</text>
<text x="93.98" y="-22.86" size="1.778" layer="95">BR_EN</text>
<text x="208.28" y="-24.13" size="1.778" layer="95">BR_EN</text>
<text x="43.18" y="-44.45" size="1.778" layer="95">NZP</text>
<text x="43.18" y="-49.53" size="1.778" layer="95">CC</text>
<text x="78.74" y="-45.72" size="1.778" layer="95">BR_EN</text>
<text x="-52.07" y="-76.2" size="1.778" layer="95">IR</text>
<text x="-10.16" y="-91.44" size="1.778" layer="95">CC</text>
<text x="-2.54" y="-77.47" size="1.778" layer="95">NZP</text>
<wire x1="142.24" y1="8.89" x2="142.24" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="142.24" y1="-1.27" x2="152.4" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="152.4" y1="-1.27" x2="152.4" y2="8.89" width="0.1524" layer="94"/>
<wire x1="152.4" y1="8.89" x2="142.24" y2="8.89" width="0.1524" layer="94"/>
<text x="148.59" y="13.97" size="1.27" layer="95">CLK</text>
<text x="153.67" y="5.08" size="1.778" layer="95">Out</text>
<text x="139.7" y="5.08" size="1.27" layer="95">In</text>
<circle x="146.05" y="-6.35" radius="1.27" width="0.1524" layer="94"/>
<wire x1="147.32" y1="-6.35" x2="149.86" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="149.86" y1="-7.62" x2="149.86" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="149.86" y1="-5.08" x2="147.32" y2="-6.35" width="0.1524" layer="94"/>
<text x="166.37" y="-82.55" size="1.778" layer="95">PC
DReg1
DReg2
DR</text>
<text x="166.37" y="-85.09" size="1.778" layer="95">BR_EN</text>
<text x="93.98" y="-6.35" size="1.778" layer="95">CTRL WD:
Read
Write
ALUop
WBbyteonly
WB_PC
Opcode
WBLoad
Loadcc
ALU_sel1
ALU_sel2
Stall
LDI/nSTI</text>
<text x="208.28" y="-6.35" size="1.778" layer="95">CTRL WD:
Read
Write
ALUop
WBbyteonly
WB_PC
Opcode
WBLoad
Loadcc
ALU_sel1
ALU_sel2
Stall
STI/LDI</text>
<wire x1="121.92" y1="-11.43" x2="121.92" y2="-26.67" width="0.1524" layer="94"/>
<wire x1="121.92" y1="-26.67" x2="127" y2="-24.13" width="0.1524" layer="94"/>
<wire x1="127" y1="-24.13" x2="127" y2="-13.97" width="0.1524" layer="94"/>
<wire x1="127" y1="-13.97" x2="121.92" y2="-11.43" width="0.1524" layer="94"/>
<wire x1="121.92" y1="-29.21" x2="121.92" y2="-44.45" width="0.1524" layer="94"/>
<wire x1="121.92" y1="-44.45" x2="127" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="127" y1="-41.91" x2="127" y2="-31.75" width="0.1524" layer="94"/>
<wire x1="127" y1="-31.75" x2="121.92" y2="-29.21" width="0.1524" layer="94"/>
<circle x="115.57" y="-39.37" radius="1.27" width="0.1524" layer="95"/>
<wire x1="114.3" y1="-39.37" x2="111.76" y2="-38.1" width="0.1524" layer="95"/>
<wire x1="111.76" y1="-38.1" x2="111.76" y2="-40.64" width="0.1524" layer="95"/>
<wire x1="111.76" y1="-40.64" x2="114.3" y2="-39.37" width="0.1524" layer="95"/>
<text x="133.35" y="-19.05" size="1.778" layer="95">Read</text>
<text x="115.57" y="-15.24" size="1.778" layer="95">Read</text>
<text x="115.57" y="-34.29" size="1.778" layer="95">Write</text>
<text x="132.08" y="-36.83" size="1.778" layer="95">Write</text>
<text x="143.51" y="3.81" size="1.27" layer="95">T Flip Flop</text>
<text x="110.49" y="-20.32" size="1.778" layer="95">LDI/nSTI</text>
<text x="125.73" y="15.24" size="1.778" layer="95">Stall</text>
<text x="116.84" y="-46.99" size="1.778" layer="95">DReg2=inData</text>
<text x="115.57" y="-53.34" size="1.778" layer="95">DReg1\</text>
<wire x1="175.26" y1="-58.42" x2="175.26" y2="-73.66" width="0.1524" layer="94"/>
<wire x1="175.26" y1="-73.66" x2="180.34" y2="-71.12" width="0.1524" layer="94"/>
<wire x1="180.34" y1="-71.12" x2="180.34" y2="-60.96" width="0.1524" layer="94"/>
<wire x1="180.34" y1="-60.96" x2="175.26" y2="-58.42" width="0.1524" layer="94"/>
<text x="162.56" y="-63.5" size="1.778" layer="95">Data_out</text>
<text x="162.56" y="-68.58" size="1.778" layer="95">DReg1</text>
<text x="187.96" y="-66.04" size="1.778" layer="95">DReg1</text>
<text x="166.37" y="36.83" size="1.778" layer="95">Load</text>
<wire x1="245.745" y1="-6.985" x2="245.745" y2="-12.065" width="0.1524" layer="94" curve="-180"/>
<wire x1="245.745" y1="-12.065" x2="241.935" y2="-12.065" width="0.1524" layer="94"/>
<wire x1="241.935" y1="-12.065" x2="241.935" y2="-6.985" width="0.1524" layer="94"/>
<wire x1="241.935" y1="-6.985" x2="245.745" y2="-6.985" width="0.1524" layer="94"/>
<text x="228.854" y="-13.208" size="1.778" layer="95">BR_EN</text>
<text x="228.854" y="-7.62" size="1.778" layer="95">WB_PC</text>
<text x="249.682" y="-12.192" size="1.778" layer="95">Load_sel</text>
<wire x1="242.824" y1="-17.78" x2="242.824" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="242.824" y1="-33.02" x2="247.904" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="247.904" y1="-30.48" x2="247.904" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="247.904" y1="-20.32" x2="242.824" y2="-17.78" width="0.1524" layer="94"/>
<text x="232.41" y="-30.48" size="1.778" layer="95">PC</text>
<text x="231.14" y="-21.59" size="1.778" layer="95">DReg1</text>
<wire x1="265.43" y1="-20.32" x2="252.73" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="252.73" y1="-20.32" x2="252.73" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="252.73" y1="-30.48" x2="265.43" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="265.43" y1="-30.48" x2="265.43" y2="-20.32" width="0.1524" layer="94"/>
<text x="254" y="-26.67" size="1.778" layer="95">Byte_sel</text>
<text x="238.125" y="-39.37" size="1.778" layer="95">Addr, WBbyteonly</text>
<polygon width="0.1524" layer="94">
<vertex x="147.32" y="-6.35"/>
<vertex x="149.86" y="-5.08"/>
<vertex x="149.86" y="-7.62"/>
</polygon>
<circle x="167.64" y="31.75" radius="1.27" width="0.1524" layer="94"/>
<text x="-10.16" y="-106.68" size="1.778" layer="95">Loadcc</text>
<wire x1="255.27" y1="-109.855" x2="238.76" y2="-109.855" width="0.1524" layer="94"/>
<wire x1="238.76" y1="-109.855" x2="238.76" y2="-120.015" width="0.1524" layer="94"/>
<wire x1="238.76" y1="-120.015" x2="255.27" y2="-120.015" width="0.1524" layer="94"/>
<wire x1="255.27" y1="-120.015" x2="255.27" y2="-109.855" width="0.1524" layer="94"/>
<text x="243.205" y="-116.205" size="1.778" layer="95">GENCC</text>
<wire x1="235.585" y1="-32.385" x2="235.585" y2="-37.465" width="0.1524" layer="94" curve="-180"/>
<wire x1="235.585" y1="-37.465" x2="231.775" y2="-37.465" width="0.1524" layer="94"/>
<wire x1="231.775" y1="-37.465" x2="231.775" y2="-32.385" width="0.1524" layer="94"/>
<wire x1="231.775" y1="-32.385" x2="235.585" y2="-32.385" width="0.1524" layer="94"/>
<text x="222.25" y="-33.02" size="1.778" layer="95">BWLoad</text>
<text x="222.25" y="-38.1" size="1.778" layer="95">WB_PC</text>
<text x="203.835" y="-106.68" size="1.778" layer="95">Loadcc</text>
<text x="-31.75" y="-93.98" size="1.778" layer="95" rot="R180">IN</text>
<text x="-24.13" y="-93.98" size="1.778" layer="95" rot="R180">Load</text>
<text x="-24.13" y="-89.535" size="1.778" layer="95" rot="R270">Out</text>
<wire x1="114.935" y1="-55.88" x2="114.935" y2="-62.23" width="0.1524" layer="94"/>
<wire x1="114.935" y1="-62.23" x2="122.555" y2="-62.23" width="0.1524" layer="94"/>
<wire x1="122.555" y1="-62.23" x2="122.555" y2="-55.88" width="0.1524" layer="94"/>
<wire x1="122.555" y1="-55.88" x2="114.935" y2="-55.88" width="0.1524" layer="94"/>
<text x="116.84" y="-59.69" size="1.27" layer="95">MAR</text>
<wire x1="127.635" y1="-48.895" x2="127.635" y2="-64.135" width="0.1524" layer="94"/>
<wire x1="127.635" y1="-64.135" x2="132.715" y2="-61.595" width="0.1524" layer="94"/>
<wire x1="132.715" y1="-61.595" x2="132.715" y2="-51.435" width="0.1524" layer="94"/>
<wire x1="132.715" y1="-51.435" x2="127.635" y2="-48.895" width="0.1524" layer="94"/>
<text x="133.35" y="-55.88" size="1.778" layer="95">Addr</text>
<text x="118.11" y="-64.77" size="1.778" layer="95">IN</text>
<text x="127" y="-57.15" size="1.27" layer="95" rot="R180">OUT</text>
<text x="114.3" y="-57.15" size="0.762" layer="95" rot="R180">STALL=load</text>
<text x="111.76" y="-74.93" size="1.778" layer="95">DReg1[0]</text>
<text x="111.76" y="-80.01" size="1.778" layer="95">WBbyteonly</text>
<wire x1="127" y1="-72.39" x2="127" y2="-82.55" width="0.1524" layer="94"/>
<wire x1="127" y1="-82.55" x2="140.97" y2="-82.55" width="0.1524" layer="94"/>
<wire x1="140.97" y1="-82.55" x2="140.97" y2="-72.39" width="0.1524" layer="94"/>
<wire x1="140.97" y1="-72.39" x2="127" y2="-72.39" width="0.1524" layer="94"/>
<text x="144.78" y="-76.2" size="1.778" layer="95">MemEN</text>
<text x="128.27" y="-77.47" size="1.778" layer="95">MEM_logic</text>
<text x="-135.89" y="11.43" size="1.778" layer="95" rot="R90">Load</text>
</plain>
<instances>
<instance part="IC1" gate="A" x="167.64" y="22.86" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$5" class="0">
<segment>
<wire x1="12.7" y1="95.885" x2="7.62" y2="95.885" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="12.7" y1="90.805" x2="7.62" y2="90.805" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="17.78" y1="93.345" x2="22.86" y2="93.345" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<wire x1="54.61" y1="90.805" x2="54.61" y2="94.615" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<wire x1="-5.08" y1="95.25" x2="-5.08" y2="100.33" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<wire x1="-10.16" y1="95.25" x2="-10.16" y2="100.33" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<wire x1="-7.62" y1="90.17" x2="-7.62" y2="85.09" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<wire x1="86.36" y1="95.885" x2="86.36" y2="98.425" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<wire x1="88.9" y1="95.885" x2="88.9" y2="98.425" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<wire x1="76.835" y1="97.155" x2="76.835" y2="99.695" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<wire x1="79.375" y1="97.155" x2="79.375" y2="99.695" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<wire x1="69.215" y1="97.155" x2="69.215" y2="100.965" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<wire x1="69.215" y1="93.98" x2="69.215" y2="90.805" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<wire x1="-130.81" y1="-66.04" x2="-130.81" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-130.81" y1="-55.88" x2="-90.17" y2="-55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<wire x1="-190.5" y1="2.54" x2="-190.5" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-138.43" y1="-7.62" x2="-138.43" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="-138.43" y1="-7.62" x2="-138.43" y2="0" width="0.1524" layer="91"/>
<junction x="-138.43" y="-7.62"/>
<wire x1="-190.5" y1="-7.62" x2="-138.43" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="2.54" x2="-181.61" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<wire x1="-153.67" y1="2.54" x2="-157.48" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="2.54" x2="-171.45" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="2.54" x2="-157.48" y2="-3.81" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="-3.81" x2="-90.17" y2="-3.81" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<wire x1="-148.59" y1="5.08" x2="-143.51" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<wire x1="-39.37" y1="-22.86" x2="-74.93" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<wire x1="-13.97" y1="-22.86" x2="17.78" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<wire x1="-74.93" y1="19.05" x2="-39.37" y2="19.05" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<wire x1="-13.97" y1="19.05" x2="17.78" y2="19.05" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<wire x1="-13.97" y1="8.89" x2="17.78" y2="8.89" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$180" class="0">
<segment>
<wire x1="71.12" y1="8.89" x2="71.12" y2="25.4" width="0.1524" layer="91"/>
<wire x1="71.12" y1="25.4" x2="33.02" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$181" class="0">
<segment>
<wire x1="73.66" y1="1.27" x2="92.71" y2="1.27" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$182" class="0">
<segment>
<wire x1="-49.53" y1="7.62" x2="-74.93" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$183" class="0">
<segment>
<wire x1="-49.53" y1="2.54" x2="-74.93" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$184" class="0">
<segment>
<wire x1="-44.45" y1="5.08" x2="-39.37" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<wire x1="-46.99" y1="-1.27" x2="-46.99" y2="-6.35" width="0.1524" layer="91"/>
<wire x1="-46.99" y1="-6.35" x2="-52.07" y2="-6.35" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$185" class="0">
<segment>
<wire x1="54.61" y1="12.7" x2="33.02" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$186" class="0">
<segment>
<wire x1="54.61" y1="7.62" x2="33.02" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$187" class="0">
<segment>
<wire x1="59.69" y1="10.16" x2="64.77" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$188" class="0">
<segment>
<wire x1="54.61" y1="-5.08" x2="33.02" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$189" class="0">
<segment>
<wire x1="54.61" y1="-10.16" x2="33.02" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$190" class="0">
<segment>
<wire x1="59.69" y1="-7.62" x2="64.77" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<wire x1="-74.93" y1="-67.31" x2="17.78" y2="-67.31" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-67.31" x2="17.78" y2="-68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$191" class="0">
<segment>
<wire x1="-39.37" y1="-35.56" x2="-74.93" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$192" class="0">
<segment>
<wire x1="-13.97" y1="-38.1" x2="17.78" y2="-38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$193" class="0">
<segment>
<wire x1="-39.37" y1="-40.64" x2="-74.93" y2="-40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$194" class="0">
<segment>
<wire x1="57.15" y1="16.51" x2="57.15" y2="20.32" width="0.1524" layer="91"/>
<wire x1="57.15" y1="20.32" x2="33.02" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$195" class="0">
<segment>
<wire x1="57.15" y1="-13.97" x2="57.15" y2="-19.05" width="0.1524" layer="91"/>
<wire x1="57.15" y1="-19.05" x2="33.02" y2="-19.05" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$196" class="0">
<segment>
<wire x1="33.02" y1="-86.36" x2="92.71" y2="-86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$197" class="0">
<segment>
<wire x1="33.02" y1="-91.44" x2="92.71" y2="-91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$199" class="0">
<segment>
<wire x1="-27.94" y1="26.67" x2="-27.94" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="45.72" x2="269.24" y2="45.72" width="0.1524" layer="91"/>
<wire x1="269.24" y1="-25.4" x2="269.24" y2="45.72" width="0.1524" layer="91"/>
<wire x1="269.24" y1="-25.4" x2="265.43" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-153.67" y1="7.62" x2="-158.75" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-158.75" y1="66.04" x2="-158.75" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-158.75" y1="66.04" x2="276.86" y2="66.04" width="0.1524" layer="91"/>
<wire x1="276.86" y1="66.04" x2="276.86" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="276.86" y1="-25.4" x2="276.86" y2="-114.935" width="0.1524" layer="91"/>
<wire x1="276.86" y1="-114.935" x2="255.27" y2="-114.935" width="0.1524" layer="91"/>
<wire x1="269.24" y1="-25.4" x2="276.86" y2="-25.4" width="0.1524" layer="91"/>
<junction x="269.24" y="-25.4"/>
<junction x="276.86" y="-25.4"/>
</segment>
</net>
<net name="N$200" class="0">
<segment>
<wire x1="-21.59" y1="26.67" x2="-21.59" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-21.59" y1="40.64" x2="240.03" y2="40.64" width="0.1524" layer="91"/>
<wire x1="240.03" y1="40.64" x2="240.03" y2="20.32" width="0.1524" layer="91"/>
<wire x1="240.03" y1="20.32" x2="222.25" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<wire x1="-151.13" y1="60.96" x2="-151.13" y2="11.43" width="0.1524" layer="91"/>
<wire x1="-151.13" y1="60.96" x2="255.27" y2="60.96" width="0.1524" layer="91"/>
<wire x1="255.27" y1="60.96" x2="255.27" y2="-9.652" width="0.1524" layer="91"/>
<wire x1="248.412" y1="-9.652" x2="255.27" y2="-9.652" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$201" class="0">
<segment>
<wire x1="250.19" y1="57.15" x2="-34.29" y2="57.15" width="0.1524" layer="91"/>
<wire x1="-34.29" y1="57.15" x2="-34.29" y2="26.67" width="0.1524" layer="91"/>
<wire x1="250.19" y1="12.7" x2="222.25" y2="12.7" width="0.1524" layer="91"/>
<wire x1="250.19" y1="57.15" x2="250.19" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<wire x1="107.95" y1="-91.44" x2="207.01" y2="-91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$202" class="0">
<segment>
<wire x1="107.95" y1="-86.36" x2="207.01" y2="-86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$203" class="0">
<segment>
<wire x1="33.02" y1="-27.94" x2="92.71" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$204" class="0">
<segment>
<wire x1="-64.77" y1="-6.35" x2="-74.93" y2="-6.35" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$205" class="0">
<segment>
<wire x1="-21.59" y1="-91.44" x2="17.78" y2="-91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$206" class="0">
<segment>
<wire x1="-49.53" y1="-49.53" x2="-74.93" y2="-49.53" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$208" class="0">
<segment>
<wire x1="-44.45" y1="-52.07" x2="17.78" y2="-52.07" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$209" class="0">
<segment>
<wire x1="-46.99" y1="-58.42" x2="-46.99" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="-46.99" y1="-63.5" x2="-52.07" y2="-63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$211" class="0">
<segment>
<wire x1="-64.77" y1="-63.5" x2="-74.93" y2="-63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$207" class="0">
<segment>
<wire x1="-54.61" y1="-55.88" x2="-49.53" y2="-55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$212" class="0">
<segment>
<wire x1="-21.59" y1="-77.47" x2="17.78" y2="-77.47" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$213" class="0">
<segment>
<wire x1="-38.1" y1="-77.47" x2="-74.93" y2="-77.47" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$214" class="0">
<segment>
<wire x1="33.02" y1="-45.72" x2="33.02" y2="-46.99" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-46.99" x2="57.15" y2="-46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$215" class="0">
<segment>
<wire x1="33.02" y1="-43.18" x2="33.02" y2="-44.45" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-44.45" x2="57.15" y2="-44.45" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$216" class="0">
<segment>
<wire x1="73.66" y1="-45.72" x2="92.71" y2="-45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$217" class="0">
<segment>
<wire x1="146.05" y1="8.89" x2="146.05" y2="13.97" width="0.1524" layer="91"/>
<wire x1="146.05" y1="13.97" x2="107.95" y2="13.97" width="0.1524" layer="91"/>
<pinref part="IC1" gate="A" pin="I0"/>
<wire x1="165.1" y1="10.16" x2="156.21" y2="10.16" width="0.1524" layer="91"/>
<wire x1="156.21" y1="10.16" x2="156.21" y2="17.78" width="0.1524" layer="91"/>
<wire x1="156.21" y1="17.78" x2="146.05" y2="17.78" width="0.1524" layer="91"/>
<wire x1="146.05" y1="17.78" x2="146.05" y2="13.97" width="0.1524" layer="91"/>
<junction x="146.05" y="13.97"/>
</segment>
</net>
<net name="N$218" class="0">
<segment>
<wire x1="149.86" y1="8.89" x2="149.86" y2="13.97" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$219" class="0">
<segment>
<wire x1="142.24" y1="3.81" x2="137.16" y2="3.81" width="0.1524" layer="91"/>
<wire x1="137.16" y1="3.81" x2="137.16" y2="-6.35" width="0.1524" layer="91"/>
<wire x1="137.16" y1="-6.35" x2="144.78" y2="-6.35" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$221" class="0">
<segment>
<wire x1="212.09" y1="27.94" x2="212.09" y2="35.56" width="0.1524" layer="91"/>
<wire x1="212.09" y1="35.56" x2="167.64" y2="35.56" width="0.1524" layer="91"/>
<wire x1="167.64" y1="35.56" x2="102.87" y2="35.56" width="0.1524" layer="91"/>
<wire x1="102.87" y1="35.56" x2="102.87" y2="27.94" width="0.1524" layer="91"/>
<wire x1="102.87" y1="35.56" x2="27.94" y2="35.56" width="0.1524" layer="91"/>
<wire x1="27.94" y1="35.56" x2="27.94" y2="27.94" width="0.1524" layer="91"/>
<junction x="102.87" y="35.56"/>
<wire x1="27.94" y1="35.56" x2="-78.74" y2="35.56" width="0.1524" layer="91"/>
<junction x="27.94" y="35.56"/>
<wire x1="-78.74" y1="35.56" x2="-78.74" y2="27.94" width="0.1524" layer="91"/>
<pinref part="IC1" gate="A" pin="O"/>
<junction x="167.64" y="35.56"/>
<wire x1="-78.74" y1="35.56" x2="-138.43" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-138.43" y1="35.56" x2="-138.43" y2="10.16" width="0.1524" layer="91"/>
<junction x="-78.74" y="35.56"/>
</segment>
</net>
<net name="N$222" class="0">
<segment>
<wire x1="121.92" y1="-16.51" x2="107.95" y2="-16.51" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$223" class="0">
<segment>
<wire x1="121.92" y1="-21.59" x2="110.49" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="110.49" y1="-21.59" x2="107.95" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-39.37" x2="110.49" y2="-39.37" width="0.1524" layer="91"/>
<wire x1="110.49" y1="-39.37" x2="110.49" y2="-21.59" width="0.1524" layer="91"/>
<junction x="110.49" y="-21.59"/>
</segment>
</net>
<net name="N$224" class="0">
<segment>
<wire x1="127" y1="-19.05" x2="149.86" y2="-19.05" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-19.05" x2="149.86" y2="-29.21" width="0.1524" layer="91"/>
<wire x1="177.8" y1="-59.69" x2="177.8" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="177.8" y1="-58.42" x2="185.42" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-58.42" x2="185.42" y2="-19.05" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-19.05" x2="149.86" y2="-19.05" width="0.1524" layer="91"/>
<junction x="149.86" y="-19.05"/>
</segment>
</net>
<net name="N$225" class="0">
<segment>
<wire x1="121.92" y1="-34.29" x2="107.95" y2="-34.29" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$226" class="0">
<segment>
<wire x1="121.92" y1="-39.37" x2="116.84" y2="-39.37" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$227" class="0">
<segment>
<wire x1="127" y1="-36.83" x2="138.43" y2="-36.83" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$228" class="0">
<segment>
<wire x1="107.95" y1="-48.26" x2="138.43" y2="-48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$231" class="0">
<segment>
<wire x1="130.81" y1="-27.94" x2="124.46" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="124.46" y1="-27.94" x2="124.46" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="152.4" y1="3.81" x2="157.48" y2="3.81" width="0.1524" layer="91"/>
<wire x1="157.48" y1="3.81" x2="157.48" y2="-6.35" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-6.35" x2="157.48" y2="-6.35" width="0.1524" layer="91"/>
<wire x1="124.46" y1="-12.7" x2="124.46" y2="-8.89" width="0.1524" layer="91"/>
<wire x1="124.46" y1="-8.89" x2="130.81" y2="-8.89" width="0.1524" layer="91"/>
<wire x1="130.81" y1="-8.89" x2="157.48" y2="-8.89" width="0.1524" layer="91"/>
<wire x1="157.48" y1="-8.89" x2="157.48" y2="-6.35" width="0.1524" layer="91"/>
<junction x="157.48" y="-6.35"/>
<wire x1="157.48" y1="-6.35" x2="170.18" y2="-6.35" width="0.1524" layer="91"/>
<pinref part="IC1" gate="A" pin="I1"/>
<wire x1="170.18" y1="-6.35" x2="170.18" y2="10.16" width="0.1524" layer="91"/>
<wire x1="130.81" y1="-27.94" x2="130.81" y2="-8.89" width="0.1524" layer="91"/>
<junction x="130.81" y="-8.89"/>
<wire x1="130.81" y1="-27.94" x2="130.81" y2="-50.4952" width="0.1524" layer="91"/>
<junction x="130.81" y="-27.94"/>
</segment>
</net>
<net name="N$220" class="0">
<segment>
<wire x1="175.26" y1="-63.5" x2="158.75" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-63.5" x2="158.75" y2="-57.15" width="0.1524" layer="91"/>
<wire x1="118.11" y1="-62.23" x2="118.11" y2="-64.77" width="0.1524" layer="91"/>
<wire x1="118.11" y1="-64.77" x2="158.75" y2="-64.77" width="0.1524" layer="91"/>
<wire x1="158.75" y1="-64.77" x2="158.75" y2="-63.5" width="0.1524" layer="91"/>
<junction x="158.75" y="-63.5"/>
</segment>
</net>
<net name="N$230" class="0">
<segment>
<wire x1="175.26" y1="-68.58" x2="107.95" y2="-68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$232" class="0">
<segment>
<wire x1="180.34" y1="-66.04" x2="207.01" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$233" class="0">
<segment>
<wire x1="241.808" y1="-8.128" x2="222.25" y2="-8.128" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$198" class="0">
<segment>
<wire x1="241.808" y1="-10.922" x2="222.25" y2="-10.922" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$235" class="0">
<segment>
<wire x1="242.824" y1="-27.94" x2="222.25" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$237" class="0">
<segment>
<wire x1="247.65" y1="-25.4" x2="252.73" y2="-25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$238" class="0">
<segment>
<wire x1="259.08" y1="-30.48" x2="259.08" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="259.08" y1="-40.64" x2="222.25" y2="-40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$234" class="0">
<segment>
<wire x1="-33.02" y1="-96.52" x2="-33.02" y2="-106.045" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="-106.045" x2="-33.02" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="238.76" y1="-114.935" x2="-33.02" y2="-114.935" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="-114.935" x2="-33.02" y2="-106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$239" class="0">
<segment>
<wire x1="238.252" y1="-35.052" x2="245.11" y2="-35.052" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$241" class="0">
<segment>
<wire x1="231.775" y1="-33.655" x2="222.25" y2="-33.655" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$242" class="0">
<segment>
<wire x1="231.775" y1="-36.195" x2="222.25" y2="-36.195" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$243" class="0">
<segment>
<wire x1="245.11" y1="-34.925" x2="245.11" y2="-31.75" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$240" class="0">
<segment>
<wire x1="-26.67" y1="-96.52" x2="-26.67" y2="-107.315" width="0.1524" layer="91"/>
<wire x1="-26.67" y1="-107.315" x2="214.63" y2="-107.315" width="0.1524" layer="91"/>
<wire x1="214.63" y1="-107.315" x2="214.63" y2="-93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$244" class="0">
<segment>
<wire x1="242.824" y1="-22.86" x2="222.25" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="222.25" y1="-22.86" x2="222.25" y2="-22.606" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$229" class="0">
<segment>
<wire x1="127.635" y1="-53.975" x2="107.95" y2="-53.975" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$236" class="0">
<segment>
<wire x1="127.635" y1="-59.055" x2="122.555" y2="-59.055" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$245" class="0">
<segment>
<wire x1="132.715" y1="-56.515" x2="138.43" y2="-56.515" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$246" class="0">
<segment>
<wire x1="114.935" y1="-59.055" x2="107.95" y2="-59.055" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$248" class="0">
<segment>
<wire x1="140.97" y1="-77.47" x2="156.21" y2="-77.47" width="0.1524" layer="91"/>
<wire x1="156.21" y1="-77.47" x2="156.21" y2="-57.15" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$249" class="0">
<segment>
<wire x1="127" y1="-74.93" x2="107.95" y2="-74.93" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$250" class="0">
<segment>
<wire x1="127" y1="-80.01" x2="107.95" y2="-80.01" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="33.02" y="71.12" size="2.032" layer="95" align="center">Ready</text>
<circle x="33.02" y="71.12" radius="17.78" width="0.1524" layer="94"/>
<text x="33.02" y="15.24" size="2.032" layer="95" align="center">Icache_miss</text>
<circle x="33.02" y="15.24" radius="17.78" width="0.1524" layer="94"/>
<text x="91.44" y="43.18" size="2.032" layer="95" align="center">Dache_miss</text>
<circle x="91.44" y="43.18" radius="17.78" width="0.1524" layer="94"/>
<wire x1="33.02" y1="53.34" x2="33.02" y2="33.02" width="0.1524" layer="94"/>
<wire x1="38.1" y1="38.1" x2="33.02" y2="33.02" width="0.1524" layer="94"/>
<wire x1="33.02" y1="33.02" x2="27.94" y2="38.1" width="0.1524" layer="94"/>
<wire x1="46.99" y1="59.69" x2="74.93" y2="49.53" width="0.1524" layer="94"/>
<wire x1="73.66" y1="53.34" x2="74.93" y2="49.53" width="0.1524" layer="94"/>
<text x="35.56" y="44.45" size="1.778" layer="95" rot="R90" align="center">Icache_strobe</text>
<text x="58.42" y="52.07" size="1.778" layer="95" rot="R180" align="center">nIcache_strobe &amp; Dcache_strobe</text>
<text x="66.04" y="63.5" size="1.778" layer="95" align="center">WB_L2_ACK</text>
<text x="60.96" y="93.98" size="3.81" layer="95" align="center">Split cache arbitrations</text>
<wire x1="78.74" y1="55.88" x2="49.53" y2="64.77" width="0.1524" layer="94"/>
<wire x1="49.53" y1="64.77" x2="50.8" y2="60.96" width="0.1524" layer="94"/>
<wire x1="49.53" y1="64.77" x2="53.34" y2="66.04" width="0.1524" layer="94"/>
<wire x1="74.93" y1="49.53" x2="71.12" y2="48.26" width="0.1524" layer="94"/>
<wire x1="26.67" y1="31.75" x2="26.67" y2="54.61" width="0.1524" layer="94"/>
<wire x1="21.59" y1="49.53" x2="26.67" y2="54.61" width="0.1524" layer="94"/>
<wire x1="26.67" y1="54.61" x2="31.75" y2="49.53" width="0.1524" layer="94"/>
<text x="24.13" y="44.45" size="1.778" layer="95" rot="R90" align="center">WB_L2_ACK</text>
<text x="26.67" y="63.5" size="1.524" layer="95">Strobe=0
Control_sel=0</text>
<text x="25.4" y="7.62" size="1.524" layer="95">Strobe=0
Control_sel=0</text>
<text x="83.82" y="35.56" size="1.524" layer="95">Strobe=0
Control_sel=1</text>
</plain>
<instances>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
<sheet>
<plain>
<wire x1="160.02" y1="54.61" x2="119.38" y2="54.61" width="0.1524" layer="94"/>
<wire x1="119.38" y1="54.61" x2="119.38" y2="26.67" width="0.1524" layer="94"/>
<wire x1="119.38" y1="26.67" x2="160.02" y2="26.67" width="0.1524" layer="94"/>
<wire x1="160.02" y1="26.67" x2="160.02" y2="54.61" width="0.1524" layer="94"/>
<text x="132.08" y="39.37" size="1.778" layer="95">instruction cache</text>
<wire x1="219.71" y1="54.61" x2="179.07" y2="54.61" width="0.1524" layer="94"/>
<wire x1="179.07" y1="54.61" x2="179.07" y2="26.67" width="0.1524" layer="94"/>
<wire x1="179.07" y1="26.67" x2="219.71" y2="26.67" width="0.1524" layer="94"/>
<wire x1="219.71" y1="26.67" x2="219.71" y2="54.61" width="0.1524" layer="94"/>
<text x="191.77" y="39.37" size="1.778" layer="95">data cache</text>
<wire x1="177.8" y1="-5.08" x2="162.56" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="162.56" y1="-5.08" x2="165.1" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="165.1" y1="-10.16" x2="175.26" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="175.26" y1="-10.16" x2="177.8" y2="-5.08" width="0.1524" layer="94"/>
<text x="148.59" y="-8.89" size="1.524" layer="95">Control_sel</text>
<text x="166.37" y="-7.62" size="1.524" layer="95"> 0      1</text>
<wire x1="190.5" y1="-20.32" x2="149.86" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="149.86" y1="-20.32" x2="149.86" y2="-48.26" width="0.1524" layer="94"/>
<wire x1="190.5" y1="-48.26" x2="190.5" y2="-20.32" width="0.1524" layer="94"/>
<text x="165.1" y="-35.56" size="1.778" layer="95">L2_cache</text>
<wire x1="190.5" y1="-48.26" x2="149.86" y2="-48.26" width="0.1524" layer="94"/>
<wire x1="190.5" y1="-58.42" x2="149.86" y2="-58.42" width="0.1524" layer="94"/>
<wire x1="149.86" y1="-58.42" x2="149.86" y2="-86.36" width="0.1524" layer="94"/>
<wire x1="190.5" y1="-86.36" x2="190.5" y2="-58.42" width="0.1524" layer="94"/>
<text x="166.37" y="-73.66" size="1.778" layer="95">DRAM</text>
<wire x1="190.5" y1="-86.36" x2="149.86" y2="-86.36" width="0.1524" layer="94"/>
<text x="171.45" y="-54.61" size="1.524" layer="95">WB_DRAM</text>
<text x="172.72" y="-16.51" size="1.524" layer="95">WB_L2</text>
<text x="177.8" y="1.27" size="1.524" layer="95">WB.master_Dcache</text>
<text x="143.51" y="1.27" size="1.524" layer="95">WB.master_Icache</text>
<wire x1="184.15" y1="-7.62" x2="199.39" y2="-7.62" width="0.1524" layer="95"/>
<wire x1="184.15" y1="-7.62" x2="187.96" y2="-11.43" width="0.1524" layer="95"/>
<wire x1="184.15" y1="-7.62" x2="187.96" y2="-3.81" width="0.1524" layer="95"/>
<text x="200.66" y="-8.89" size="1.524" layer="95">Encoder+Mux</text>
<text x="262.89" y="3.81" size="2.032" layer="95" align="center">Ready</text>
<circle x="264.16" y="0" radius="17.78" width="0.1524" layer="94"/>
<text x="264.16" y="-54.61" size="2.032" layer="95" align="center">Icache_miss</text>
<circle x="264.16" y="-55.88" radius="17.78" width="0.1524" layer="94"/>
<text x="337.82" y="2.54" size="2.032" layer="95" align="center">Dache_miss</text>
<circle x="337.82" y="0" radius="17.78" width="0.1524" layer="94"/>
<wire x1="264.16" y1="-17.78" x2="264.16" y2="-38.1" width="0.1524" layer="94"/>
<wire x1="269.24" y1="-33.02" x2="264.16" y2="-38.1" width="0.1524" layer="94"/>
<wire x1="264.16" y1="-38.1" x2="259.08" y2="-33.02" width="0.1524" layer="94"/>
<text x="266.7" y="-26.67" size="1.778" layer="95" rot="R90" align="center">Icache_strobe</text>
<text x="299.72" y="-5.08" size="1.778" layer="95" align="center">Dcache_strobe &amp; !Icache_strobe</text>
<text x="300.99" y="8.89" size="1.778" layer="95" align="center">WB_L2_ACK</text>
<text x="292.1" y="22.86" size="3.81" layer="95" align="center">Split cache arbitrations states</text>
<wire x1="257.81" y1="-39.37" x2="257.81" y2="-16.51" width="0.1524" layer="94"/>
<wire x1="252.73" y1="-21.59" x2="257.81" y2="-16.51" width="0.1524" layer="94"/>
<wire x1="257.81" y1="-16.51" x2="262.89" y2="-21.59" width="0.1524" layer="94"/>
<text x="255.27" y="-26.67" size="1.778" layer="95" rot="R90" align="center">WB_L2_ACK</text>
<text x="256.54" y="-3.81" size="1.524" layer="95">Controler_strobe=0
Control_sel=0</text>
<text x="256.54" y="-62.23" size="1.524" layer="95">Controler_strobe=1
Control_sel=0</text>
<text x="330.2" y="-3.81" size="1.524" layer="95">Controler_strobe=1
Control_sel=1</text>
<text x="207.01" y="-29.21" size="1.524" layer="95">Controler_strobe</text>
<wire x1="280.67" y1="-6.35" x2="321.31" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="316.23" y1="-1.27" x2="321.31" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="321.31" y1="-6.35" x2="316.23" y2="-11.43" width="0.1524" layer="94"/>
<wire x1="280.67" y1="6.35" x2="321.31" y2="6.35" width="0.1524" layer="94"/>
<wire x1="285.75" y1="1.27" x2="280.67" y2="6.35" width="0.1524" layer="94"/>
<wire x1="280.67" y1="6.35" x2="285.75" y2="11.43" width="0.1524" layer="94"/>
<text x="193.04" y="77.47" size="1.524" layer="95">WB.slave_Dcache</text>
<text x="133.35" y="77.47" size="1.524" layer="95">WB.slave_Icache</text>
</plain>
<instances>
</instances>
<busses>
</busses>
<nets>
<net name="N$4" class="0">
<segment>
<wire x1="172.72" y1="-5.08" x2="172.72" y2="0" width="0.1524" layer="91"/>
<wire x1="172.72" y1="0" x2="200.66" y2="0" width="0.1524" layer="91"/>
<wire x1="200.66" y1="0" x2="200.66" y2="26.67" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="167.64" y1="-5.08" x2="167.64" y2="0" width="0.1524" layer="91"/>
<wire x1="167.64" y1="0" x2="139.7" y2="0" width="0.1524" layer="91"/>
<wire x1="139.7" y1="0" x2="139.7" y2="26.67" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="170.18" y1="-10.16" x2="170.18" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="163.83" y1="-7.62" x2="160.02" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="170.18" y1="-48.26" x2="170.18" y2="-58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="190.5" y1="-29.21" x2="205.74" y2="-29.21" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="139.7" y1="54.61" x2="139.7" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="200.66" y1="54.61" x2="200.66" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="86.995" y="213.995" size="3.556" layer="95">+</text>
<wire x1="86.36" y1="222.885" x2="86.36" y2="207.645" width="0.1524" layer="94"/>
<wire x1="86.36" y1="207.645" x2="91.44" y2="210.185" width="0.1524" layer="94"/>
<wire x1="91.44" y1="210.185" x2="91.44" y2="220.345" width="0.1524" layer="94"/>
<wire x1="91.44" y1="220.345" x2="86.36" y2="222.885" width="0.1524" layer="94"/>
<wire x1="100.33" y1="213.995" x2="100.33" y2="207.645" width="0.1524" layer="94"/>
<wire x1="100.33" y1="207.645" x2="113.03" y2="207.645" width="0.1524" layer="94"/>
<wire x1="113.03" y1="207.645" x2="113.03" y2="213.995" width="0.1524" layer="94"/>
<wire x1="113.03" y1="213.995" x2="100.33" y2="213.995" width="0.1524" layer="94"/>
<text x="104.14" y="210.185" size="1.778" layer="95">ADJ6</text>
<wire x1="125.73" y1="212.725" x2="125.73" y2="207.645" width="0.1524" layer="94"/>
<wire x1="125.73" y1="207.645" x2="130.81" y2="207.645" width="0.1524" layer="94"/>
<wire x1="130.81" y1="207.645" x2="130.81" y2="212.725" width="0.1524" layer="94"/>
<wire x1="130.81" y1="212.725" x2="125.73" y2="212.725" width="0.1524" layer="94"/>
<text x="128.27" y="210.185" size="1.778" layer="95" align="center">PC</text>
<wire x1="-99.06" y1="106.68" x2="-99.06" y2="66.04" width="0.1524" layer="94"/>
<wire x1="-99.06" y1="66.04" x2="-73.66" y2="66.04" width="0.1524" layer="94"/>
<wire x1="-73.66" y1="66.04" x2="-73.66" y2="106.68" width="0.1524" layer="94"/>
<wire x1="-73.66" y1="106.68" x2="-99.06" y2="106.68" width="0.1524" layer="94"/>
<text x="-86.36" y="86.36" size="2.54" layer="95" align="center">Valid bit
array</text>
<wire x1="-66.04" y1="106.68" x2="-66.04" y2="66.04" width="0.1524" layer="94"/>
<wire x1="-66.04" y1="66.04" x2="-40.64" y2="66.04" width="0.1524" layer="94"/>
<wire x1="-40.64" y1="66.04" x2="-40.64" y2="106.68" width="0.1524" layer="94"/>
<wire x1="-40.64" y1="106.68" x2="-66.04" y2="106.68" width="0.1524" layer="94"/>
<text x="-53.34" y="86.36" size="2.54" layer="95" align="center">Dirty bit
array</text>
<wire x1="-33.02" y1="106.68" x2="-33.02" y2="66.04" width="0.1524" layer="94"/>
<wire x1="-33.02" y1="66.04" x2="-7.62" y2="66.04" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="66.04" x2="-7.62" y2="106.68" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="106.68" x2="-33.02" y2="106.68" width="0.1524" layer="94"/>
<text x="-20.32" y="86.36" size="2.54" layer="95" align="center">Tag</text>
<text x="-19.05" y="114.3" size="1.778" layer="95" align="center">SLAVE_ADDR[6:4]</text>
<text x="-20.32" y="44.45" size="2.54" layer="95" align="center">=</text>
<circle x="-20.32" y="44.45" radius="5.23634375" width="0.1524" layer="94"/>
<text x="-33.02" y="45.72" size="1.778" layer="95" rot="R90" align="center">SLAVE_ADDR[15:7]</text>
<text x="-52.07" y="114.3" size="1.778" layer="95" align="center">SLAVE_ADDR[6:4]</text>
<text x="-85.09" y="114.3" size="1.778" layer="95" align="center">SLAVE_ADDR[6:4]</text>
<wire x1="-49.53" y1="3.81" x2="-54.61" y2="3.81" width="0.1524" layer="94" curve="-180"/>
<wire x1="-54.61" y1="3.81" x2="-54.61" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-54.61" y1="7.62" x2="-49.53" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-49.53" y1="7.62" x2="-49.53" y2="3.81" width="0.1524" layer="94"/>
<wire x1="1.27" y1="106.68" x2="1.27" y2="66.04" width="0.1524" layer="94"/>
<wire x1="1.27" y1="66.04" x2="26.67" y2="66.04" width="0.1524" layer="94"/>
<wire x1="26.67" y1="66.04" x2="26.67" y2="106.68" width="0.1524" layer="94"/>
<wire x1="26.67" y1="106.68" x2="1.27" y2="106.68" width="0.1524" layer="94"/>
<text x="13.97" y="86.36" size="2.54" layer="95" align="center">Data</text>
<text x="15.24" y="114.3" size="1.778" layer="95" align="center">SLAVE_ADDR[6:4]</text>
<wire x1="82.55" y1="106.68" x2="82.55" y2="66.04" width="0.1524" layer="94"/>
<wire x1="82.55" y1="66.04" x2="107.95" y2="66.04" width="0.1524" layer="94"/>
<wire x1="107.95" y1="66.04" x2="107.95" y2="106.68" width="0.1524" layer="94"/>
<wire x1="107.95" y1="106.68" x2="82.55" y2="106.68" width="0.1524" layer="94"/>
<text x="95.25" y="86.36" size="2.54" layer="95" align="center">Valid bit
array</text>
<wire x1="115.57" y1="106.68" x2="115.57" y2="66.04" width="0.1524" layer="94"/>
<wire x1="115.57" y1="66.04" x2="140.97" y2="66.04" width="0.1524" layer="94"/>
<wire x1="140.97" y1="66.04" x2="140.97" y2="106.68" width="0.1524" layer="94"/>
<wire x1="140.97" y1="106.68" x2="115.57" y2="106.68" width="0.1524" layer="94"/>
<text x="128.27" y="86.36" size="2.54" layer="95" align="center">Dirty bit
array</text>
<wire x1="148.59" y1="106.68" x2="148.59" y2="66.04" width="0.1524" layer="94"/>
<wire x1="148.59" y1="66.04" x2="173.99" y2="66.04" width="0.1524" layer="94"/>
<wire x1="173.99" y1="66.04" x2="173.99" y2="106.68" width="0.1524" layer="94"/>
<wire x1="173.99" y1="106.68" x2="148.59" y2="106.68" width="0.1524" layer="94"/>
<text x="161.29" y="86.36" size="2.54" layer="95" align="center">Tag</text>
<text x="162.56" y="114.3" size="1.778" layer="95" align="center">SLAVE_ADDR[6:4]</text>
<text x="161.29" y="44.45" size="2.54" layer="95" align="center">=</text>
<circle x="161.29" y="44.45" radius="5.23634375" width="0.1524" layer="94"/>
<text x="148.59" y="42.545" size="1.778" layer="95" rot="R90" align="center">SLAVE_ADDR[15:7]</text>
<text x="130.81" y="113.665" size="1.778" layer="95" align="center">SLAVE_ADDR[6:4]</text>
<text x="96.52" y="114.3" size="1.778" layer="95" align="center">SLAVE_ADDR[6:4]</text>
<wire x1="132.08" y1="3.81" x2="127" y2="3.81" width="0.1524" layer="94" curve="-180"/>
<wire x1="127" y1="3.81" x2="127" y2="7.62" width="0.1524" layer="94"/>
<wire x1="127" y1="7.62" x2="132.08" y2="7.62" width="0.1524" layer="94"/>
<wire x1="132.08" y1="7.62" x2="132.08" y2="3.81" width="0.1524" layer="94"/>
<wire x1="181.61" y1="106.68" x2="181.61" y2="66.04" width="0.1524" layer="94"/>
<wire x1="181.61" y1="66.04" x2="207.01" y2="66.04" width="0.1524" layer="94"/>
<wire x1="207.01" y1="66.04" x2="207.01" y2="106.68" width="0.1524" layer="94"/>
<wire x1="207.01" y1="106.68" x2="181.61" y2="106.68" width="0.1524" layer="94"/>
<text x="194.31" y="86.36" size="2.54" layer="95" align="center">Data</text>
<text x="195.58" y="114.3" size="1.778" layer="95" align="center">SLAVE_ADDR[6:4]</text>
<wire x1="36.83" y1="-24.13" x2="33.02" y2="-24.13" width="0.1524" layer="94" curve="-180"/>
<wire x1="34.925" y1="-30.48" x2="33.02" y2="-28.575" width="0.1524" layer="94" curve="-90"/>
<wire x1="33.02" y1="-28.575" x2="33.02" y2="-24.13" width="0.1524" layer="94"/>
<wire x1="34.925" y1="-30.48" x2="36.83" y2="-28.575" width="0.1524" layer="94" curve="90"/>
<wire x1="36.83" y1="-28.575" x2="36.83" y2="-24.13" width="0.1524" layer="94"/>
<wire x1="137.16" y1="-57.785" x2="122.555" y2="-57.785" width="0.1524" layer="94"/>
<wire x1="122.555" y1="-57.785" x2="125.095" y2="-62.865" width="0.1524" layer="94"/>
<wire x1="125.095" y1="-62.865" x2="134.62" y2="-62.865" width="0.1524" layer="94"/>
<wire x1="134.62" y1="-62.865" x2="137.16" y2="-57.785" width="0.1524" layer="94"/>
<text x="34.925" y="-60.96" size="1.778" layer="95" align="center">SLAVE.ACK_resp</text>
<wire x1="73.66" y1="217.17" x2="58.42" y2="217.17" width="0.1524" layer="94"/>
<wire x1="58.42" y1="217.17" x2="60.96" y2="212.09" width="0.1524" layer="94"/>
<wire x1="60.96" y1="212.09" x2="71.12" y2="212.09" width="0.1524" layer="94"/>
<wire x1="71.12" y1="212.09" x2="73.66" y2="217.17" width="0.1524" layer="94"/>
<wire x1="209.55" y1="26.67" x2="209.55" y2="-13.97" width="0.1524" layer="94"/>
<wire x1="209.55" y1="-13.97" x2="234.95" y2="-13.97" width="0.1524" layer="94"/>
<wire x1="234.95" y1="-13.97" x2="234.95" y2="26.67" width="0.1524" layer="94"/>
<wire x1="234.95" y1="26.67" x2="209.55" y2="26.67" width="0.1524" layer="94"/>
<text x="222.25" y="6.35" size="2.54" layer="95" align="center">TRUE LRU</text>
<text x="223.52" y="34.29" size="1.778" layer="95" align="center">ADDR[6:4]</text>
<wire x1="154.305" y1="215.265" x2="149.225" y2="215.265" width="0.1524" layer="94" curve="-180"/>
<wire x1="149.225" y1="215.265" x2="149.225" y2="219.075" width="0.1524" layer="94"/>
<wire x1="149.225" y1="219.075" x2="154.305" y2="219.075" width="0.1524" layer="94"/>
<wire x1="154.305" y1="219.075" x2="154.305" y2="215.265" width="0.1524" layer="94"/>
<wire x1="163.195" y1="219.075" x2="159.385" y2="219.075" width="0.1524" layer="94" curve="-180"/>
<wire x1="161.29" y1="212.725" x2="159.385" y2="214.63" width="0.1524" layer="94" curve="-90"/>
<wire x1="159.385" y1="214.63" x2="159.385" y2="219.075" width="0.1524" layer="94"/>
<wire x1="161.29" y1="212.725" x2="163.195" y2="214.63" width="0.1524" layer="94" curve="90"/>
<wire x1="163.195" y1="214.63" x2="163.195" y2="219.075" width="0.1524" layer="94"/>
<wire x1="141.605" y1="219.075" x2="142.875" y2="217.17" width="0.1524" layer="94"/>
<wire x1="142.875" y1="217.17" x2="144.145" y2="219.075" width="0.1524" layer="94"/>
<wire x1="144.145" y1="219.075" x2="141.605" y2="219.075" width="0.1524" layer="94"/>
<circle x="142.875" y="216.535" radius="0.635" width="0.1524" layer="94"/>
<wire x1="227.965" y1="-22.225" x2="229.235" y2="-24.13" width="0.1524" layer="94"/>
<wire x1="229.235" y1="-24.13" x2="230.505" y2="-22.225" width="0.1524" layer="94"/>
<wire x1="230.505" y1="-22.225" x2="227.965" y2="-22.225" width="0.1524" layer="94"/>
<circle x="229.235" y="-24.765" radius="0.635" width="0.1524" layer="94"/>
<wire x1="219.075" y1="-31.115" x2="213.995" y2="-31.115" width="0.1524" layer="94" curve="-180"/>
<wire x1="213.995" y1="-31.115" x2="213.995" y2="-27.305" width="0.1524" layer="94"/>
<wire x1="213.995" y1="-27.305" x2="219.075" y2="-27.305" width="0.1524" layer="94"/>
<wire x1="219.075" y1="-27.305" x2="219.075" y2="-31.115" width="0.1524" layer="94"/>
<wire x1="233.045" y1="-31.115" x2="227.965" y2="-31.115" width="0.1524" layer="94" curve="-180"/>
<wire x1="227.965" y1="-31.115" x2="227.965" y2="-27.305" width="0.1524" layer="94"/>
<wire x1="227.965" y1="-27.305" x2="233.045" y2="-27.305" width="0.1524" layer="94"/>
<wire x1="233.045" y1="-27.305" x2="233.045" y2="-31.115" width="0.1524" layer="94"/>
<wire x1="225.425" y1="-40.64" x2="221.615" y2="-40.64" width="0.1524" layer="94" curve="-180"/>
<wire x1="223.52" y1="-46.99" x2="221.615" y2="-45.085" width="0.1524" layer="94" curve="-90"/>
<wire x1="221.615" y1="-45.085" x2="221.615" y2="-40.64" width="0.1524" layer="94"/>
<wire x1="223.52" y1="-46.99" x2="225.425" y2="-45.085" width="0.1524" layer="94" curve="90"/>
<wire x1="225.425" y1="-45.085" x2="225.425" y2="-40.64" width="0.1524" layer="94"/>
<text x="-53.975" y="58.42" size="1.778" layer="95" align="center">Dout1</text>
<text x="128.905" y="58.42" size="1.778" layer="95" align="center">Dout2</text>
<text x="233.68" y="-19.685" size="1.778" layer="95" rot="R180" align="center">Dout2</text>
<text x="219.71" y="-19.685" size="1.778" layer="95" rot="R180" align="center">Dout1</text>
<text x="223.52" y="-51.435" size="1.778" layer="95" align="center">Eviction_needed</text>
<text x="45.085" y="119.38" size="1.778" layer="95" align="center">SLAVE_SEL[15:0]</text>
<text x="228.6" y="97.79" size="1.778" layer="95" align="center">SLAVE_MDATA</text>
<text x="225.425" y="119.38" size="1.778" layer="95" align="center">SLAVE_SEL[15:0]</text>
<wire x1="36.195" y1="-43.18" x2="31.115" y2="-43.18" width="0.1524" layer="94" curve="-180"/>
<wire x1="31.115" y1="-43.18" x2="31.115" y2="-39.37" width="0.1524" layer="94"/>
<wire x1="31.115" y1="-39.37" x2="36.195" y2="-39.37" width="0.1524" layer="94"/>
<wire x1="36.195" y1="-39.37" x2="36.195" y2="-43.18" width="0.1524" layer="94"/>
<text x="27.94" y="-34.925" size="1.778" layer="95" align="center">mem_read</text>
<wire x1="36.83" y1="-50.8" x2="33.02" y2="-50.8" width="0.1524" layer="94" curve="-180"/>
<wire x1="34.925" y1="-57.15" x2="33.02" y2="-55.245" width="0.1524" layer="94" curve="-90"/>
<wire x1="33.02" y1="-55.245" x2="33.02" y2="-50.8" width="0.1524" layer="94"/>
<wire x1="34.925" y1="-57.15" x2="36.83" y2="-55.245" width="0.1524" layer="94" curve="90"/>
<wire x1="36.83" y1="-55.245" x2="36.83" y2="-50.8" width="0.1524" layer="94"/>
<text x="55.88" y="-42.545" size="1.778" layer="95" align="center">Cache_controller_write_response</text>
<text x="-29.845" y="123.825" size="1.778" layer="95" align="center">cache_control_load_tag1</text>
<text x="151.765" y="123.825" size="1.778" layer="95" align="center">cache_control_load_tag2</text>
<text x="-62.865" y="122.555" size="1.778" layer="95" align="center">cache_control_load_dirty1</text>
<text x="260.985" y="-5.715" size="1.778" layer="95" align="center">LRU_to_controller</text>
<polygon width="0.1524" layer="94">
<vertex x="-53.34" y="7.62"/>
<vertex x="-52.07" y="8.89"/>
<vertex x="-54.61" y="8.89"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-50.8" y="7.62"/>
<vertex x="-49.53" y="8.89"/>
<vertex x="-52.07" y="8.89"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-86.36" y="106.68"/>
<vertex x="-85.09" y="107.95"/>
<vertex x="-87.63" y="107.95"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-53.34" y="106.68"/>
<vertex x="-52.07" y="107.95"/>
<vertex x="-54.61" y="107.95"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-63.5" y="106.68"/>
<vertex x="-62.23" y="107.95"/>
<vertex x="-64.77" y="107.95"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-20.32" y="106.68"/>
<vertex x="-19.05" y="107.95"/>
<vertex x="-21.59" y="107.95"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-29.845" y="106.68"/>
<vertex x="-28.575" y="107.95"/>
<vertex x="-31.115" y="107.95"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="13.97" y="106.68"/>
<vertex x="15.24" y="107.95"/>
<vertex x="12.7" y="107.95"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="127" y="-57.785"/>
<vertex x="128.27" y="-56.515"/>
<vertex x="125.73" y="-56.515"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="132.715" y="-57.785"/>
<vertex x="133.985" y="-56.515"/>
<vertex x="131.445" y="-56.515"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="135.89" y="-60.325"/>
<vertex x="137.16" y="-61.595"/>
<vertex x="137.16" y="-59.055"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="161.29" y="49.53"/>
<vertex x="162.56" y="50.8"/>
<vertex x="160.02" y="50.8"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="156.21" y="44.45"/>
<vertex x="154.94" y="45.72"/>
<vertex x="154.94" y="43.18"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="148.59" y="74.93"/>
<vertex x="147.32" y="76.2"/>
<vertex x="147.32" y="73.66"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="151.765" y="106.68"/>
<vertex x="153.035" y="107.95"/>
<vertex x="150.495" y="107.95"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="161.29" y="106.68"/>
<vertex x="162.56" y="107.95"/>
<vertex x="160.02" y="107.95"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="194.31" y="106.68"/>
<vertex x="195.58" y="107.95"/>
<vertex x="193.04" y="107.95"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="128.27" y="106.68"/>
<vertex x="129.54" y="107.95"/>
<vertex x="127" y="107.95"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="95.25" y="106.68"/>
<vertex x="96.52" y="107.95"/>
<vertex x="93.98" y="107.95"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="207.01" y="97.79"/>
<vertex x="208.28" y="96.52"/>
<vertex x="208.28" y="99.06"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="207.01" y="101.6"/>
<vertex x="208.28" y="100.33"/>
<vertex x="208.28" y="102.87"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-20.32" y="49.53"/>
<vertex x="-19.05" y="50.8"/>
<vertex x="-21.59" y="50.8"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-25.4" y="44.45"/>
<vertex x="-26.67" y="45.72"/>
<vertex x="-26.67" y="43.18"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-33.02" y="77.47"/>
<vertex x="-34.29" y="78.74"/>
<vertex x="-34.29" y="76.2"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="26.67" y="97.79"/>
<vertex x="27.94" y="96.52"/>
<vertex x="27.94" y="99.06"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="26.67" y="101.6"/>
<vertex x="27.94" y="100.33"/>
<vertex x="27.94" y="102.87"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="260.985" y="-7.62"/>
<vertex x="259.715" y="-8.89"/>
<vertex x="262.255" y="-8.89"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="223.52" y="-50.8"/>
<vertex x="224.79" y="-49.53"/>
<vertex x="222.25" y="-49.53"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="222.25" y="26.67"/>
<vertex x="223.52" y="27.94"/>
<vertex x="220.98" y="27.94"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-53.975" y="59.69"/>
<vertex x="-52.705" y="60.96"/>
<vertex x="-55.245" y="60.96"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="128.905" y="59.69"/>
<vertex x="130.175" y="60.96"/>
<vertex x="127.635" y="60.96"/>
</polygon>
<text x="120.65" y="122.555" size="1.778" layer="95" align="center">cache_control_load_dirty2</text>
<polygon width="0.1524" layer="94">
<vertex x="120.015" y="106.68"/>
<vertex x="121.285" y="107.95"/>
<vertex x="118.745" y="107.95"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-66.04" y="97.79"/>
<vertex x="-67.31" y="99.06"/>
<vertex x="-67.31" y="96.52"/>
</polygon>
<text x="-69.85" y="113.665" size="1.778" layer="95" align="center">dirty_in</text>
<polygon width="0.1524" layer="94">
<vertex x="115.57" y="97.79"/>
<vertex x="114.3" y="99.06"/>
<vertex x="114.3" y="96.52"/>
</polygon>
<text x="111.76" y="113.665" size="1.778" layer="95" align="center">dirty_in</text>
<text x="-98.425" y="122.555" size="1.778" layer="95" align="center">cache_control_load_valid_bit1</text>
<polygon width="0.1524" layer="94">
<vertex x="-95.25" y="106.68"/>
<vertex x="-93.98" y="107.95"/>
<vertex x="-96.52" y="107.95"/>
</polygon>
<text x="83.185" y="122.555" size="1.778" layer="95" align="center">cache_control_load_valid_bit2</text>
<polygon width="0.1524" layer="94">
<vertex x="86.36" y="106.68"/>
<vertex x="87.63" y="107.95"/>
<vertex x="85.09" y="107.95"/>
</polygon>
<text x="-43.815" y="-51.435" size="1.778" layer="95" align="center">{SLAVE_ADDR[15:4],0000}</text>
<wire x1="-22.225" y1="-58.42" x2="-37.465" y2="-58.42" width="0.1524" layer="94"/>
<wire x1="-37.465" y1="-58.42" x2="-34.925" y2="-63.5" width="0.1524" layer="94"/>
<wire x1="-34.925" y1="-63.5" x2="-24.765" y2="-63.5" width="0.1524" layer="94"/>
<wire x1="-24.765" y1="-63.5" x2="-22.225" y2="-58.42" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="-32.385" y="-58.42"/>
<vertex x="-31.115" y="-57.15"/>
<vertex x="-33.655" y="-57.15"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-27.305" y="-58.42"/>
<vertex x="-26.035" y="-57.15"/>
<vertex x="-28.575" y="-57.15"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-29.845" y="-69.85"/>
<vertex x="-28.575" y="-68.58"/>
<vertex x="-31.115" y="-68.58"/>
</polygon>
<text x="-48.895" y="-34.29" size="1.778" layer="95" align="center">{TAG_OUT1,SLAVE_ADDR[6:4],0000}</text>
<text x="-5.715" y="-34.29" size="1.778" layer="95" align="center">{TAG_OUT2,SLAVE_ADDR[6:4],0000}</text>
<wire x1="-19.685" y1="-41.91" x2="-34.925" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-34.925" y1="-41.91" x2="-32.385" y2="-46.99" width="0.1524" layer="94"/>
<wire x1="-32.385" y1="-46.99" x2="-22.225" y2="-46.99" width="0.1524" layer="94"/>
<wire x1="-22.225" y1="-46.99" x2="-19.685" y2="-41.91" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="-29.845" y="-41.91"/>
<vertex x="-28.575" y="-40.64"/>
<vertex x="-31.115" y="-40.64"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-24.765" y="-41.91"/>
<vertex x="-23.495" y="-40.64"/>
<vertex x="-26.035" y="-40.64"/>
</polygon>
<text x="-1.27" y="-60.96" size="1.778" layer="95" align="center">controller_address_selector</text>
<text x="-3.175" y="-44.45" size="1.778" layer="95" align="center">LRU_to_controller</text>
<text x="-29.845" y="-71.755" size="1.778" layer="95" align="center">MASTER.ADDR</text>
<polygon width="0.1524" layer="94">
<vertex x="234.95" y="20.32"/>
<vertex x="236.22" y="19.05"/>
<vertex x="236.22" y="21.59"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="234.95" y="13.335"/>
<vertex x="236.22" y="12.065"/>
<vertex x="236.22" y="14.605"/>
</polygon>
<text x="259.08" y="20.32" size="1.778" layer="95" align="center">Load_LRU</text>
<text x="259.08" y="13.335" size="1.778" layer="95" align="center">LRU_in_bit</text>
<text x="-13.335" y="59.055" size="1.778" layer="95" align="center">TAG_OUT1</text>
<text x="168.91" y="58.42" size="1.778" layer="95" align="center">TAG_OUT2</text>
<polygon width="0.1524" layer="94">
<vertex x="34.925" y="-59.69"/>
<vertex x="36.195" y="-58.42"/>
<vertex x="33.655" y="-58.42"/>
</polygon>
<text x="-19.685" y="117.475" size="12.7" layer="94">ICACHE &amp; DCACHE
</text>
<text x="129.54" y="-70.485" size="1.778" layer="95" align="center">SLAVE_SDATA</text>
<text x="47.625" y="97.79" size="1.778" layer="95" align="center">SLAVE_MDATA</text>
</plain>
<instances>
</instances>
<busses>
</busses>
<nets>
<net name="N$13" class="0">
<segment>
<wire x1="86.36" y1="217.805" x2="81.28" y2="217.805" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="86.36" y1="212.725" x2="81.28" y2="212.725" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="91.44" y1="215.265" x2="96.52" y2="215.265" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="128.27" y1="212.725" x2="128.27" y2="216.535" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="-86.36" y1="106.68" x2="-86.36" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<wire x1="-53.34" y1="106.68" x2="-53.34" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<wire x1="-20.32" y1="106.68" x2="-20.32" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<wire x1="-20.32" y1="49.53" x2="-20.32" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<wire x1="-25.4" y1="44.45" x2="-27.305" y2="44.45" width="0.1524" layer="91"/>
<wire x1="-27.305" y1="44.45" x2="-31.75" y2="44.45" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="77.47" x2="-36.83" y2="77.47" width="0.1524" layer="91"/>
<wire x1="-36.83" y1="77.47" x2="-36.83" y2="62.23" width="0.1524" layer="91"/>
<wire x1="-36.83" y1="62.23" x2="-27.305" y2="62.23" width="0.1524" layer="91"/>
<wire x1="-27.305" y1="62.23" x2="-27.305" y2="44.45" width="0.1524" layer="91"/>
<junction x="-27.305" y="44.45"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<wire x1="-50.8" y1="7.62" x2="-50.8" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="10.16" x2="-20.32" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="10.16" x2="-20.32" y2="39.37" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<wire x1="-86.36" y1="66.04" x2="-86.36" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="10.16" x2="-53.34" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="10.16" x2="-53.34" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<wire x1="13.97" y1="106.68" x2="13.97" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<wire x1="95.25" y1="106.68" x2="95.25" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<wire x1="128.27" y1="106.68" x2="128.27" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<wire x1="161.29" y1="106.68" x2="161.29" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<wire x1="161.29" y1="49.53" x2="161.29" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<wire x1="156.21" y1="44.45" x2="153.035" y2="44.45" width="0.1524" layer="91"/>
<wire x1="153.035" y1="44.45" x2="149.86" y2="44.45" width="0.1524" layer="91"/>
<wire x1="148.59" y1="75.565" x2="148.59" y2="74.93" width="0.1524" layer="91"/>
<wire x1="148.59" y1="74.93" x2="146.05" y2="74.93" width="0.1524" layer="91"/>
<wire x1="146.05" y1="74.93" x2="146.05" y2="59.055" width="0.1524" layer="91"/>
<wire x1="146.05" y1="59.055" x2="153.035" y2="59.055" width="0.1524" layer="91"/>
<wire x1="153.035" y1="59.055" x2="153.035" y2="44.45" width="0.1524" layer="91"/>
<junction x="153.035" y="44.45"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<wire x1="130.81" y1="7.62" x2="130.81" y2="10.16" width="0.1524" layer="91"/>
<wire x1="130.81" y1="10.16" x2="161.29" y2="10.16" width="0.1524" layer="91"/>
<wire x1="161.29" y1="10.16" x2="161.29" y2="39.37" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<wire x1="95.25" y1="66.04" x2="95.25" y2="10.16" width="0.1524" layer="91"/>
<wire x1="95.25" y1="10.16" x2="128.27" y2="10.16" width="0.1524" layer="91"/>
<wire x1="128.27" y1="10.16" x2="128.27" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<wire x1="194.31" y1="106.68" x2="194.31" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<wire x1="33.655" y1="-25.4" x2="33.655" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="33.655" y1="-22.86" x2="-52.07" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="-52.07" y1="-22.86" x2="-52.07" y2="1.27" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<wire x1="135.89" y1="-60.325" x2="140.97" y2="-60.325" width="0.1524" layer="91"/>
<wire x1="36.195" y1="-25.4" x2="36.195" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="36.195" y1="-22.86" x2="129.54" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="129.54" y1="-22.86" x2="129.54" y2="1.27" width="0.1524" layer="91"/>
<wire x1="140.97" y1="-60.325" x2="140.97" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="140.97" y1="-22.86" x2="129.54" y2="-22.86" width="0.1524" layer="91"/>
<junction x="129.54" y="-22.86"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<wire x1="127" y1="-57.785" x2="127" y2="-12.065" width="0.1524" layer="91"/>
<wire x1="127" y1="-12.065" x2="13.97" y2="-12.065" width="0.1524" layer="91"/>
<wire x1="13.97" y1="-12.065" x2="13.97" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<wire x1="132.715" y1="-57.785" x2="132.715" y2="-12.065" width="0.1524" layer="91"/>
<wire x1="132.715" y1="-12.065" x2="194.31" y2="-12.065" width="0.1524" layer="91"/>
<wire x1="194.31" y1="-12.065" x2="194.31" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<wire x1="34.925" y1="-30.48" x2="34.925" y2="-36.83" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<wire x1="68.58" y1="217.17" x2="68.58" y2="222.25" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<wire x1="63.5" y1="217.17" x2="63.5" y2="222.25" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<wire x1="66.04" y1="212.09" x2="66.04" y2="207.01" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<wire x1="222.25" y1="26.67" x2="222.25" y2="31.75" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<wire x1="160.02" y1="217.805" x2="160.02" y2="220.345" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<wire x1="162.56" y1="217.805" x2="162.56" y2="220.345" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<wire x1="150.495" y1="219.075" x2="150.495" y2="221.615" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<wire x1="153.035" y1="219.075" x2="153.035" y2="221.615" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<wire x1="142.875" y1="219.075" x2="142.875" y2="222.885" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<wire x1="142.875" y1="215.9" x2="142.875" y2="212.725" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<wire x1="215.265" y1="-17.145" x2="222.25" y2="-17.145" width="0.1524" layer="91"/>
<wire x1="222.25" y1="-17.145" x2="222.25" y2="-13.97" width="0.1524" layer="91"/>
<junction x="222.25" y="-17.145"/>
<wire x1="222.25" y1="-17.145" x2="229.235" y2="-17.145" width="0.1524" layer="91"/>
<wire x1="229.235" y1="-17.145" x2="260.985" y2="-17.145" width="0.1524" layer="91"/>
<wire x1="260.985" y1="-17.145" x2="260.985" y2="-8.255" width="0.1524" layer="91"/>
<wire x1="215.265" y1="-27.305" x2="215.265" y2="-17.145" width="0.1524" layer="91"/>
<wire x1="229.235" y1="-22.225" x2="229.235" y2="-17.145" width="0.1524" layer="91"/>
<junction x="229.235" y="-17.145"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<wire x1="222.25" y1="-41.91" x2="222.25" y2="-39.37" width="0.1524" layer="91"/>
<wire x1="216.535" y1="-33.655" x2="216.535" y2="-39.37" width="0.1524" layer="91"/>
<wire x1="216.535" y1="-39.37" x2="222.25" y2="-39.37" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<wire x1="224.79" y1="-41.91" x2="224.79" y2="-39.37" width="0.1524" layer="91"/>
<wire x1="224.79" y1="-39.37" x2="230.505" y2="-39.37" width="0.1524" layer="91"/>
<wire x1="230.505" y1="-39.37" x2="230.505" y2="-33.655" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<wire x1="-53.975" y1="66.04" x2="-53.975" y2="59.69" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<wire x1="128.905" y1="66.04" x2="128.905" y2="59.69" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<wire x1="231.775" y1="-27.305" x2="231.775" y2="-20.955" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<wire x1="217.805" y1="-27.305" x2="217.805" y2="-20.955" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<wire x1="223.52" y1="-46.99" x2="223.52" y2="-50.165" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<wire x1="26.67" y1="97.79" x2="38.735" y2="97.79" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<wire x1="26.67" y1="101.6" x2="45.085" y2="101.6" width="0.1524" layer="91"/>
<wire x1="45.085" y1="101.6" x2="45.085" y2="117.475" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<wire x1="207.01" y1="97.79" x2="219.075" y2="97.79" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$105" class="0">
<segment>
<wire x1="207.01" y1="101.6" x2="225.425" y2="101.6" width="0.1524" layer="91"/>
<wire x1="225.425" y1="101.6" x2="225.425" y2="117.475" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<wire x1="32.385" y1="-39.37" x2="32.385" y2="-36.83" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<wire x1="34.925" y1="-39.37" x2="34.925" y2="-36.83" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$108" class="0">
<segment>
<wire x1="33.655" y1="-52.07" x2="33.655" y2="-45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$109" class="0">
<segment>
<wire x1="36.195" y1="-52.07" x2="36.195" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="36.195" y1="-49.53" x2="41.91" y2="-49.53" width="0.1524" layer="91"/>
<wire x1="41.91" y1="-49.53" x2="41.91" y2="-45.085" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$110" class="0">
<segment>
<wire x1="34.925" y1="-57.15" x2="34.925" y2="-59.69" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$111" class="0">
<segment>
<wire x1="129.54" y1="-62.865" x2="129.54" y2="-67.945" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$112" class="0">
<segment>
<wire x1="-29.845" y1="106.68" x2="-29.845" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$113" class="0">
<segment>
<wire x1="151.765" y1="106.68" x2="151.765" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$114" class="0">
<segment>
<wire x1="-63.5" y1="106.68" x2="-63.5" y2="120.65" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$115" class="0">
<segment>
<wire x1="229.235" y1="-27.305" x2="229.235" y2="-25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$116" class="0">
<segment>
<wire x1="120.015" y1="106.68" x2="120.015" y2="120.65" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$117" class="0">
<segment>
<wire x1="-67.31" y1="97.79" x2="-69.215" y2="97.79" width="0.1524" layer="91"/>
<wire x1="-69.215" y1="97.79" x2="-69.215" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$118" class="0">
<segment>
<wire x1="114.3" y1="97.79" x2="112.395" y2="97.79" width="0.1524" layer="91"/>
<wire x1="112.395" y1="97.79" x2="112.395" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$119" class="0">
<segment>
<wire x1="-95.25" y1="106.68" x2="-95.25" y2="120.65" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$120" class="0">
<segment>
<wire x1="86.36" y1="106.68" x2="86.36" y2="120.65" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$121" class="0">
<segment>
<wire x1="-32.385" y1="-58.42" x2="-32.385" y2="-53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$122" class="0">
<segment>
<wire x1="-29.845" y1="-63.5" x2="-29.845" y2="-68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$123" class="0">
<segment>
<wire x1="-24.765" y1="-41.91" x2="-24.765" y2="-36.83" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$124" class="0">
<segment>
<wire x1="-29.845" y1="-41.91" x2="-29.845" y2="-36.83" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$125" class="0">
<segment>
<wire x1="-27.305" y1="-46.99" x2="-27.305" y2="-58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$126" class="0">
<segment>
<wire x1="-20.955" y1="-44.45" x2="-13.97" y2="-44.45" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$127" class="0">
<segment>
<wire x1="-23.495" y1="-60.96" x2="-16.51" y2="-60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$128" class="0">
<segment>
<wire x1="234.95" y1="20.32" x2="251.46" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$129" class="0">
<segment>
<wire x1="234.95" y1="13.335" x2="251.46" y2="13.335" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<wire x1="-80.645" y1="116.84" x2="-80.645" y2="76.2" width="0.1524" layer="94"/>
<wire x1="-80.645" y1="76.2" x2="-55.245" y2="76.2" width="0.1524" layer="94"/>
<wire x1="-55.245" y1="76.2" x2="-55.245" y2="116.84" width="0.1524" layer="94"/>
<wire x1="-55.245" y1="116.84" x2="-80.645" y2="116.84" width="0.1524" layer="94"/>
<text x="-67.945" y="96.52" size="2.54" layer="95" align="center">Valid bit
array</text>
<wire x1="-47.625" y1="116.84" x2="-47.625" y2="76.2" width="0.1524" layer="94"/>
<wire x1="-47.625" y1="76.2" x2="-22.225" y2="76.2" width="0.1524" layer="94"/>
<wire x1="-22.225" y1="76.2" x2="-22.225" y2="116.84" width="0.1524" layer="94"/>
<wire x1="-22.225" y1="116.84" x2="-47.625" y2="116.84" width="0.1524" layer="94"/>
<text x="-34.925" y="96.52" size="2.54" layer="95" align="center">Dirty bit
array</text>
<wire x1="-14.605" y1="116.84" x2="-14.605" y2="76.2" width="0.1524" layer="94"/>
<wire x1="-14.605" y1="76.2" x2="10.795" y2="76.2" width="0.1524" layer="94"/>
<wire x1="10.795" y1="76.2" x2="10.795" y2="116.84" width="0.1524" layer="94"/>
<wire x1="10.795" y1="116.84" x2="-14.605" y2="116.84" width="0.1524" layer="94"/>
<text x="-1.905" y="96.52" size="2.54" layer="95" align="center">Tag</text>
<text x="-0.635" y="124.46" size="1.778" layer="95" align="center">SLAVE_ADDR[6:4]</text>
<text x="-1.905" y="54.61" size="2.54" layer="95" align="center">=</text>
<circle x="-1.905" y="54.61" radius="5.23634375" width="0.1524" layer="94"/>
<text x="-14.605" y="55.88" size="1.778" layer="95" rot="R90" align="center">SLAVE_ADDR[15:7]</text>
<text x="-33.655" y="124.46" size="1.778" layer="95" align="center">SLAVE_ADDR[6:4]</text>
<text x="-66.675" y="124.46" size="1.778" layer="95" align="center">SLAVE_ADDR[6:4]</text>
<wire x1="-31.115" y1="13.97" x2="-36.195" y2="13.97" width="0.1524" layer="94" curve="-180"/>
<wire x1="-36.195" y1="13.97" x2="-36.195" y2="17.78" width="0.1524" layer="94"/>
<wire x1="-36.195" y1="17.78" x2="-31.115" y2="17.78" width="0.1524" layer="94"/>
<wire x1="-31.115" y1="17.78" x2="-31.115" y2="13.97" width="0.1524" layer="94"/>
<wire x1="19.685" y1="116.84" x2="19.685" y2="76.2" width="0.1524" layer="94"/>
<wire x1="19.685" y1="76.2" x2="45.085" y2="76.2" width="0.1524" layer="94"/>
<wire x1="45.085" y1="76.2" x2="45.085" y2="116.84" width="0.1524" layer="94"/>
<wire x1="45.085" y1="116.84" x2="19.685" y2="116.84" width="0.1524" layer="94"/>
<text x="32.385" y="96.52" size="2.54" layer="95" align="center">Data</text>
<text x="33.655" y="124.46" size="1.778" layer="95" align="center">SLAVE_ADDR[6:4]</text>
<wire x1="100.965" y1="116.84" x2="100.965" y2="76.2" width="0.1524" layer="94"/>
<wire x1="100.965" y1="76.2" x2="126.365" y2="76.2" width="0.1524" layer="94"/>
<wire x1="126.365" y1="76.2" x2="126.365" y2="116.84" width="0.1524" layer="94"/>
<wire x1="126.365" y1="116.84" x2="100.965" y2="116.84" width="0.1524" layer="94"/>
<text x="113.665" y="96.52" size="2.54" layer="95" align="center">Valid bit
array</text>
<wire x1="133.985" y1="116.84" x2="133.985" y2="76.2" width="0.1524" layer="94"/>
<wire x1="133.985" y1="76.2" x2="159.385" y2="76.2" width="0.1524" layer="94"/>
<wire x1="159.385" y1="76.2" x2="159.385" y2="116.84" width="0.1524" layer="94"/>
<wire x1="159.385" y1="116.84" x2="133.985" y2="116.84" width="0.1524" layer="94"/>
<text x="146.685" y="96.52" size="2.54" layer="95" align="center">Dirty bit
array</text>
<wire x1="167.005" y1="116.84" x2="167.005" y2="76.2" width="0.1524" layer="94"/>
<wire x1="167.005" y1="76.2" x2="192.405" y2="76.2" width="0.1524" layer="94"/>
<wire x1="192.405" y1="76.2" x2="192.405" y2="116.84" width="0.1524" layer="94"/>
<wire x1="192.405" y1="116.84" x2="167.005" y2="116.84" width="0.1524" layer="94"/>
<text x="179.705" y="96.52" size="2.54" layer="95" align="center">Tag</text>
<text x="180.975" y="124.46" size="1.778" layer="95" align="center">SLAVE_ADDR[6:4]</text>
<text x="179.705" y="54.61" size="2.54" layer="95" align="center">=</text>
<circle x="179.705" y="54.61" radius="5.23634375" width="0.1524" layer="94"/>
<text x="167.005" y="52.705" size="1.778" layer="95" rot="R90" align="center">SLAVE_ADDR[15:7]</text>
<text x="149.225" y="123.825" size="1.778" layer="95" align="center">SLAVE_ADDR[6:4]</text>
<text x="114.935" y="124.46" size="1.778" layer="95" align="center">SLAVE_ADDR[6:4]</text>
<wire x1="150.495" y1="13.97" x2="145.415" y2="13.97" width="0.1524" layer="94" curve="-180"/>
<wire x1="145.415" y1="13.97" x2="145.415" y2="17.78" width="0.1524" layer="94"/>
<wire x1="145.415" y1="17.78" x2="150.495" y2="17.78" width="0.1524" layer="94"/>
<wire x1="150.495" y1="17.78" x2="150.495" y2="13.97" width="0.1524" layer="94"/>
<wire x1="200.025" y1="116.84" x2="200.025" y2="76.2" width="0.1524" layer="94"/>
<wire x1="200.025" y1="76.2" x2="225.425" y2="76.2" width="0.1524" layer="94"/>
<wire x1="225.425" y1="76.2" x2="225.425" y2="116.84" width="0.1524" layer="94"/>
<wire x1="225.425" y1="116.84" x2="200.025" y2="116.84" width="0.1524" layer="94"/>
<text x="212.725" y="96.52" size="2.54" layer="95" align="center">Data</text>
<text x="213.995" y="124.46" size="1.778" layer="95" align="center">SLAVE_ADDR[6:4]</text>
<wire x1="55.245" y1="-13.97" x2="51.435" y2="-13.97" width="0.1524" layer="94" curve="-180"/>
<wire x1="53.34" y1="-20.32" x2="51.435" y2="-18.415" width="0.1524" layer="94" curve="-90"/>
<wire x1="51.435" y1="-18.415" x2="51.435" y2="-13.97" width="0.1524" layer="94"/>
<wire x1="53.34" y1="-20.32" x2="55.245" y2="-18.415" width="0.1524" layer="94" curve="90"/>
<wire x1="55.245" y1="-18.415" x2="55.245" y2="-13.97" width="0.1524" layer="94"/>
<wire x1="155.575" y1="-47.625" x2="140.97" y2="-47.625" width="0.1524" layer="94"/>
<wire x1="140.97" y1="-47.625" x2="143.51" y2="-52.705" width="0.1524" layer="94"/>
<wire x1="143.51" y1="-52.705" x2="153.035" y2="-52.705" width="0.1524" layer="94"/>
<wire x1="153.035" y1="-52.705" x2="155.575" y2="-47.625" width="0.1524" layer="94"/>
<text x="53.34" y="-50.8" size="1.778" layer="95" align="center">SLAVE.ACK_resp</text>
<wire x1="-325.12" y1="63.5" x2="-325.12" y2="22.86" width="0.1524" layer="94"/>
<wire x1="-325.12" y1="22.86" x2="-299.72" y2="22.86" width="0.1524" layer="94"/>
<wire x1="-299.72" y1="22.86" x2="-299.72" y2="63.5" width="0.1524" layer="94"/>
<wire x1="-299.72" y1="63.5" x2="-325.12" y2="63.5" width="0.1524" layer="94"/>
<text x="-312.42" y="43.18" size="2.54" layer="95" align="center">PSEUDO
LRU</text>
<text x="-311.15" y="71.12" size="1.778" layer="95" align="center">ADDR[6:4]</text>
<wire x1="-306.705" y1="14.605" x2="-305.435" y2="12.7" width="0.1524" layer="94"/>
<wire x1="-305.435" y1="12.7" x2="-304.165" y2="14.605" width="0.1524" layer="94"/>
<wire x1="-304.165" y1="14.605" x2="-306.705" y2="14.605" width="0.1524" layer="94"/>
<circle x="-305.435" y="12.065" radius="0.635" width="0.1524" layer="94"/>
<wire x1="-315.595" y1="5.715" x2="-320.675" y2="5.715" width="0.1524" layer="94" curve="-180"/>
<wire x1="-320.675" y1="5.715" x2="-320.675" y2="9.525" width="0.1524" layer="94"/>
<wire x1="-320.675" y1="9.525" x2="-315.595" y2="9.525" width="0.1524" layer="94"/>
<wire x1="-315.595" y1="9.525" x2="-315.595" y2="5.715" width="0.1524" layer="94"/>
<wire x1="-301.625" y1="5.715" x2="-306.705" y2="5.715" width="0.1524" layer="94" curve="-180"/>
<wire x1="-306.705" y1="5.715" x2="-306.705" y2="9.525" width="0.1524" layer="94"/>
<wire x1="-306.705" y1="9.525" x2="-301.625" y2="9.525" width="0.1524" layer="94"/>
<wire x1="-301.625" y1="9.525" x2="-301.625" y2="5.715" width="0.1524" layer="94"/>
<wire x1="-309.245" y1="-3.81" x2="-313.055" y2="-3.81" width="0.1524" layer="94" curve="-180"/>
<wire x1="-311.15" y1="-10.16" x2="-313.055" y2="-8.255" width="0.1524" layer="94" curve="-90"/>
<wire x1="-313.055" y1="-8.255" x2="-313.055" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-311.15" y1="-10.16" x2="-309.245" y2="-8.255" width="0.1524" layer="94" curve="90"/>
<wire x1="-309.245" y1="-8.255" x2="-309.245" y2="-3.81" width="0.1524" layer="94"/>
<text x="-35.56" y="68.58" size="1.778" layer="95" align="center">Dout1</text>
<text x="147.32" y="68.58" size="1.778" layer="95" align="center">Dout2</text>
<text x="-300.99" y="17.145" size="1.778" layer="95" rot="R180" align="center">Dout2</text>
<text x="-314.96" y="17.145" size="1.778" layer="95" rot="R180" align="center">Dout1</text>
<text x="-311.15" y="-14.605" size="1.778" layer="95" align="center">Eviction_needed</text>
<text x="63.5" y="129.54" size="1.778" layer="95" align="center">SLAVE_SEL[15:0]</text>
<text x="247.015" y="107.95" size="1.778" layer="95" align="center">SLAVE_MDATA</text>
<text x="243.84" y="129.54" size="1.778" layer="95" align="center">SLAVE_SEL[15:0]</text>
<wire x1="54.61" y1="-33.02" x2="49.53" y2="-33.02" width="0.1524" layer="94" curve="-180"/>
<wire x1="49.53" y1="-33.02" x2="49.53" y2="-29.21" width="0.1524" layer="94"/>
<wire x1="49.53" y1="-29.21" x2="54.61" y2="-29.21" width="0.1524" layer="94"/>
<wire x1="54.61" y1="-29.21" x2="54.61" y2="-33.02" width="0.1524" layer="94"/>
<text x="46.355" y="-24.765" size="1.778" layer="95" align="center">mem_read</text>
<wire x1="55.245" y1="-40.64" x2="51.435" y2="-40.64" width="0.1524" layer="94" curve="-180"/>
<wire x1="53.34" y1="-46.99" x2="51.435" y2="-45.085" width="0.1524" layer="94" curve="-90"/>
<wire x1="51.435" y1="-45.085" x2="51.435" y2="-40.64" width="0.1524" layer="94"/>
<wire x1="53.34" y1="-46.99" x2="55.245" y2="-45.085" width="0.1524" layer="94" curve="90"/>
<wire x1="55.245" y1="-45.085" x2="55.245" y2="-40.64" width="0.1524" layer="94"/>
<text x="74.295" y="-32.385" size="1.778" layer="95" align="center">Cache_controller_write_response</text>
<text x="-11.43" y="133.985" size="1.778" layer="95" align="center">cache_control_load_tag1</text>
<text x="170.18" y="133.985" size="1.778" layer="95" align="center">cache_control_load_tag2</text>
<text x="-44.45" y="132.715" size="1.778" layer="95" align="center">cache_control_load_dirty1</text>
<text x="-273.685" y="31.115" size="1.778" layer="95" align="center">LRU_to_controller</text>
<polygon width="0.1524" layer="94">
<vertex x="-34.925" y="17.78"/>
<vertex x="-33.655" y="19.05"/>
<vertex x="-36.195" y="19.05"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-32.385" y="17.78"/>
<vertex x="-31.115" y="19.05"/>
<vertex x="-33.655" y="19.05"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-67.945" y="116.84"/>
<vertex x="-66.675" y="118.11"/>
<vertex x="-69.215" y="118.11"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-34.925" y="116.84"/>
<vertex x="-33.655" y="118.11"/>
<vertex x="-36.195" y="118.11"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-45.085" y="116.84"/>
<vertex x="-43.815" y="118.11"/>
<vertex x="-46.355" y="118.11"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-1.905" y="116.84"/>
<vertex x="-0.635" y="118.11"/>
<vertex x="-3.175" y="118.11"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-11.43" y="116.84"/>
<vertex x="-10.16" y="118.11"/>
<vertex x="-12.7" y="118.11"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="32.385" y="116.84"/>
<vertex x="33.655" y="118.11"/>
<vertex x="31.115" y="118.11"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="145.415" y="-47.625"/>
<vertex x="146.685" y="-46.355"/>
<vertex x="144.145" y="-46.355"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="151.13" y="-47.625"/>
<vertex x="152.4" y="-46.355"/>
<vertex x="149.86" y="-46.355"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="154.305" y="-50.165"/>
<vertex x="155.575" y="-51.435"/>
<vertex x="155.575" y="-48.895"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="179.705" y="59.69"/>
<vertex x="180.975" y="60.96"/>
<vertex x="178.435" y="60.96"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="174.625" y="54.61"/>
<vertex x="173.355" y="55.88"/>
<vertex x="173.355" y="53.34"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="167.005" y="85.09"/>
<vertex x="165.735" y="86.36"/>
<vertex x="165.735" y="83.82"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="170.18" y="116.84"/>
<vertex x="171.45" y="118.11"/>
<vertex x="168.91" y="118.11"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="179.705" y="116.84"/>
<vertex x="180.975" y="118.11"/>
<vertex x="178.435" y="118.11"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="212.725" y="116.84"/>
<vertex x="213.995" y="118.11"/>
<vertex x="211.455" y="118.11"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="146.685" y="116.84"/>
<vertex x="147.955" y="118.11"/>
<vertex x="145.415" y="118.11"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="113.665" y="116.84"/>
<vertex x="114.935" y="118.11"/>
<vertex x="112.395" y="118.11"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="225.425" y="107.95"/>
<vertex x="226.695" y="106.68"/>
<vertex x="226.695" y="109.22"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="225.425" y="111.76"/>
<vertex x="226.695" y="110.49"/>
<vertex x="226.695" y="113.03"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-1.905" y="59.69"/>
<vertex x="-0.635" y="60.96"/>
<vertex x="-3.175" y="60.96"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-6.985" y="54.61"/>
<vertex x="-8.255" y="55.88"/>
<vertex x="-8.255" y="53.34"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-14.605" y="87.63"/>
<vertex x="-15.875" y="88.9"/>
<vertex x="-15.875" y="86.36"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="45.085" y="107.95"/>
<vertex x="46.355" y="106.68"/>
<vertex x="46.355" y="109.22"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="45.085" y="111.76"/>
<vertex x="46.355" y="110.49"/>
<vertex x="46.355" y="113.03"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-273.685" y="29.21"/>
<vertex x="-274.955" y="27.94"/>
<vertex x="-272.415" y="27.94"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-311.15" y="-13.97"/>
<vertex x="-309.88" y="-12.7"/>
<vertex x="-312.42" y="-12.7"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-312.42" y="63.5"/>
<vertex x="-311.15" y="64.77"/>
<vertex x="-313.69" y="64.77"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-35.56" y="69.85"/>
<vertex x="-34.29" y="71.12"/>
<vertex x="-36.83" y="71.12"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="147.32" y="69.85"/>
<vertex x="148.59" y="71.12"/>
<vertex x="146.05" y="71.12"/>
</polygon>
<text x="139.065" y="132.715" size="1.778" layer="95" align="center">cache_control_load_dirty2</text>
<polygon width="0.1524" layer="94">
<vertex x="138.43" y="116.84"/>
<vertex x="139.7" y="118.11"/>
<vertex x="137.16" y="118.11"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-47.625" y="107.95"/>
<vertex x="-48.895" y="109.22"/>
<vertex x="-48.895" y="106.68"/>
</polygon>
<text x="-51.435" y="123.825" size="1.778" layer="95" align="center">dirty_in</text>
<polygon width="0.1524" layer="94">
<vertex x="133.985" y="107.95"/>
<vertex x="132.715" y="109.22"/>
<vertex x="132.715" y="106.68"/>
</polygon>
<text x="130.175" y="123.825" size="1.778" layer="95" align="center">dirty_in</text>
<text x="-80.01" y="132.715" size="1.778" layer="95" align="center">cache_control_load_valid_bit1</text>
<polygon width="0.1524" layer="94">
<vertex x="-76.835" y="116.84"/>
<vertex x="-75.565" y="118.11"/>
<vertex x="-78.105" y="118.11"/>
</polygon>
<text x="101.6" y="132.715" size="1.778" layer="95" align="center">cache_control_load_valid_bit2</text>
<polygon width="0.1524" layer="94">
<vertex x="104.775" y="116.84"/>
<vertex x="106.045" y="118.11"/>
<vertex x="103.505" y="118.11"/>
</polygon>
<text x="-200.025" y="36.83" size="1.778" layer="95" align="center">{SLAVE_ADDR[15:4],0000}</text>
<wire x1="-178.435" y1="29.21" x2="-193.675" y2="29.21" width="0.1524" layer="94"/>
<wire x1="-193.675" y1="29.21" x2="-191.135" y2="24.13" width="0.1524" layer="94"/>
<wire x1="-191.135" y1="24.13" x2="-180.975" y2="24.13" width="0.1524" layer="94"/>
<wire x1="-180.975" y1="24.13" x2="-178.435" y2="29.21" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="-188.595" y="29.21"/>
<vertex x="-187.325" y="30.48"/>
<vertex x="-189.865" y="30.48"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-183.515" y="29.21"/>
<vertex x="-182.245" y="30.48"/>
<vertex x="-184.785" y="30.48"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-186.055" y="17.78"/>
<vertex x="-184.785" y="19.05"/>
<vertex x="-187.325" y="19.05"/>
</polygon>
<text x="-205.105" y="55.245" size="1.778" layer="95" align="center">{TAG_OUT1,SLAVE_ADDR[8:5],00000}</text>
<text x="-180.975" y="59.69" size="1.778" layer="95" align="center">{TAG_OUT2,SLAVE_ADDR[8:5],00000}</text>
<wire x1="-151.765" y1="45.72" x2="-191.135" y2="45.72" width="0.1524" layer="94"/>
<wire x1="-191.135" y1="45.72" x2="-186.69" y2="40.64" width="0.1524" layer="94"/>
<wire x1="-186.69" y1="40.64" x2="-154.305" y2="40.64" width="0.1524" layer="94"/>
<wire x1="-154.305" y1="40.64" x2="-151.765" y2="45.72" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="-186.055" y="45.72"/>
<vertex x="-184.785" y="46.99"/>
<vertex x="-187.325" y="46.99"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-177.8" y="45.72"/>
<vertex x="-176.53" y="46.99"/>
<vertex x="-179.07" y="46.99"/>
</polygon>
<text x="-157.48" y="26.67" size="1.778" layer="95" align="center">controller_address_selector</text>
<text x="-135.255" y="43.18" size="1.778" layer="95" align="center">LRU_to_controller</text>
<text x="-186.055" y="15.875" size="1.778" layer="95" align="center">MASTER.ADDR</text>
<polygon width="0.1524" layer="94">
<vertex x="-299.72" y="57.15"/>
<vertex x="-298.45" y="55.88"/>
<vertex x="-298.45" y="58.42"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-299.72" y="50.165"/>
<vertex x="-298.45" y="48.895"/>
<vertex x="-298.45" y="51.435"/>
</polygon>
<text x="-275.59" y="57.15" size="1.778" layer="95" align="center">Load_LRU</text>
<text x="-275.59" y="50.165" size="1.778" layer="95" align="center">LRU_in_bit</text>
<text x="5.08" y="69.215" size="1.778" layer="95" align="center">TAG_OUT1</text>
<text x="187.325" y="68.58" size="1.778" layer="95" align="center">TAG_OUT2</text>
<polygon width="0.1524" layer="94">
<vertex x="53.34" y="-49.53"/>
<vertex x="54.61" y="-48.26"/>
<vertex x="52.07" y="-48.26"/>
</polygon>
<text x="38.1" y="158.115" size="12.7" layer="94">L2_CACHE</text>
<text x="147.955" y="-60.325" size="1.778" layer="95" align="center">SLAVE_SDATA</text>
<text x="66.04" y="107.95" size="1.778" layer="95" align="center">SLAVE_MDATA</text>
<polygon width="0.1524" layer="94">
<vertex x="-167.64" y="45.72"/>
<vertex x="-166.37" y="46.99"/>
<vertex x="-168.91" y="46.99"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-157.48" y="45.72"/>
<vertex x="-156.21" y="46.99"/>
<vertex x="-158.75" y="46.99"/>
</polygon>
<text x="-153.035" y="55.245" size="1.778" layer="95" align="center">{TAG_OUT3,SLAVE_ADDR[8:5],00000}</text>
<text x="-140.335" y="52.07" size="1.778" layer="95" align="center">{TAG_OUT4,SLAVE_ADDR[8:5],00000}</text>
</plain>
<instances>
</instances>
<busses>
</busses>
<nets>
<net name="N$12" class="0">
<segment>
<wire x1="-67.945" y1="116.84" x2="-67.945" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<wire x1="-34.925" y1="116.84" x2="-34.925" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<wire x1="-1.905" y1="116.84" x2="-1.905" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<wire x1="-1.905" y1="59.69" x2="-1.905" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<wire x1="-6.985" y1="54.61" x2="-8.89" y2="54.61" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="54.61" x2="-13.335" y2="54.61" width="0.1524" layer="91"/>
<wire x1="-14.605" y1="87.63" x2="-18.415" y2="87.63" width="0.1524" layer="91"/>
<wire x1="-18.415" y1="87.63" x2="-18.415" y2="72.39" width="0.1524" layer="91"/>
<wire x1="-18.415" y1="72.39" x2="-8.89" y2="72.39" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="72.39" x2="-8.89" y2="54.61" width="0.1524" layer="91"/>
<junction x="-8.89" y="54.61"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<wire x1="-32.385" y1="17.78" x2="-32.385" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-32.385" y1="20.32" x2="-1.905" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-1.905" y1="20.32" x2="-1.905" y2="49.53" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<wire x1="-67.945" y1="76.2" x2="-67.945" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-67.945" y1="20.32" x2="-34.925" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-34.925" y1="20.32" x2="-34.925" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<wire x1="32.385" y1="116.84" x2="32.385" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<wire x1="113.665" y1="116.84" x2="113.665" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<wire x1="146.685" y1="116.84" x2="146.685" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<wire x1="179.705" y1="116.84" x2="179.705" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<wire x1="179.705" y1="59.69" x2="179.705" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<wire x1="174.625" y1="54.61" x2="171.45" y2="54.61" width="0.1524" layer="91"/>
<wire x1="171.45" y1="54.61" x2="168.275" y2="54.61" width="0.1524" layer="91"/>
<wire x1="167.005" y1="85.725" x2="167.005" y2="85.09" width="0.1524" layer="91"/>
<wire x1="167.005" y1="85.09" x2="164.465" y2="85.09" width="0.1524" layer="91"/>
<wire x1="164.465" y1="85.09" x2="164.465" y2="69.215" width="0.1524" layer="91"/>
<wire x1="164.465" y1="69.215" x2="171.45" y2="69.215" width="0.1524" layer="91"/>
<wire x1="171.45" y1="69.215" x2="171.45" y2="54.61" width="0.1524" layer="91"/>
<junction x="171.45" y="54.61"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<wire x1="149.225" y1="17.78" x2="149.225" y2="20.32" width="0.1524" layer="91"/>
<wire x1="149.225" y1="20.32" x2="179.705" y2="20.32" width="0.1524" layer="91"/>
<wire x1="179.705" y1="20.32" x2="179.705" y2="49.53" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<wire x1="113.665" y1="76.2" x2="113.665" y2="20.32" width="0.1524" layer="91"/>
<wire x1="113.665" y1="20.32" x2="146.685" y2="20.32" width="0.1524" layer="91"/>
<wire x1="146.685" y1="20.32" x2="146.685" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<wire x1="212.725" y1="116.84" x2="212.725" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<wire x1="52.07" y1="-15.24" x2="52.07" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="52.07" y1="-12.7" x2="-33.655" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-33.655" y1="-12.7" x2="-33.655" y2="11.43" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<wire x1="154.305" y1="-50.165" x2="159.385" y2="-50.165" width="0.1524" layer="91"/>
<wire x1="54.61" y1="-15.24" x2="54.61" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="54.61" y1="-12.7" x2="147.955" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="147.955" y1="-12.7" x2="147.955" y2="11.43" width="0.1524" layer="91"/>
<wire x1="159.385" y1="-50.165" x2="159.385" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="159.385" y1="-12.7" x2="147.955" y2="-12.7" width="0.1524" layer="91"/>
<junction x="147.955" y="-12.7"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<wire x1="145.415" y1="-47.625" x2="145.415" y2="-1.905" width="0.1524" layer="91"/>
<wire x1="145.415" y1="-1.905" x2="32.385" y2="-1.905" width="0.1524" layer="91"/>
<wire x1="32.385" y1="-1.905" x2="32.385" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<wire x1="151.13" y1="-47.625" x2="151.13" y2="-1.905" width="0.1524" layer="91"/>
<wire x1="151.13" y1="-1.905" x2="212.725" y2="-1.905" width="0.1524" layer="91"/>
<wire x1="212.725" y1="-1.905" x2="212.725" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<wire x1="53.34" y1="-20.32" x2="53.34" y2="-26.67" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<wire x1="-312.42" y1="63.5" x2="-312.42" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$103" class="0">
<segment>
<wire x1="-319.405" y1="19.685" x2="-312.42" y2="19.685" width="0.1524" layer="91"/>
<wire x1="-312.42" y1="19.685" x2="-312.42" y2="22.86" width="0.1524" layer="91"/>
<junction x="-312.42" y="19.685"/>
<wire x1="-312.42" y1="19.685" x2="-305.435" y2="19.685" width="0.1524" layer="91"/>
<wire x1="-305.435" y1="19.685" x2="-273.685" y2="19.685" width="0.1524" layer="91"/>
<wire x1="-273.685" y1="19.685" x2="-273.685" y2="28.575" width="0.1524" layer="91"/>
<wire x1="-319.405" y1="9.525" x2="-319.405" y2="19.685" width="0.1524" layer="91"/>
<wire x1="-305.435" y1="14.605" x2="-305.435" y2="19.685" width="0.1524" layer="91"/>
<junction x="-305.435" y="19.685"/>
</segment>
</net>
<net name="N$104" class="0">
<segment>
<wire x1="-312.42" y1="-5.08" x2="-312.42" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-318.135" y1="3.175" x2="-318.135" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-318.135" y1="-2.54" x2="-312.42" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$130" class="0">
<segment>
<wire x1="-309.88" y1="-5.08" x2="-309.88" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-309.88" y1="-2.54" x2="-304.165" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-304.165" y1="-2.54" x2="-304.165" y2="3.175" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$131" class="0">
<segment>
<wire x1="-35.56" y1="76.2" x2="-35.56" y2="69.85" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$132" class="0">
<segment>
<wire x1="147.32" y1="76.2" x2="147.32" y2="69.85" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$133" class="0">
<segment>
<wire x1="-302.895" y1="9.525" x2="-302.895" y2="15.875" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$134" class="0">
<segment>
<wire x1="-316.865" y1="9.525" x2="-316.865" y2="15.875" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$135" class="0">
<segment>
<wire x1="-311.15" y1="-10.16" x2="-311.15" y2="-13.335" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$136" class="0">
<segment>
<wire x1="45.085" y1="107.95" x2="57.15" y2="107.95" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$137" class="0">
<segment>
<wire x1="45.085" y1="111.76" x2="63.5" y2="111.76" width="0.1524" layer="91"/>
<wire x1="63.5" y1="111.76" x2="63.5" y2="127.635" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$138" class="0">
<segment>
<wire x1="225.425" y1="107.95" x2="237.49" y2="107.95" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$139" class="0">
<segment>
<wire x1="225.425" y1="111.76" x2="243.84" y2="111.76" width="0.1524" layer="91"/>
<wire x1="243.84" y1="111.76" x2="243.84" y2="127.635" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$140" class="0">
<segment>
<wire x1="50.8" y1="-29.21" x2="50.8" y2="-26.67" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$141" class="0">
<segment>
<wire x1="53.34" y1="-29.21" x2="53.34" y2="-26.67" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$142" class="0">
<segment>
<wire x1="52.07" y1="-41.91" x2="52.07" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$143" class="0">
<segment>
<wire x1="54.61" y1="-41.91" x2="54.61" y2="-39.37" width="0.1524" layer="91"/>
<wire x1="54.61" y1="-39.37" x2="60.325" y2="-39.37" width="0.1524" layer="91"/>
<wire x1="60.325" y1="-39.37" x2="60.325" y2="-34.925" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$144" class="0">
<segment>
<wire x1="53.34" y1="-46.99" x2="53.34" y2="-49.53" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$145" class="0">
<segment>
<wire x1="147.955" y1="-52.705" x2="147.955" y2="-57.785" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$146" class="0">
<segment>
<wire x1="-11.43" y1="116.84" x2="-11.43" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$147" class="0">
<segment>
<wire x1="170.18" y1="116.84" x2="170.18" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$148" class="0">
<segment>
<wire x1="-45.085" y1="116.84" x2="-45.085" y2="130.81" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$149" class="0">
<segment>
<wire x1="-305.435" y1="9.525" x2="-305.435" y2="11.43" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$150" class="0">
<segment>
<wire x1="138.43" y1="116.84" x2="138.43" y2="130.81" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$151" class="0">
<segment>
<wire x1="-48.895" y1="107.95" x2="-50.8" y2="107.95" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="107.95" x2="-50.8" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$152" class="0">
<segment>
<wire x1="132.715" y1="107.95" x2="130.81" y2="107.95" width="0.1524" layer="91"/>
<wire x1="130.81" y1="107.95" x2="130.81" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$153" class="0">
<segment>
<wire x1="-76.835" y1="116.84" x2="-76.835" y2="130.81" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$154" class="0">
<segment>
<wire x1="104.775" y1="116.84" x2="104.775" y2="130.81" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$155" class="0">
<segment>
<wire x1="-188.595" y1="30.48" x2="-188.595" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$156" class="0">
<segment>
<wire x1="-186.055" y1="24.13" x2="-186.055" y2="19.05" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$158" class="0">
<segment>
<wire x1="-186.055" y1="45.72" x2="-186.055" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$161" class="0">
<segment>
<wire x1="-179.705" y1="26.67" x2="-172.72" y2="26.67" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$162" class="0">
<segment>
<wire x1="-299.72" y1="57.15" x2="-283.21" y2="57.15" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$163" class="0">
<segment>
<wire x1="-299.72" y1="50.165" x2="-283.21" y2="50.165" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$157" class="0">
<segment>
<wire x1="-177.8" y1="46.99" x2="-177.8" y2="57.785" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$164" class="0">
<segment>
<wire x1="-167.64" y1="46.99" x2="-167.64" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$165" class="0">
<segment>
<wire x1="-157.48" y1="46.99" x2="-157.48" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$160" class="0">
<segment>
<wire x1="-153.035" y1="43.18" x2="-146.05" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$159" class="0">
<segment>
<wire x1="-183.515" y1="30.48" x2="-183.515" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
