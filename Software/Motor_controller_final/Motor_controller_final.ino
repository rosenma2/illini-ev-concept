#define DRIVERENABLE 7 //Enable pin on driver IC
#define DRIVER_SS 8 //Driver IC's SPI CS
#define PHASE_A_PWM 10
#define PHASE_B_PWM 9
#define PHASE_C_PWM 2
#include <SPI.h>
void setup() {
  Serial.begin(9600);
  analogReference(EXTERNAL);
  //enable driver
  pinMode(DRIVERENABLE, OUTPUT);
  digitalWrite(DRIVERENABLE, HIGH);
  //prep SPI
  pinMode(DRIVER_SS, OUTPUT);
  digitalWrite(DRIVER_SS, HIGH);
  SPI.begin();
  //SPI.setClockDivider(SPI_CLOCK_DIV128);
  //set driver regs
  SPI.beginTransaction(SPISettings(9600, MSBFIRST, SPI_MODE1));
  digitalWrite(DRIVER_SS, 0);
  SPI.transfer16(0x1021);//clear fault, disable UVLO
  digitalWrite(DRIVER_SS, 1);
  delay(1);//
  digitalWrite(DRIVER_SS, 0);
  SPI.transfer16(0x1021);//clear fault, disable UVLO
  digitalWrite(DRIVER_SS, 1);
  delay(1);//
  digitalWrite(DRIVER_SS, 0);
  SPI.transfer16(0x1811);//clear fault, disable UVLO
  digitalWrite(DRIVER_SS, 1);
  delay(1);//
  digitalWrite(DRIVER_SS, 0);
  SPI.transfer16(0x2711);//clear fault, disable UVLO
  digitalWrite(DRIVER_SS, 1);
  delay(1);//
  digitalWrite(DRIVER_SS, 0);
  SPI.transfer16(0x29FF);//clear fault, disable UVLO
  digitalWrite(DRIVER_SS, 1);
  SPI.endTransaction();
  //set PWMpins as output
  pinMode(PHASE_A_PWM, OUTPUT);
  pinMode(PHASE_B_PWM, OUTPUT);
  pinMode(PHASE_C_PWM, OUTPUT);
  digitalWrite(PHASE_A_PWM,LOW);
  digitalWrite(PHASE_B_PWM,LOW);
  digitalWrite(PHASE_C_PWM,LOW);
  //SPI.beginTransaction(SPISettings(9600, MSBFIRST, SPI_MODE1));
  setPwmFrequency(PHASE_B_PWM,1);
  analogWrite(PHASE_B_PWM,0);
}
byte pwm = 0;
void loop() {
  SPI.beginTransaction(SPISettings(9600, MSBFIRST, SPI_MODE1));
  for (int x = 0; x < 8; x++) {
    digitalWrite(DRIVER_SS, 0);
    word result = SPI.transfer16(0x8000 + 0x0800 * x);
    digitalWrite(DRIVER_SS, 1);
    Serial.print(String(x) + ": ");
    Serial.println(result, HEX);

    //delay(10);
  }
  delay(1000);
  SPI.endTransaction();
  setPwmFrequency(PHASE_B_PWM,1);
  analogWrite(PHASE_B_PWM,pwm);
  if(pwm<128){
    pwm+=5;
  }

}

void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x07; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}
