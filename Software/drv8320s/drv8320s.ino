#include <SPI.h>
void setup() {
  // put your setup code here, to run once:
  pinMode(10,OUTPUT);
  pinMode(11,OUTPUT);
  pinMode(12,INPUT_PULLUP);
  //digitalWrite(12,1);
  pinMode(13,OUTPUT);
  pinMode(6,INPUT_PULLUP);
  digitalWrite(10,1);
  SPI.begin();
  SPI.setClockDivider(SPI_CLOCK_DIV128);
  Serial.begin(9600);
  SPI.beginTransaction(SPISettings(9600, MSBFIRST, SPI_MODE1));
  digitalWrite(10,0);
  SPI.transfer16(0x1021);//clear fault, disable UVLO
  digitalWrite(10,1);
  delay(1);//
  digitalWrite(10,0);
  SPI.transfer16(0x1844);//clear fault, disable UVLO
  digitalWrite(10,1);
  delay(1);//
  digitalWrite(10,0);
  SPI.transfer16(0x2744);//clear fault, disable UVLO
  digitalWrite(10,1);
  SPI.endTransaction();
  //setPwmFrequency(3,1);
  analogWrite(3,128);
  SPI.beginTransaction(SPISettings(9600, MSBFIRST, SPI_MODE1));
  for(int x=0;x<8;x++){
  digitalWrite(10,0);
  word result = SPI.transfer16(0x8000+0x0800*x);
  digitalWrite(10,1);
  Serial.print(String(x)+": ");
  Serial.println(result,HEX);
  
  delay(10);
  }
  delay(1000);
SPI.endTransaction();
}

void loop() {
  // put your main code here, to run repeatedly:
  SPI.beginTransaction(SPISettings(9600, MSBFIRST, SPI_MODE1));
  for(int x=0;x<8;x++){
  digitalWrite(10,0);
  word result = SPI.transfer16(0x8000+0x0800*x);
  digitalWrite(10,1);
  Serial.print(String(x)+": ");
  Serial.println(result,HEX);
  
  delay(10);
  }
  delay(1000);
  SPI.endTransaction();
}

