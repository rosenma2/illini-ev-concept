<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.4.3">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="LED BOARD">
<packages>
<package name="SK6812">
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<smd name="P$1" x="-2.6" y="1.6" dx="1.75" dy="1.2" layer="1"/>
<smd name="P$2" x="-2.6" y="-1.6" dx="1.75" dy="1.2" layer="1"/>
<smd name="P$3" x="2.6" y="1.6" dx="1.75" dy="1.2" layer="1"/>
<smd name="P$4" x="2.6" y="-1.6" dx="1.75" dy="1.2" layer="1"/>
<circle x="1.2" y="-2.2" radius="0.2" width="0.127" layer="21"/>
<text x="3.81" y="-2.54" size="1.27" layer="25">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="SK6812">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="4DOUT" x="-12.7" y="-5.08" length="middle"/>
<pin name="3VDD" x="-12.7" y="5.08" length="middle"/>
<pin name="2DIN" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="1VSS" x="12.7" y="-5.08" length="middle" rot="R180"/>
<text x="-3.81" y="0" size="1.778" layer="94">SK6812</text>
<text x="15.24" y="-7.62" size="1.778" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SK6812">
<gates>
<gate name="G$1" symbol="SK6812" x="50.8" y="-12.7"/>
</gates>
<devices>
<device name="" package="SK6812">
<connects>
<connect gate="G$1" pin="1VSS" pad="P$4"/>
<connect gate="G$1" pin="2DIN" pad="P$3"/>
<connect gate="G$1" pin="3VDD" pad="P$1"/>
<connect gate="G$1" pin="4DOUT" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-molex" urn="urn:adsk.eagle:library:165">
<description>&lt;b&gt;Molex Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="22-23-2021" library_version="1">
<description>.100" (2.54mm) Center Headers - 2 Pin</description>
<wire x1="-2.54" y1="3.175" x2="2.54" y2="3.175" width="0.254" layer="21"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-3.175" width="0.254" layer="21"/>
<wire x1="2.54" y1="-3.175" x2="-2.54" y2="-3.175" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-3.175" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="3.175" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1" shape="long" rot="R90"/>
<text x="-2.54" y="3.81" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="MV" library_version="1">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="M" library_version="1">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="22-23-2021" prefix="X" library_version="1">
<description>.100" (2.54mm) Center Header - 2 Pin</description>
<gates>
<gate name="-1" symbol="MV" x="0" y="0" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="22-23-2021">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MOLEX" constant="no"/>
<attribute name="MPN" value="22-23-2021" constant="no"/>
<attribute name="OC_FARNELL" value="1462926" constant="no"/>
<attribute name="OC_NEWARK" value="25C3832" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BMS">
<packages>
<package name="BINDINGPOST">
<pad name="P$1" x="0" y="0" drill="4.5" diameter="8"/>
</package>
</packages>
<symbols>
<symbol name="BINDINGPOST">
<circle x="0" y="0" radius="5.08" width="0.254" layer="94"/>
<pin name="P$1" x="0" y="0" length="point"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BINDINGPOST">
<gates>
<gate name="G$1" symbol="BINDINGPOST" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BINDINGPOST">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$2" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$3" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$4" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$5" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$6" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$7" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$8" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$9" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$10" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$11" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$12" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$13" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$14" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$15" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$16" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$17" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$18" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$19" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$20" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$21" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$22" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$23" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$24" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$25" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$26" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$27" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$28" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$29" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$30" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$31" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$44" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$45" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$58" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$59" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$72" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="DATA_IN" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2021" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$73" library="BMS" deviceset="BINDINGPOST" device=""/>
<part name="U$74" library="BMS" deviceset="BINDINGPOST" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$32" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$33" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$34" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$35" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$36" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$37" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$38" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$39" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$40" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$41" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$42" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$43" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$46" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$47" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$48" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$49" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$50" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$51" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$52" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$53" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$54" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$55" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$56" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$57" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$60" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$61" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$62" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$63" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$64" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$65" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="P+7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$66" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$67" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$68" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$69" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$70" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$71" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="P+8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="7.62" y="81.28"/>
<instance part="U$2" gate="G$1" x="38.1" y="81.28"/>
<instance part="U$3" gate="G$1" x="68.58" y="81.28"/>
<instance part="U$4" gate="G$1" x="99.06" y="81.28"/>
<instance part="U$5" gate="G$1" x="129.54" y="81.28"/>
<instance part="U$6" gate="G$1" x="160.02" y="81.28"/>
<instance part="U$7" gate="G$1" x="190.5" y="81.28"/>
<instance part="U$8" gate="G$1" x="220.98" y="81.28"/>
<instance part="U$9" gate="G$1" x="251.46" y="81.28"/>
<instance part="U$10" gate="G$1" x="281.94" y="81.28"/>
<instance part="U$11" gate="G$1" x="7.62" y="55.88" rot="R90"/>
<instance part="U$12" gate="G$1" x="7.62" y="25.4" rot="R90"/>
<instance part="U$13" gate="G$1" x="7.62" y="-66.04" rot="R90"/>
<instance part="U$14" gate="G$1" x="7.62" y="-35.56" rot="R90"/>
<instance part="U$15" gate="G$1" x="7.62" y="-5.08" rot="R90"/>
<instance part="U$16" gate="G$1" x="281.94" y="-91.44" rot="R180"/>
<instance part="U$17" gate="G$1" x="251.46" y="-91.44" rot="R180"/>
<instance part="U$18" gate="G$1" x="220.98" y="-91.44" rot="R180"/>
<instance part="U$19" gate="G$1" x="190.5" y="-91.44" rot="R180"/>
<instance part="U$20" gate="G$1" x="160.02" y="-91.44" rot="R180"/>
<instance part="U$21" gate="G$1" x="129.54" y="-91.44" rot="R180"/>
<instance part="U$22" gate="G$1" x="99.06" y="-91.44" rot="R180"/>
<instance part="U$23" gate="G$1" x="68.58" y="-91.44" rot="R180"/>
<instance part="U$24" gate="G$1" x="38.1" y="-91.44" rot="R180"/>
<instance part="U$25" gate="G$1" x="7.62" y="-91.44" rot="R180"/>
<instance part="U$26" gate="G$1" x="281.94" y="-66.04" rot="R270"/>
<instance part="U$27" gate="G$1" x="281.94" y="-35.56" rot="R270"/>
<instance part="U$28" gate="G$1" x="281.94" y="55.88" rot="R270"/>
<instance part="U$29" gate="G$1" x="281.94" y="25.4" rot="R270"/>
<instance part="U$30" gate="G$1" x="281.94" y="-5.08" rot="R270"/>
<instance part="U$31" gate="G$1" x="45.72" y="45.72"/>
<instance part="U$44" gate="G$1" x="45.72" y="25.4"/>
<instance part="U$45" gate="G$1" x="45.72" y="5.08"/>
<instance part="U$58" gate="G$1" x="45.72" y="-15.24"/>
<instance part="U$59" gate="G$1" x="45.72" y="-35.56"/>
<instance part="U$72" gate="G$1" x="45.72" y="-55.88"/>
<instance part="DATA_IN" gate="-1" x="309.88" y="86.36"/>
<instance part="DATA_IN" gate="-2" x="309.88" y="83.82"/>
<instance part="GND1" gate="1" x="304.8" y="76.2"/>
<instance part="U$73" gate="G$1" x="307.34" y="55.88"/>
<instance part="U$74" gate="G$1" x="307.34" y="33.02"/>
<instance part="P+1" gate="VCC" x="314.96" y="66.04"/>
<instance part="GND2" gate="1" x="314.96" y="25.4"/>
<instance part="P+2" gate="VCC" x="30.48" y="58.42"/>
<instance part="GND3" gate="1" x="60.96" y="-66.04"/>
<instance part="U$32" gate="G$1" x="78.74" y="-55.88" rot="R180"/>
<instance part="U$33" gate="G$1" x="78.74" y="-35.56" rot="R180"/>
<instance part="U$34" gate="G$1" x="78.74" y="-15.24" rot="R180"/>
<instance part="U$35" gate="G$1" x="78.74" y="5.08" rot="R180"/>
<instance part="U$36" gate="G$1" x="78.74" y="25.4" rot="R180"/>
<instance part="U$37" gate="G$1" x="78.74" y="45.72" rot="R180"/>
<instance part="P+3" gate="VCC" x="93.98" y="-68.58" rot="R180"/>
<instance part="GND4" gate="1" x="63.5" y="55.88" rot="R180"/>
<instance part="U$38" gate="G$1" x="111.76" y="45.72"/>
<instance part="U$39" gate="G$1" x="111.76" y="25.4"/>
<instance part="U$40" gate="G$1" x="111.76" y="5.08"/>
<instance part="U$41" gate="G$1" x="111.76" y="-15.24"/>
<instance part="U$42" gate="G$1" x="111.76" y="-35.56"/>
<instance part="U$43" gate="G$1" x="111.76" y="-55.88"/>
<instance part="P+4" gate="VCC" x="96.52" y="58.42"/>
<instance part="GND5" gate="1" x="127" y="-66.04"/>
<instance part="U$46" gate="G$1" x="144.78" y="-55.88" rot="R180"/>
<instance part="U$47" gate="G$1" x="144.78" y="-35.56" rot="R180"/>
<instance part="U$48" gate="G$1" x="144.78" y="-15.24" rot="R180"/>
<instance part="U$49" gate="G$1" x="144.78" y="5.08" rot="R180"/>
<instance part="U$50" gate="G$1" x="144.78" y="25.4" rot="R180"/>
<instance part="U$51" gate="G$1" x="144.78" y="45.72" rot="R180"/>
<instance part="P+5" gate="VCC" x="160.02" y="-68.58" rot="R180"/>
<instance part="GND6" gate="1" x="129.54" y="55.88" rot="R180"/>
<instance part="U$52" gate="G$1" x="177.8" y="45.72"/>
<instance part="U$53" gate="G$1" x="177.8" y="25.4"/>
<instance part="U$54" gate="G$1" x="177.8" y="5.08"/>
<instance part="U$55" gate="G$1" x="177.8" y="-15.24"/>
<instance part="U$56" gate="G$1" x="177.8" y="-35.56"/>
<instance part="U$57" gate="G$1" x="177.8" y="-55.88"/>
<instance part="P+6" gate="VCC" x="162.56" y="58.42"/>
<instance part="GND7" gate="1" x="193.04" y="-66.04"/>
<instance part="U$60" gate="G$1" x="210.82" y="-55.88" rot="R180"/>
<instance part="U$61" gate="G$1" x="210.82" y="-35.56" rot="R180"/>
<instance part="U$62" gate="G$1" x="210.82" y="-15.24" rot="R180"/>
<instance part="U$63" gate="G$1" x="210.82" y="5.08" rot="R180"/>
<instance part="U$64" gate="G$1" x="210.82" y="25.4" rot="R180"/>
<instance part="U$65" gate="G$1" x="210.82" y="45.72" rot="R180"/>
<instance part="P+7" gate="VCC" x="226.06" y="-68.58" rot="R180"/>
<instance part="GND8" gate="1" x="195.58" y="55.88" rot="R180"/>
<instance part="U$66" gate="G$1" x="243.84" y="45.72"/>
<instance part="U$67" gate="G$1" x="243.84" y="25.4"/>
<instance part="U$68" gate="G$1" x="243.84" y="5.08"/>
<instance part="U$69" gate="G$1" x="243.84" y="-15.24"/>
<instance part="U$70" gate="G$1" x="243.84" y="-35.56"/>
<instance part="U$71" gate="G$1" x="243.84" y="-55.88"/>
<instance part="P+8" gate="VCC" x="228.6" y="58.42"/>
<instance part="GND9" gate="1" x="259.08" y="-66.04"/>
<instance part="P+9" gate="VCC" x="299.72" y="96.52"/>
<instance part="GND10" gate="1" x="-7.62" y="-88.9"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$2" class="0">
<segment>
<pinref part="U$10" gate="G$1" pin="2DIN"/>
<pinref part="DATA_IN" gate="-1" pin="S"/>
<wire x1="294.64" y1="86.36" x2="307.34" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="DATA_IN" gate="-2" pin="S"/>
<wire x1="307.34" y1="83.82" x2="304.8" y2="83.82" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="304.8" y1="83.82" x2="304.8" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U$28" gate="G$1" pin="1VSS"/>
<wire x1="304.8" y1="81.28" x2="304.8" y2="78.74" width="0.1524" layer="91"/>
<wire x1="276.86" y1="43.18" x2="269.24" y2="43.18" width="0.1524" layer="91"/>
<wire x1="269.24" y1="43.18" x2="266.7" y2="40.64" width="0.1524" layer="91"/>
<wire x1="266.7" y1="40.64" x2="266.7" y2="12.7" width="0.1524" layer="91"/>
<wire x1="266.7" y1="12.7" x2="266.7" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="266.7" y1="-17.78" x2="266.7" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="266.7" y1="-48.26" x2="266.7" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="266.7" y1="-73.66" x2="264.16" y2="-76.2" width="0.1524" layer="91"/>
<wire x1="264.16" y1="-76.2" x2="261.62" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="261.62" y1="-78.74" x2="236.22" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="236.22" y1="-78.74" x2="208.28" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-78.74" x2="177.8" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="177.8" y1="-78.74" x2="147.32" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="147.32" y1="-78.74" x2="116.84" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-78.74" x2="86.36" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-78.74" x2="53.34" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-78.74" x2="22.86" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-78.74" x2="22.86" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-48.26" x2="22.86" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-17.78" x2="22.86" y2="12.7" width="0.1524" layer="91"/>
<wire x1="22.86" y1="12.7" x2="22.86" y2="45.72" width="0.1524" layer="91"/>
<wire x1="22.86" y1="45.72" x2="22.86" y2="63.5" width="0.1524" layer="91"/>
<wire x1="22.86" y1="63.5" x2="22.86" y2="68.58" width="0.1524" layer="91"/>
<wire x1="22.86" y1="68.58" x2="25.4" y2="68.58" width="0.1524" layer="91"/>
<wire x1="25.4" y1="68.58" x2="53.34" y2="68.58" width="0.1524" layer="91"/>
<wire x1="53.34" y1="68.58" x2="83.82" y2="68.58" width="0.1524" layer="91"/>
<wire x1="83.82" y1="68.58" x2="114.3" y2="68.58" width="0.1524" layer="91"/>
<wire x1="114.3" y1="68.58" x2="144.78" y2="68.58" width="0.1524" layer="91"/>
<wire x1="144.78" y1="68.58" x2="175.26" y2="68.58" width="0.1524" layer="91"/>
<wire x1="175.26" y1="68.58" x2="205.74" y2="68.58" width="0.1524" layer="91"/>
<wire x1="205.74" y1="68.58" x2="236.22" y2="68.58" width="0.1524" layer="91"/>
<wire x1="236.22" y1="68.58" x2="264.16" y2="68.58" width="0.1524" layer="91"/>
<wire x1="264.16" y1="68.58" x2="266.7" y2="68.58" width="0.1524" layer="91"/>
<wire x1="266.7" y1="68.58" x2="269.24" y2="71.12" width="0.1524" layer="91"/>
<wire x1="269.24" y1="71.12" x2="294.64" y2="71.12" width="0.1524" layer="91"/>
<wire x1="294.64" y1="71.12" x2="299.72" y2="71.12" width="0.1524" layer="91"/>
<wire x1="299.72" y1="71.12" x2="299.72" y2="81.28" width="0.1524" layer="91"/>
<wire x1="299.72" y1="81.28" x2="304.8" y2="81.28" width="0.1524" layer="91"/>
<junction x="304.8" y="81.28"/>
<pinref part="U$10" gate="G$1" pin="1VSS"/>
<wire x1="294.64" y1="76.2" x2="294.64" y2="71.12" width="0.1524" layer="91"/>
<junction x="294.64" y="71.12"/>
<pinref part="U$9" gate="G$1" pin="1VSS"/>
<wire x1="264.16" y1="76.2" x2="264.16" y2="68.58" width="0.1524" layer="91"/>
<junction x="264.16" y="68.58"/>
<pinref part="U$8" gate="G$1" pin="1VSS"/>
<wire x1="233.68" y1="76.2" x2="233.68" y2="71.12" width="0.1524" layer="91"/>
<wire x1="233.68" y1="71.12" x2="236.22" y2="68.58" width="0.1524" layer="91"/>
<junction x="236.22" y="68.58"/>
<pinref part="U$7" gate="G$1" pin="1VSS"/>
<wire x1="203.2" y1="76.2" x2="203.2" y2="71.12" width="0.1524" layer="91"/>
<wire x1="203.2" y1="71.12" x2="205.74" y2="68.58" width="0.1524" layer="91"/>
<junction x="205.74" y="68.58"/>
<pinref part="U$6" gate="G$1" pin="1VSS"/>
<wire x1="172.72" y1="76.2" x2="172.72" y2="71.12" width="0.1524" layer="91"/>
<wire x1="172.72" y1="71.12" x2="175.26" y2="68.58" width="0.1524" layer="91"/>
<junction x="175.26" y="68.58"/>
<pinref part="U$5" gate="G$1" pin="1VSS"/>
<wire x1="142.24" y1="76.2" x2="142.24" y2="71.12" width="0.1524" layer="91"/>
<wire x1="142.24" y1="71.12" x2="144.78" y2="68.58" width="0.1524" layer="91"/>
<junction x="144.78" y="68.58"/>
<pinref part="U$4" gate="G$1" pin="1VSS"/>
<wire x1="111.76" y1="76.2" x2="111.76" y2="71.12" width="0.1524" layer="91"/>
<wire x1="111.76" y1="71.12" x2="114.3" y2="68.58" width="0.1524" layer="91"/>
<junction x="114.3" y="68.58"/>
<pinref part="U$3" gate="G$1" pin="1VSS"/>
<wire x1="81.28" y1="76.2" x2="81.28" y2="71.12" width="0.1524" layer="91"/>
<wire x1="81.28" y1="71.12" x2="83.82" y2="68.58" width="0.1524" layer="91"/>
<junction x="83.82" y="68.58"/>
<pinref part="U$2" gate="G$1" pin="1VSS"/>
<wire x1="50.8" y1="76.2" x2="50.8" y2="71.12" width="0.1524" layer="91"/>
<wire x1="50.8" y1="71.12" x2="53.34" y2="68.58" width="0.1524" layer="91"/>
<junction x="53.34" y="68.58"/>
<pinref part="U$1" gate="G$1" pin="1VSS"/>
<wire x1="20.32" y1="76.2" x2="20.32" y2="73.66" width="0.1524" layer="91"/>
<wire x1="20.32" y1="73.66" x2="25.4" y2="68.58" width="0.1524" layer="91"/>
<junction x="25.4" y="68.58"/>
<pinref part="U$11" gate="G$1" pin="1VSS"/>
<wire x1="12.7" y1="68.58" x2="17.78" y2="68.58" width="0.1524" layer="91"/>
<wire x1="17.78" y1="68.58" x2="22.86" y2="63.5" width="0.1524" layer="91"/>
<junction x="22.86" y="63.5"/>
<pinref part="U$12" gate="G$1" pin="1VSS"/>
<wire x1="12.7" y1="38.1" x2="15.24" y2="38.1" width="0.1524" layer="91"/>
<wire x1="15.24" y1="38.1" x2="22.86" y2="45.72" width="0.1524" layer="91"/>
<junction x="22.86" y="45.72"/>
<pinref part="U$15" gate="G$1" pin="1VSS"/>
<wire x1="12.7" y1="7.62" x2="17.78" y2="7.62" width="0.1524" layer="91"/>
<wire x1="17.78" y1="7.62" x2="22.86" y2="12.7" width="0.1524" layer="91"/>
<junction x="22.86" y="12.7"/>
<pinref part="U$14" gate="G$1" pin="1VSS"/>
<wire x1="12.7" y1="-22.86" x2="17.78" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-22.86" x2="22.86" y2="-17.78" width="0.1524" layer="91"/>
<junction x="22.86" y="-17.78"/>
<pinref part="U$13" gate="G$1" pin="1VSS"/>
<wire x1="12.7" y1="-53.34" x2="17.78" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-48.26" x2="22.86" y2="-48.26" width="0.1524" layer="91"/>
<junction x="22.86" y="-48.26"/>
<pinref part="U$24" gate="G$1" pin="1VSS"/>
<wire x1="25.4" y1="-86.36" x2="22.86" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-83.82" x2="22.86" y2="-78.74" width="0.1524" layer="91"/>
<junction x="22.86" y="-78.74"/>
<pinref part="U$23" gate="G$1" pin="1VSS"/>
<wire x1="55.88" y1="-86.36" x2="53.34" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-83.82" x2="53.34" y2="-78.74" width="0.1524" layer="91"/>
<junction x="53.34" y="-78.74"/>
<pinref part="U$22" gate="G$1" pin="1VSS"/>
<wire x1="86.36" y1="-86.36" x2="86.36" y2="-78.74" width="0.1524" layer="91"/>
<junction x="86.36" y="-78.74"/>
<pinref part="U$21" gate="G$1" pin="1VSS"/>
<wire x1="116.84" y1="-86.36" x2="116.84" y2="-78.74" width="0.1524" layer="91"/>
<junction x="116.84" y="-78.74"/>
<pinref part="U$20" gate="G$1" pin="1VSS"/>
<wire x1="147.32" y1="-86.36" x2="147.32" y2="-78.74" width="0.1524" layer="91"/>
<junction x="147.32" y="-78.74"/>
<pinref part="U$19" gate="G$1" pin="1VSS"/>
<wire x1="177.8" y1="-86.36" x2="177.8" y2="-78.74" width="0.1524" layer="91"/>
<junction x="177.8" y="-78.74"/>
<pinref part="U$18" gate="G$1" pin="1VSS"/>
<wire x1="208.28" y1="-86.36" x2="208.28" y2="-78.74" width="0.1524" layer="91"/>
<junction x="208.28" y="-78.74"/>
<pinref part="U$17" gate="G$1" pin="1VSS"/>
<wire x1="238.76" y1="-86.36" x2="236.22" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="236.22" y1="-83.82" x2="236.22" y2="-78.74" width="0.1524" layer="91"/>
<junction x="236.22" y="-78.74"/>
<wire x1="269.24" y1="-83.82" x2="264.16" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="264.16" y1="-78.74" x2="264.16" y2="-76.2" width="0.1524" layer="91"/>
<junction x="264.16" y="-76.2"/>
<pinref part="U$16" gate="G$1" pin="1VSS"/>
<wire x1="269.24" y1="-83.82" x2="269.24" y2="-86.36" width="0.1524" layer="91"/>
<pinref part="U$26" gate="G$1" pin="1VSS"/>
<wire x1="276.86" y1="-78.74" x2="271.78" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="271.78" y1="-73.66" x2="266.7" y2="-73.66" width="0.1524" layer="91"/>
<junction x="266.7" y="-73.66"/>
<pinref part="U$27" gate="G$1" pin="1VSS"/>
<wire x1="276.86" y1="-48.26" x2="266.7" y2="-48.26" width="0.1524" layer="91"/>
<junction x="266.7" y="-48.26"/>
<pinref part="U$30" gate="G$1" pin="1VSS"/>
<wire x1="276.86" y1="-17.78" x2="266.7" y2="-17.78" width="0.1524" layer="91"/>
<junction x="266.7" y="-17.78"/>
<pinref part="U$29" gate="G$1" pin="1VSS"/>
<wire x1="276.86" y1="12.7" x2="266.7" y2="12.7" width="0.1524" layer="91"/>
<junction x="266.7" y="12.7"/>
</segment>
<segment>
<pinref part="U$74" gate="G$1" pin="P$1"/>
<wire x1="307.34" y1="33.02" x2="314.96" y2="33.02" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="314.96" y1="33.02" x2="314.96" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="60.96" y1="-63.5" x2="60.96" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="U$31" gate="G$1" pin="1VSS"/>
<wire x1="60.96" y1="-60.96" x2="60.96" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-40.64" x2="60.96" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-20.32" x2="60.96" y2="0" width="0.1524" layer="91"/>
<wire x1="60.96" y1="0" x2="60.96" y2="20.32" width="0.1524" layer="91"/>
<wire x1="60.96" y1="20.32" x2="60.96" y2="40.64" width="0.1524" layer="91"/>
<wire x1="60.96" y1="40.64" x2="58.42" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$44" gate="G$1" pin="1VSS"/>
<wire x1="58.42" y1="20.32" x2="60.96" y2="20.32" width="0.1524" layer="91"/>
<junction x="60.96" y="20.32"/>
<pinref part="U$45" gate="G$1" pin="1VSS"/>
<wire x1="58.42" y1="0" x2="60.96" y2="0" width="0.1524" layer="91"/>
<junction x="60.96" y="0"/>
<pinref part="U$58" gate="G$1" pin="1VSS"/>
<wire x1="58.42" y1="-20.32" x2="60.96" y2="-20.32" width="0.1524" layer="91"/>
<junction x="60.96" y="-20.32"/>
<pinref part="U$72" gate="G$1" pin="1VSS"/>
<wire x1="58.42" y1="-60.96" x2="60.96" y2="-60.96" width="0.1524" layer="91"/>
<junction x="60.96" y="-60.96"/>
<pinref part="U$59" gate="G$1" pin="1VSS"/>
<wire x1="58.42" y1="-40.64" x2="60.96" y2="-40.64" width="0.1524" layer="91"/>
<junction x="60.96" y="-40.64"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="63.5" y1="53.34" x2="63.5" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U$32" gate="G$1" pin="1VSS"/>
<wire x1="63.5" y1="50.8" x2="63.5" y2="30.48" width="0.1524" layer="91"/>
<wire x1="63.5" y1="30.48" x2="63.5" y2="10.16" width="0.1524" layer="91"/>
<wire x1="63.5" y1="10.16" x2="63.5" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-10.16" x2="63.5" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-30.48" x2="63.5" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-50.8" x2="66.04" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="U$33" gate="G$1" pin="1VSS"/>
<wire x1="66.04" y1="-30.48" x2="63.5" y2="-30.48" width="0.1524" layer="91"/>
<junction x="63.5" y="-30.48"/>
<pinref part="U$34" gate="G$1" pin="1VSS"/>
<wire x1="66.04" y1="-10.16" x2="63.5" y2="-10.16" width="0.1524" layer="91"/>
<junction x="63.5" y="-10.16"/>
<pinref part="U$35" gate="G$1" pin="1VSS"/>
<wire x1="66.04" y1="10.16" x2="63.5" y2="10.16" width="0.1524" layer="91"/>
<junction x="63.5" y="10.16"/>
<pinref part="U$37" gate="G$1" pin="1VSS"/>
<wire x1="66.04" y1="50.8" x2="63.5" y2="50.8" width="0.1524" layer="91"/>
<junction x="63.5" y="50.8"/>
<pinref part="U$36" gate="G$1" pin="1VSS"/>
<wire x1="66.04" y1="30.48" x2="63.5" y2="30.48" width="0.1524" layer="91"/>
<junction x="63.5" y="30.48"/>
</segment>
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="127" y1="-63.5" x2="127" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="U$38" gate="G$1" pin="1VSS"/>
<wire x1="127" y1="-60.96" x2="127" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="127" y1="-40.64" x2="127" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="127" y1="-20.32" x2="127" y2="0" width="0.1524" layer="91"/>
<wire x1="127" y1="0" x2="127" y2="20.32" width="0.1524" layer="91"/>
<wire x1="127" y1="20.32" x2="127" y2="40.64" width="0.1524" layer="91"/>
<wire x1="127" y1="40.64" x2="124.46" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$39" gate="G$1" pin="1VSS"/>
<wire x1="124.46" y1="20.32" x2="127" y2="20.32" width="0.1524" layer="91"/>
<junction x="127" y="20.32"/>
<pinref part="U$40" gate="G$1" pin="1VSS"/>
<wire x1="124.46" y1="0" x2="127" y2="0" width="0.1524" layer="91"/>
<junction x="127" y="0"/>
<pinref part="U$41" gate="G$1" pin="1VSS"/>
<wire x1="124.46" y1="-20.32" x2="127" y2="-20.32" width="0.1524" layer="91"/>
<junction x="127" y="-20.32"/>
<pinref part="U$43" gate="G$1" pin="1VSS"/>
<wire x1="124.46" y1="-60.96" x2="127" y2="-60.96" width="0.1524" layer="91"/>
<junction x="127" y="-60.96"/>
<pinref part="U$42" gate="G$1" pin="1VSS"/>
<wire x1="124.46" y1="-40.64" x2="127" y2="-40.64" width="0.1524" layer="91"/>
<junction x="127" y="-40.64"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="129.54" y1="53.34" x2="129.54" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U$46" gate="G$1" pin="1VSS"/>
<wire x1="129.54" y1="50.8" x2="129.54" y2="30.48" width="0.1524" layer="91"/>
<wire x1="129.54" y1="30.48" x2="129.54" y2="10.16" width="0.1524" layer="91"/>
<wire x1="129.54" y1="10.16" x2="129.54" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="129.54" y1="-10.16" x2="129.54" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="129.54" y1="-30.48" x2="129.54" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="129.54" y1="-50.8" x2="132.08" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="U$47" gate="G$1" pin="1VSS"/>
<wire x1="132.08" y1="-30.48" x2="129.54" y2="-30.48" width="0.1524" layer="91"/>
<junction x="129.54" y="-30.48"/>
<pinref part="U$48" gate="G$1" pin="1VSS"/>
<wire x1="132.08" y1="-10.16" x2="129.54" y2="-10.16" width="0.1524" layer="91"/>
<junction x="129.54" y="-10.16"/>
<pinref part="U$49" gate="G$1" pin="1VSS"/>
<wire x1="132.08" y1="10.16" x2="129.54" y2="10.16" width="0.1524" layer="91"/>
<junction x="129.54" y="10.16"/>
<pinref part="U$51" gate="G$1" pin="1VSS"/>
<wire x1="132.08" y1="50.8" x2="129.54" y2="50.8" width="0.1524" layer="91"/>
<junction x="129.54" y="50.8"/>
<pinref part="U$50" gate="G$1" pin="1VSS"/>
<wire x1="132.08" y1="30.48" x2="129.54" y2="30.48" width="0.1524" layer="91"/>
<junction x="129.54" y="30.48"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="193.04" y1="-63.5" x2="193.04" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="U$52" gate="G$1" pin="1VSS"/>
<wire x1="193.04" y1="-60.96" x2="193.04" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="193.04" y1="-40.64" x2="193.04" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="193.04" y1="-20.32" x2="193.04" y2="0" width="0.1524" layer="91"/>
<wire x1="193.04" y1="0" x2="193.04" y2="20.32" width="0.1524" layer="91"/>
<wire x1="193.04" y1="20.32" x2="193.04" y2="40.64" width="0.1524" layer="91"/>
<wire x1="193.04" y1="40.64" x2="190.5" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$53" gate="G$1" pin="1VSS"/>
<wire x1="190.5" y1="20.32" x2="193.04" y2="20.32" width="0.1524" layer="91"/>
<junction x="193.04" y="20.32"/>
<pinref part="U$54" gate="G$1" pin="1VSS"/>
<wire x1="190.5" y1="0" x2="193.04" y2="0" width="0.1524" layer="91"/>
<junction x="193.04" y="0"/>
<pinref part="U$55" gate="G$1" pin="1VSS"/>
<wire x1="190.5" y1="-20.32" x2="193.04" y2="-20.32" width="0.1524" layer="91"/>
<junction x="193.04" y="-20.32"/>
<pinref part="U$57" gate="G$1" pin="1VSS"/>
<wire x1="190.5" y1="-60.96" x2="193.04" y2="-60.96" width="0.1524" layer="91"/>
<junction x="193.04" y="-60.96"/>
<pinref part="U$56" gate="G$1" pin="1VSS"/>
<wire x1="190.5" y1="-40.64" x2="193.04" y2="-40.64" width="0.1524" layer="91"/>
<junction x="193.04" y="-40.64"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="195.58" y1="53.34" x2="195.58" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U$60" gate="G$1" pin="1VSS"/>
<wire x1="195.58" y1="50.8" x2="195.58" y2="30.48" width="0.1524" layer="91"/>
<wire x1="195.58" y1="30.48" x2="195.58" y2="10.16" width="0.1524" layer="91"/>
<wire x1="195.58" y1="10.16" x2="195.58" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="195.58" y1="-10.16" x2="195.58" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="195.58" y1="-30.48" x2="195.58" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="195.58" y1="-50.8" x2="198.12" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="U$61" gate="G$1" pin="1VSS"/>
<wire x1="198.12" y1="-30.48" x2="195.58" y2="-30.48" width="0.1524" layer="91"/>
<junction x="195.58" y="-30.48"/>
<pinref part="U$62" gate="G$1" pin="1VSS"/>
<wire x1="198.12" y1="-10.16" x2="195.58" y2="-10.16" width="0.1524" layer="91"/>
<junction x="195.58" y="-10.16"/>
<pinref part="U$63" gate="G$1" pin="1VSS"/>
<wire x1="198.12" y1="10.16" x2="195.58" y2="10.16" width="0.1524" layer="91"/>
<junction x="195.58" y="10.16"/>
<pinref part="U$65" gate="G$1" pin="1VSS"/>
<wire x1="198.12" y1="50.8" x2="195.58" y2="50.8" width="0.1524" layer="91"/>
<junction x="195.58" y="50.8"/>
<pinref part="U$64" gate="G$1" pin="1VSS"/>
<wire x1="198.12" y1="30.48" x2="195.58" y2="30.48" width="0.1524" layer="91"/>
<junction x="195.58" y="30.48"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="259.08" y1="-63.5" x2="259.08" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="U$66" gate="G$1" pin="1VSS"/>
<wire x1="259.08" y1="-60.96" x2="259.08" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="259.08" y1="-40.64" x2="259.08" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="259.08" y1="-20.32" x2="259.08" y2="0" width="0.1524" layer="91"/>
<wire x1="259.08" y1="0" x2="259.08" y2="20.32" width="0.1524" layer="91"/>
<wire x1="259.08" y1="20.32" x2="259.08" y2="40.64" width="0.1524" layer="91"/>
<wire x1="259.08" y1="40.64" x2="256.54" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$67" gate="G$1" pin="1VSS"/>
<wire x1="256.54" y1="20.32" x2="259.08" y2="20.32" width="0.1524" layer="91"/>
<junction x="259.08" y="20.32"/>
<pinref part="U$68" gate="G$1" pin="1VSS"/>
<wire x1="256.54" y1="0" x2="259.08" y2="0" width="0.1524" layer="91"/>
<junction x="259.08" y="0"/>
<pinref part="U$69" gate="G$1" pin="1VSS"/>
<wire x1="256.54" y1="-20.32" x2="259.08" y2="-20.32" width="0.1524" layer="91"/>
<junction x="259.08" y="-20.32"/>
<pinref part="U$71" gate="G$1" pin="1VSS"/>
<wire x1="256.54" y1="-60.96" x2="259.08" y2="-60.96" width="0.1524" layer="91"/>
<junction x="259.08" y="-60.96"/>
<pinref part="U$70" gate="G$1" pin="1VSS"/>
<wire x1="256.54" y1="-40.64" x2="259.08" y2="-40.64" width="0.1524" layer="91"/>
<junction x="259.08" y="-40.64"/>
</segment>
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<pinref part="U$25" gate="G$1" pin="1VSS"/>
<wire x1="-7.62" y1="-86.36" x2="-5.08" y2="-86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="314.96" y1="63.5" x2="314.96" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$73" gate="G$1" pin="P$1"/>
<wire x1="314.96" y1="55.88" x2="307.34" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$72" gate="G$1" pin="3VDD"/>
<wire x1="33.02" y1="-50.8" x2="30.48" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="P+2" gate="VCC" pin="VCC"/>
<wire x1="30.48" y1="-50.8" x2="30.48" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="U$59" gate="G$1" pin="3VDD"/>
<wire x1="30.48" y1="-30.48" x2="30.48" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-10.16" x2="30.48" y2="10.16" width="0.1524" layer="91"/>
<wire x1="30.48" y1="10.16" x2="30.48" y2="30.48" width="0.1524" layer="91"/>
<wire x1="30.48" y1="30.48" x2="30.48" y2="50.8" width="0.1524" layer="91"/>
<wire x1="30.48" y1="50.8" x2="30.48" y2="55.88" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-30.48" x2="30.48" y2="-30.48" width="0.1524" layer="91"/>
<junction x="30.48" y="-30.48"/>
<pinref part="U$58" gate="G$1" pin="3VDD"/>
<wire x1="33.02" y1="-10.16" x2="30.48" y2="-10.16" width="0.1524" layer="91"/>
<junction x="30.48" y="-10.16"/>
<pinref part="U$45" gate="G$1" pin="3VDD"/>
<wire x1="33.02" y1="10.16" x2="30.48" y2="10.16" width="0.1524" layer="91"/>
<junction x="30.48" y="10.16"/>
<pinref part="U$44" gate="G$1" pin="3VDD"/>
<wire x1="33.02" y1="30.48" x2="30.48" y2="30.48" width="0.1524" layer="91"/>
<junction x="30.48" y="30.48"/>
<pinref part="U$31" gate="G$1" pin="3VDD"/>
<wire x1="33.02" y1="50.8" x2="30.48" y2="50.8" width="0.1524" layer="91"/>
<junction x="30.48" y="50.8"/>
</segment>
<segment>
<pinref part="U$37" gate="G$1" pin="3VDD"/>
<wire x1="91.44" y1="40.64" x2="93.98" y2="40.64" width="0.1524" layer="91"/>
<pinref part="P+3" gate="VCC" pin="VCC"/>
<wire x1="93.98" y1="40.64" x2="93.98" y2="20.32" width="0.1524" layer="91"/>
<pinref part="U$36" gate="G$1" pin="3VDD"/>
<wire x1="93.98" y1="20.32" x2="93.98" y2="0" width="0.1524" layer="91"/>
<wire x1="93.98" y1="0" x2="93.98" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-20.32" x2="93.98" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-40.64" x2="93.98" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-60.96" x2="93.98" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="91.44" y1="20.32" x2="93.98" y2="20.32" width="0.1524" layer="91"/>
<junction x="93.98" y="20.32"/>
<pinref part="U$35" gate="G$1" pin="3VDD"/>
<wire x1="91.44" y1="0" x2="93.98" y2="0" width="0.1524" layer="91"/>
<junction x="93.98" y="0"/>
<pinref part="U$34" gate="G$1" pin="3VDD"/>
<wire x1="91.44" y1="-20.32" x2="93.98" y2="-20.32" width="0.1524" layer="91"/>
<junction x="93.98" y="-20.32"/>
<pinref part="U$33" gate="G$1" pin="3VDD"/>
<wire x1="91.44" y1="-40.64" x2="93.98" y2="-40.64" width="0.1524" layer="91"/>
<junction x="93.98" y="-40.64"/>
<pinref part="U$32" gate="G$1" pin="3VDD"/>
<wire x1="91.44" y1="-60.96" x2="93.98" y2="-60.96" width="0.1524" layer="91"/>
<junction x="93.98" y="-60.96"/>
</segment>
<segment>
<pinref part="U$43" gate="G$1" pin="3VDD"/>
<wire x1="99.06" y1="-50.8" x2="96.52" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="P+4" gate="VCC" pin="VCC"/>
<wire x1="96.52" y1="-50.8" x2="96.52" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="U$42" gate="G$1" pin="3VDD"/>
<wire x1="96.52" y1="-30.48" x2="96.52" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-10.16" x2="96.52" y2="10.16" width="0.1524" layer="91"/>
<wire x1="96.52" y1="10.16" x2="96.52" y2="30.48" width="0.1524" layer="91"/>
<wire x1="96.52" y1="30.48" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<wire x1="96.52" y1="50.8" x2="96.52" y2="55.88" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-30.48" x2="96.52" y2="-30.48" width="0.1524" layer="91"/>
<junction x="96.52" y="-30.48"/>
<pinref part="U$41" gate="G$1" pin="3VDD"/>
<wire x1="99.06" y1="-10.16" x2="96.52" y2="-10.16" width="0.1524" layer="91"/>
<junction x="96.52" y="-10.16"/>
<pinref part="U$40" gate="G$1" pin="3VDD"/>
<wire x1="99.06" y1="10.16" x2="96.52" y2="10.16" width="0.1524" layer="91"/>
<junction x="96.52" y="10.16"/>
<pinref part="U$39" gate="G$1" pin="3VDD"/>
<wire x1="99.06" y1="30.48" x2="96.52" y2="30.48" width="0.1524" layer="91"/>
<junction x="96.52" y="30.48"/>
<pinref part="U$38" gate="G$1" pin="3VDD"/>
<wire x1="99.06" y1="50.8" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<junction x="96.52" y="50.8"/>
</segment>
<segment>
<pinref part="U$51" gate="G$1" pin="3VDD"/>
<wire x1="157.48" y1="40.64" x2="160.02" y2="40.64" width="0.1524" layer="91"/>
<pinref part="P+5" gate="VCC" pin="VCC"/>
<wire x1="160.02" y1="40.64" x2="160.02" y2="20.32" width="0.1524" layer="91"/>
<pinref part="U$50" gate="G$1" pin="3VDD"/>
<wire x1="160.02" y1="20.32" x2="160.02" y2="0" width="0.1524" layer="91"/>
<wire x1="160.02" y1="0" x2="160.02" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-20.32" x2="160.02" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-40.64" x2="160.02" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-60.96" x2="160.02" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="157.48" y1="20.32" x2="160.02" y2="20.32" width="0.1524" layer="91"/>
<junction x="160.02" y="20.32"/>
<pinref part="U$49" gate="G$1" pin="3VDD"/>
<wire x1="157.48" y1="0" x2="160.02" y2="0" width="0.1524" layer="91"/>
<junction x="160.02" y="0"/>
<pinref part="U$48" gate="G$1" pin="3VDD"/>
<wire x1="157.48" y1="-20.32" x2="160.02" y2="-20.32" width="0.1524" layer="91"/>
<junction x="160.02" y="-20.32"/>
<pinref part="U$47" gate="G$1" pin="3VDD"/>
<wire x1="157.48" y1="-40.64" x2="160.02" y2="-40.64" width="0.1524" layer="91"/>
<junction x="160.02" y="-40.64"/>
<pinref part="U$46" gate="G$1" pin="3VDD"/>
<wire x1="157.48" y1="-60.96" x2="160.02" y2="-60.96" width="0.1524" layer="91"/>
<junction x="160.02" y="-60.96"/>
</segment>
<segment>
<pinref part="U$57" gate="G$1" pin="3VDD"/>
<wire x1="165.1" y1="-50.8" x2="162.56" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="P+6" gate="VCC" pin="VCC"/>
<wire x1="162.56" y1="-50.8" x2="162.56" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="U$56" gate="G$1" pin="3VDD"/>
<wire x1="162.56" y1="-30.48" x2="162.56" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-10.16" x2="162.56" y2="10.16" width="0.1524" layer="91"/>
<wire x1="162.56" y1="10.16" x2="162.56" y2="30.48" width="0.1524" layer="91"/>
<wire x1="162.56" y1="30.48" x2="162.56" y2="50.8" width="0.1524" layer="91"/>
<wire x1="162.56" y1="50.8" x2="162.56" y2="55.88" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-30.48" x2="162.56" y2="-30.48" width="0.1524" layer="91"/>
<junction x="162.56" y="-30.48"/>
<pinref part="U$55" gate="G$1" pin="3VDD"/>
<wire x1="165.1" y1="-10.16" x2="162.56" y2="-10.16" width="0.1524" layer="91"/>
<junction x="162.56" y="-10.16"/>
<pinref part="U$54" gate="G$1" pin="3VDD"/>
<wire x1="165.1" y1="10.16" x2="162.56" y2="10.16" width="0.1524" layer="91"/>
<junction x="162.56" y="10.16"/>
<pinref part="U$53" gate="G$1" pin="3VDD"/>
<wire x1="165.1" y1="30.48" x2="162.56" y2="30.48" width="0.1524" layer="91"/>
<junction x="162.56" y="30.48"/>
<pinref part="U$52" gate="G$1" pin="3VDD"/>
<wire x1="165.1" y1="50.8" x2="162.56" y2="50.8" width="0.1524" layer="91"/>
<junction x="162.56" y="50.8"/>
</segment>
<segment>
<pinref part="U$65" gate="G$1" pin="3VDD"/>
<wire x1="223.52" y1="40.64" x2="226.06" y2="40.64" width="0.1524" layer="91"/>
<pinref part="P+7" gate="VCC" pin="VCC"/>
<wire x1="226.06" y1="40.64" x2="226.06" y2="20.32" width="0.1524" layer="91"/>
<pinref part="U$64" gate="G$1" pin="3VDD"/>
<wire x1="226.06" y1="20.32" x2="226.06" y2="0" width="0.1524" layer="91"/>
<wire x1="226.06" y1="0" x2="226.06" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="226.06" y1="-20.32" x2="226.06" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="226.06" y1="-40.64" x2="226.06" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="226.06" y1="-60.96" x2="226.06" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="223.52" y1="20.32" x2="226.06" y2="20.32" width="0.1524" layer="91"/>
<junction x="226.06" y="20.32"/>
<pinref part="U$63" gate="G$1" pin="3VDD"/>
<wire x1="223.52" y1="0" x2="226.06" y2="0" width="0.1524" layer="91"/>
<junction x="226.06" y="0"/>
<pinref part="U$62" gate="G$1" pin="3VDD"/>
<wire x1="223.52" y1="-20.32" x2="226.06" y2="-20.32" width="0.1524" layer="91"/>
<junction x="226.06" y="-20.32"/>
<pinref part="U$61" gate="G$1" pin="3VDD"/>
<wire x1="223.52" y1="-40.64" x2="226.06" y2="-40.64" width="0.1524" layer="91"/>
<junction x="226.06" y="-40.64"/>
<pinref part="U$60" gate="G$1" pin="3VDD"/>
<wire x1="223.52" y1="-60.96" x2="226.06" y2="-60.96" width="0.1524" layer="91"/>
<junction x="226.06" y="-60.96"/>
</segment>
<segment>
<pinref part="U$71" gate="G$1" pin="3VDD"/>
<wire x1="231.14" y1="-50.8" x2="228.6" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="P+8" gate="VCC" pin="VCC"/>
<wire x1="228.6" y1="-50.8" x2="228.6" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="U$70" gate="G$1" pin="3VDD"/>
<wire x1="228.6" y1="-30.48" x2="228.6" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="228.6" y1="-10.16" x2="228.6" y2="10.16" width="0.1524" layer="91"/>
<wire x1="228.6" y1="10.16" x2="228.6" y2="30.48" width="0.1524" layer="91"/>
<wire x1="228.6" y1="30.48" x2="228.6" y2="50.8" width="0.1524" layer="91"/>
<wire x1="228.6" y1="50.8" x2="228.6" y2="55.88" width="0.1524" layer="91"/>
<wire x1="231.14" y1="-30.48" x2="228.6" y2="-30.48" width="0.1524" layer="91"/>
<junction x="228.6" y="-30.48"/>
<pinref part="U$69" gate="G$1" pin="3VDD"/>
<wire x1="231.14" y1="-10.16" x2="228.6" y2="-10.16" width="0.1524" layer="91"/>
<junction x="228.6" y="-10.16"/>
<pinref part="U$68" gate="G$1" pin="3VDD"/>
<wire x1="231.14" y1="10.16" x2="228.6" y2="10.16" width="0.1524" layer="91"/>
<junction x="228.6" y="10.16"/>
<pinref part="U$67" gate="G$1" pin="3VDD"/>
<wire x1="231.14" y1="30.48" x2="228.6" y2="30.48" width="0.1524" layer="91"/>
<junction x="228.6" y="30.48"/>
<pinref part="U$66" gate="G$1" pin="3VDD"/>
<wire x1="231.14" y1="50.8" x2="228.6" y2="50.8" width="0.1524" layer="91"/>
<junction x="228.6" y="50.8"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="3VDD"/>
<wire x1="-5.08" y1="86.36" x2="-5.08" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="91.44" x2="25.4" y2="91.44" width="0.1524" layer="91"/>
<pinref part="P+9" gate="VCC" pin="VCC"/>
<wire x1="25.4" y1="91.44" x2="55.88" y2="91.44" width="0.1524" layer="91"/>
<wire x1="55.88" y1="91.44" x2="86.36" y2="91.44" width="0.1524" layer="91"/>
<wire x1="86.36" y1="91.44" x2="116.84" y2="91.44" width="0.1524" layer="91"/>
<wire x1="116.84" y1="91.44" x2="147.32" y2="91.44" width="0.1524" layer="91"/>
<wire x1="147.32" y1="91.44" x2="177.8" y2="91.44" width="0.1524" layer="91"/>
<wire x1="177.8" y1="91.44" x2="208.28" y2="91.44" width="0.1524" layer="91"/>
<wire x1="208.28" y1="91.44" x2="238.76" y2="91.44" width="0.1524" layer="91"/>
<wire x1="238.76" y1="91.44" x2="269.24" y2="91.44" width="0.1524" layer="91"/>
<wire x1="269.24" y1="91.44" x2="299.72" y2="91.44" width="0.1524" layer="91"/>
<wire x1="299.72" y1="91.44" x2="299.72" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="3VDD"/>
<wire x1="25.4" y1="86.36" x2="25.4" y2="91.44" width="0.1524" layer="91"/>
<junction x="25.4" y="91.44"/>
<pinref part="U$3" gate="G$1" pin="3VDD"/>
<wire x1="55.88" y1="86.36" x2="55.88" y2="91.44" width="0.1524" layer="91"/>
<junction x="55.88" y="91.44"/>
<pinref part="U$4" gate="G$1" pin="3VDD"/>
<wire x1="86.36" y1="86.36" x2="86.36" y2="91.44" width="0.1524" layer="91"/>
<junction x="86.36" y="91.44"/>
<pinref part="U$5" gate="G$1" pin="3VDD"/>
<wire x1="116.84" y1="86.36" x2="116.84" y2="91.44" width="0.1524" layer="91"/>
<junction x="116.84" y="91.44"/>
<pinref part="U$6" gate="G$1" pin="3VDD"/>
<wire x1="147.32" y1="86.36" x2="147.32" y2="91.44" width="0.1524" layer="91"/>
<junction x="147.32" y="91.44"/>
<pinref part="U$7" gate="G$1" pin="3VDD"/>
<wire x1="177.8" y1="86.36" x2="177.8" y2="91.44" width="0.1524" layer="91"/>
<junction x="177.8" y="91.44"/>
<pinref part="U$8" gate="G$1" pin="3VDD"/>
<wire x1="208.28" y1="86.36" x2="208.28" y2="91.44" width="0.1524" layer="91"/>
<junction x="208.28" y="91.44"/>
<pinref part="U$9" gate="G$1" pin="3VDD"/>
<wire x1="238.76" y1="86.36" x2="238.76" y2="91.44" width="0.1524" layer="91"/>
<junction x="238.76" y="91.44"/>
<pinref part="U$10" gate="G$1" pin="3VDD"/>
<wire x1="269.24" y1="86.36" x2="269.24" y2="91.44" width="0.1524" layer="91"/>
<junction x="269.24" y="91.44"/>
<pinref part="U$13" gate="G$1" pin="3VDD"/>
<wire x1="2.54" y1="-78.74" x2="-10.16" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-78.74" x2="-10.16" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-48.26" x2="-10.16" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-17.78" x2="-10.16" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="12.7" x2="-10.16" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="43.18" x2="-10.16" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="91.44" x2="-5.08" y2="91.44" width="0.1524" layer="91"/>
<junction x="-5.08" y="91.44"/>
<pinref part="U$11" gate="G$1" pin="3VDD"/>
<wire x1="2.54" y1="43.18" x2="-10.16" y2="43.18" width="0.1524" layer="91"/>
<junction x="-10.16" y="43.18"/>
<pinref part="U$12" gate="G$1" pin="3VDD"/>
<wire x1="2.54" y1="12.7" x2="-10.16" y2="12.7" width="0.1524" layer="91"/>
<junction x="-10.16" y="12.7"/>
<pinref part="U$15" gate="G$1" pin="3VDD"/>
<wire x1="2.54" y1="-17.78" x2="-10.16" y2="-17.78" width="0.1524" layer="91"/>
<junction x="-10.16" y="-17.78"/>
<pinref part="U$14" gate="G$1" pin="3VDD"/>
<wire x1="2.54" y1="-48.26" x2="-10.16" y2="-48.26" width="0.1524" layer="91"/>
<junction x="-10.16" y="-48.26"/>
<pinref part="U$28" gate="G$1" pin="3VDD"/>
<wire x1="287.02" y1="68.58" x2="297.18" y2="68.58" width="0.1524" layer="91"/>
<wire x1="297.18" y1="68.58" x2="297.18" y2="38.1" width="0.1524" layer="91"/>
<wire x1="297.18" y1="38.1" x2="297.18" y2="7.62" width="0.1524" layer="91"/>
<wire x1="297.18" y1="7.62" x2="297.18" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="297.18" y1="-22.86" x2="297.18" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="297.18" y1="-53.34" x2="297.18" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="297.18" y1="-96.52" x2="297.18" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="297.18" y1="-104.14" x2="264.16" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="264.16" y1="-104.14" x2="233.68" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="233.68" y1="-104.14" x2="203.2" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="203.2" y1="-104.14" x2="172.72" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="172.72" y1="-104.14" x2="142.24" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="142.24" y1="-104.14" x2="111.76" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-104.14" x2="83.82" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-104.14" x2="50.8" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-104.14" x2="20.32" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-104.14" x2="-12.7" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-104.14" x2="-12.7" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-78.74" x2="-10.16" y2="-78.74" width="0.1524" layer="91"/>
<junction x="-10.16" y="-78.74"/>
<pinref part="U$25" gate="G$1" pin="3VDD"/>
<wire x1="20.32" y1="-96.52" x2="20.32" y2="-104.14" width="0.1524" layer="91"/>
<junction x="20.32" y="-104.14"/>
<pinref part="U$24" gate="G$1" pin="3VDD"/>
<wire x1="50.8" y1="-96.52" x2="50.8" y2="-104.14" width="0.1524" layer="91"/>
<junction x="50.8" y="-104.14"/>
<wire x1="83.82" y1="-96.52" x2="83.82" y2="-104.14" width="0.1524" layer="91"/>
<junction x="83.82" y="-104.14"/>
<pinref part="U$23" gate="G$1" pin="3VDD"/>
<wire x1="81.28" y1="-96.52" x2="83.82" y2="-96.52" width="0.1524" layer="91"/>
<pinref part="U$22" gate="G$1" pin="3VDD"/>
<wire x1="111.76" y1="-96.52" x2="111.76" y2="-104.14" width="0.1524" layer="91"/>
<junction x="111.76" y="-104.14"/>
<pinref part="U$21" gate="G$1" pin="3VDD"/>
<wire x1="142.24" y1="-96.52" x2="142.24" y2="-104.14" width="0.1524" layer="91"/>
<junction x="142.24" y="-104.14"/>
<pinref part="U$20" gate="G$1" pin="3VDD"/>
<wire x1="172.72" y1="-96.52" x2="172.72" y2="-104.14" width="0.1524" layer="91"/>
<junction x="172.72" y="-104.14"/>
<pinref part="U$19" gate="G$1" pin="3VDD"/>
<wire x1="203.2" y1="-96.52" x2="203.2" y2="-104.14" width="0.1524" layer="91"/>
<junction x="203.2" y="-104.14"/>
<pinref part="U$18" gate="G$1" pin="3VDD"/>
<wire x1="233.68" y1="-96.52" x2="233.68" y2="-104.14" width="0.1524" layer="91"/>
<junction x="233.68" y="-104.14"/>
<pinref part="U$17" gate="G$1" pin="3VDD"/>
<wire x1="264.16" y1="-96.52" x2="264.16" y2="-104.14" width="0.1524" layer="91"/>
<junction x="264.16" y="-104.14"/>
<pinref part="U$16" gate="G$1" pin="3VDD"/>
<wire x1="294.64" y1="-96.52" x2="297.18" y2="-96.52" width="0.1524" layer="91"/>
<junction x="297.18" y="-96.52"/>
<pinref part="U$26" gate="G$1" pin="3VDD"/>
<wire x1="287.02" y1="-53.34" x2="297.18" y2="-53.34" width="0.1524" layer="91"/>
<junction x="297.18" y="-53.34"/>
<pinref part="U$27" gate="G$1" pin="3VDD"/>
<wire x1="287.02" y1="-22.86" x2="297.18" y2="-22.86" width="0.1524" layer="91"/>
<junction x="297.18" y="-22.86"/>
<pinref part="U$30" gate="G$1" pin="3VDD"/>
<wire x1="287.02" y1="7.62" x2="297.18" y2="7.62" width="0.1524" layer="91"/>
<junction x="297.18" y="7.62"/>
<pinref part="U$29" gate="G$1" pin="3VDD"/>
<wire x1="287.02" y1="38.1" x2="297.18" y2="38.1" width="0.1524" layer="91"/>
<junction x="297.18" y="38.1"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U$31" gate="G$1" pin="4DOUT"/>
<wire x1="33.02" y1="40.64" x2="33.02" y2="35.56" width="0.1524" layer="91"/>
<wire x1="33.02" y1="35.56" x2="58.42" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U$44" gate="G$1" pin="2DIN"/>
<wire x1="58.42" y1="35.56" x2="58.42" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U$44" gate="G$1" pin="4DOUT"/>
<wire x1="33.02" y1="20.32" x2="33.02" y2="15.24" width="0.1524" layer="91"/>
<wire x1="33.02" y1="15.24" x2="58.42" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U$45" gate="G$1" pin="2DIN"/>
<wire x1="58.42" y1="15.24" x2="58.42" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U$45" gate="G$1" pin="4DOUT"/>
<wire x1="33.02" y1="0" x2="33.02" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-5.08" x2="58.42" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="U$58" gate="G$1" pin="2DIN"/>
<wire x1="58.42" y1="-5.08" x2="58.42" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U$58" gate="G$1" pin="4DOUT"/>
<wire x1="33.02" y1="-20.32" x2="33.02" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-25.4" x2="58.42" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="U$59" gate="G$1" pin="2DIN"/>
<wire x1="58.42" y1="-25.4" x2="58.42" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U$59" gate="G$1" pin="4DOUT"/>
<wire x1="33.02" y1="-40.64" x2="33.02" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-45.72" x2="58.42" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="U$72" gate="G$1" pin="2DIN"/>
<wire x1="58.42" y1="-45.72" x2="58.42" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U$32" gate="G$1" pin="4DOUT"/>
<wire x1="91.44" y1="-50.8" x2="91.44" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-45.72" x2="66.04" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="U$33" gate="G$1" pin="2DIN"/>
<wire x1="66.04" y1="-45.72" x2="66.04" y2="-40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U$33" gate="G$1" pin="4DOUT"/>
<wire x1="91.44" y1="-30.48" x2="91.44" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-25.4" x2="66.04" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="U$34" gate="G$1" pin="2DIN"/>
<wire x1="66.04" y1="-25.4" x2="66.04" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U$34" gate="G$1" pin="4DOUT"/>
<wire x1="91.44" y1="-10.16" x2="91.44" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-5.08" x2="66.04" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="U$35" gate="G$1" pin="2DIN"/>
<wire x1="66.04" y1="-5.08" x2="66.04" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U$35" gate="G$1" pin="4DOUT"/>
<wire x1="91.44" y1="10.16" x2="91.44" y2="15.24" width="0.1524" layer="91"/>
<wire x1="91.44" y1="15.24" x2="66.04" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U$36" gate="G$1" pin="2DIN"/>
<wire x1="66.04" y1="15.24" x2="66.04" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U$36" gate="G$1" pin="4DOUT"/>
<wire x1="91.44" y1="30.48" x2="91.44" y2="35.56" width="0.1524" layer="91"/>
<wire x1="91.44" y1="35.56" x2="66.04" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U$37" gate="G$1" pin="2DIN"/>
<wire x1="66.04" y1="35.56" x2="66.04" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U$38" gate="G$1" pin="4DOUT"/>
<wire x1="99.06" y1="40.64" x2="99.06" y2="35.56" width="0.1524" layer="91"/>
<wire x1="99.06" y1="35.56" x2="124.46" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U$39" gate="G$1" pin="2DIN"/>
<wire x1="124.46" y1="35.56" x2="124.46" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U$39" gate="G$1" pin="4DOUT"/>
<wire x1="99.06" y1="20.32" x2="99.06" y2="15.24" width="0.1524" layer="91"/>
<wire x1="99.06" y1="15.24" x2="124.46" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U$40" gate="G$1" pin="2DIN"/>
<wire x1="124.46" y1="15.24" x2="124.46" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U$40" gate="G$1" pin="4DOUT"/>
<wire x1="99.06" y1="0" x2="99.06" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-5.08" x2="124.46" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="U$41" gate="G$1" pin="2DIN"/>
<wire x1="124.46" y1="-5.08" x2="124.46" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U$41" gate="G$1" pin="4DOUT"/>
<wire x1="99.06" y1="-20.32" x2="99.06" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-25.4" x2="124.46" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="U$42" gate="G$1" pin="2DIN"/>
<wire x1="124.46" y1="-25.4" x2="124.46" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U$42" gate="G$1" pin="4DOUT"/>
<wire x1="99.06" y1="-40.64" x2="99.06" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-45.72" x2="124.46" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="U$43" gate="G$1" pin="2DIN"/>
<wire x1="124.46" y1="-45.72" x2="124.46" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U$46" gate="G$1" pin="4DOUT"/>
<wire x1="157.48" y1="-50.8" x2="157.48" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="157.48" y1="-45.72" x2="132.08" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="U$47" gate="G$1" pin="2DIN"/>
<wire x1="132.08" y1="-45.72" x2="132.08" y2="-40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U$47" gate="G$1" pin="4DOUT"/>
<wire x1="157.48" y1="-30.48" x2="157.48" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="157.48" y1="-25.4" x2="132.08" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="U$48" gate="G$1" pin="2DIN"/>
<wire x1="132.08" y1="-25.4" x2="132.08" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="U$48" gate="G$1" pin="4DOUT"/>
<wire x1="157.48" y1="-10.16" x2="157.48" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="157.48" y1="-5.08" x2="132.08" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="U$49" gate="G$1" pin="2DIN"/>
<wire x1="132.08" y1="-5.08" x2="132.08" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="U$49" gate="G$1" pin="4DOUT"/>
<wire x1="157.48" y1="10.16" x2="157.48" y2="15.24" width="0.1524" layer="91"/>
<wire x1="157.48" y1="15.24" x2="132.08" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U$50" gate="G$1" pin="2DIN"/>
<wire x1="132.08" y1="15.24" x2="132.08" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="U$50" gate="G$1" pin="4DOUT"/>
<wire x1="157.48" y1="30.48" x2="157.48" y2="35.56" width="0.1524" layer="91"/>
<wire x1="157.48" y1="35.56" x2="132.08" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U$51" gate="G$1" pin="2DIN"/>
<wire x1="132.08" y1="35.56" x2="132.08" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="U$52" gate="G$1" pin="4DOUT"/>
<wire x1="165.1" y1="40.64" x2="165.1" y2="35.56" width="0.1524" layer="91"/>
<wire x1="165.1" y1="35.56" x2="190.5" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U$53" gate="G$1" pin="2DIN"/>
<wire x1="190.5" y1="35.56" x2="190.5" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="U$53" gate="G$1" pin="4DOUT"/>
<wire x1="165.1" y1="20.32" x2="165.1" y2="15.24" width="0.1524" layer="91"/>
<wire x1="165.1" y1="15.24" x2="190.5" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U$54" gate="G$1" pin="2DIN"/>
<wire x1="190.5" y1="15.24" x2="190.5" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="U$54" gate="G$1" pin="4DOUT"/>
<wire x1="165.1" y1="0" x2="165.1" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-5.08" x2="190.5" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="U$55" gate="G$1" pin="2DIN"/>
<wire x1="190.5" y1="-5.08" x2="190.5" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="U$55" gate="G$1" pin="4DOUT"/>
<wire x1="165.1" y1="-20.32" x2="165.1" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-25.4" x2="190.5" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="U$56" gate="G$1" pin="2DIN"/>
<wire x1="190.5" y1="-25.4" x2="190.5" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="U$56" gate="G$1" pin="4DOUT"/>
<wire x1="165.1" y1="-40.64" x2="165.1" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-45.72" x2="190.5" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="U$57" gate="G$1" pin="2DIN"/>
<wire x1="190.5" y1="-45.72" x2="190.5" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="U$60" gate="G$1" pin="4DOUT"/>
<wire x1="223.52" y1="-50.8" x2="223.52" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="223.52" y1="-45.72" x2="198.12" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="U$61" gate="G$1" pin="2DIN"/>
<wire x1="198.12" y1="-45.72" x2="198.12" y2="-40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="U$61" gate="G$1" pin="4DOUT"/>
<wire x1="223.52" y1="-30.48" x2="223.52" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="223.52" y1="-25.4" x2="198.12" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="U$62" gate="G$1" pin="2DIN"/>
<wire x1="198.12" y1="-25.4" x2="198.12" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="U$62" gate="G$1" pin="4DOUT"/>
<wire x1="223.52" y1="-10.16" x2="223.52" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="223.52" y1="-5.08" x2="198.12" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="U$63" gate="G$1" pin="2DIN"/>
<wire x1="198.12" y1="-5.08" x2="198.12" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="U$63" gate="G$1" pin="4DOUT"/>
<wire x1="223.52" y1="10.16" x2="223.52" y2="15.24" width="0.1524" layer="91"/>
<wire x1="223.52" y1="15.24" x2="198.12" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U$64" gate="G$1" pin="2DIN"/>
<wire x1="198.12" y1="15.24" x2="198.12" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="U$64" gate="G$1" pin="4DOUT"/>
<wire x1="223.52" y1="30.48" x2="223.52" y2="35.56" width="0.1524" layer="91"/>
<wire x1="223.52" y1="35.56" x2="198.12" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U$65" gate="G$1" pin="2DIN"/>
<wire x1="198.12" y1="35.56" x2="198.12" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="U$66" gate="G$1" pin="4DOUT"/>
<wire x1="231.14" y1="40.64" x2="231.14" y2="35.56" width="0.1524" layer="91"/>
<wire x1="231.14" y1="35.56" x2="256.54" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U$67" gate="G$1" pin="2DIN"/>
<wire x1="256.54" y1="35.56" x2="256.54" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="U$67" gate="G$1" pin="4DOUT"/>
<wire x1="231.14" y1="20.32" x2="231.14" y2="15.24" width="0.1524" layer="91"/>
<wire x1="231.14" y1="15.24" x2="256.54" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U$68" gate="G$1" pin="2DIN"/>
<wire x1="256.54" y1="15.24" x2="256.54" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="U$68" gate="G$1" pin="4DOUT"/>
<wire x1="231.14" y1="0" x2="231.14" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="231.14" y1="-5.08" x2="256.54" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="U$69" gate="G$1" pin="2DIN"/>
<wire x1="256.54" y1="-5.08" x2="256.54" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="U$69" gate="G$1" pin="4DOUT"/>
<wire x1="231.14" y1="-20.32" x2="231.14" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="231.14" y1="-25.4" x2="256.54" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="U$70" gate="G$1" pin="2DIN"/>
<wire x1="256.54" y1="-25.4" x2="256.54" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="U$70" gate="G$1" pin="4DOUT"/>
<wire x1="231.14" y1="-40.64" x2="231.14" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="231.14" y1="-45.72" x2="256.54" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="U$71" gate="G$1" pin="2DIN"/>
<wire x1="256.54" y1="-45.72" x2="256.54" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="U$31" gate="G$1" pin="2DIN"/>
<wire x1="58.42" y1="50.8" x2="58.42" y2="60.96" width="0.1524" layer="91"/>
<wire x1="58.42" y1="60.96" x2="91.44" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U$37" gate="G$1" pin="4DOUT"/>
<wire x1="91.44" y1="60.96" x2="91.44" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="U$38" gate="G$1" pin="2DIN"/>
<wire x1="124.46" y1="50.8" x2="124.46" y2="60.96" width="0.1524" layer="91"/>
<wire x1="124.46" y1="60.96" x2="157.48" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U$51" gate="G$1" pin="4DOUT"/>
<wire x1="157.48" y1="60.96" x2="157.48" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="U$52" gate="G$1" pin="2DIN"/>
<wire x1="190.5" y1="50.8" x2="190.5" y2="60.96" width="0.1524" layer="91"/>
<wire x1="190.5" y1="60.96" x2="223.52" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U$65" gate="G$1" pin="4DOUT"/>
<wire x1="223.52" y1="60.96" x2="223.52" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="U$66" gate="G$1" pin="2DIN"/>
<wire x1="256.54" y1="50.8" x2="271.78" y2="50.8" width="0.1524" layer="91"/>
<wire x1="271.78" y1="50.8" x2="271.78" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U$28" gate="G$1" pin="4DOUT"/>
<wire x1="271.78" y1="68.58" x2="276.86" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="U$71" gate="G$1" pin="4DOUT"/>
<wire x1="231.14" y1="-60.96" x2="231.14" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="231.14" y1="-71.12" x2="198.12" y2="-71.12" width="0.1524" layer="91"/>
<pinref part="U$60" gate="G$1" pin="2DIN"/>
<wire x1="198.12" y1="-71.12" x2="198.12" y2="-60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="U$57" gate="G$1" pin="4DOUT"/>
<wire x1="165.1" y1="-60.96" x2="165.1" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-73.66" x2="132.08" y2="-73.66" width="0.1524" layer="91"/>
<pinref part="U$46" gate="G$1" pin="2DIN"/>
<wire x1="132.08" y1="-73.66" x2="132.08" y2="-60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="U$43" gate="G$1" pin="4DOUT"/>
<wire x1="99.06" y1="-60.96" x2="99.06" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-73.66" x2="66.04" y2="-73.66" width="0.1524" layer="91"/>
<pinref part="U$32" gate="G$1" pin="2DIN"/>
<wire x1="66.04" y1="-73.66" x2="66.04" y2="-60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="U$10" gate="G$1" pin="4DOUT"/>
<pinref part="U$9" gate="G$1" pin="2DIN"/>
<wire x1="269.24" y1="76.2" x2="264.16" y2="81.28" width="0.1524" layer="91"/>
<wire x1="264.16" y1="81.28" x2="264.16" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="U$9" gate="G$1" pin="4DOUT"/>
<pinref part="U$8" gate="G$1" pin="2DIN"/>
<wire x1="238.76" y1="76.2" x2="233.68" y2="81.28" width="0.1524" layer="91"/>
<wire x1="233.68" y1="81.28" x2="233.68" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="4DOUT"/>
<pinref part="U$7" gate="G$1" pin="2DIN"/>
<wire x1="208.28" y1="76.2" x2="203.2" y2="81.28" width="0.1524" layer="91"/>
<wire x1="203.2" y1="81.28" x2="203.2" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="4DOUT"/>
<pinref part="U$6" gate="G$1" pin="2DIN"/>
<wire x1="177.8" y1="76.2" x2="172.72" y2="81.28" width="0.1524" layer="91"/>
<wire x1="172.72" y1="81.28" x2="172.72" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="U$6" gate="G$1" pin="4DOUT"/>
<pinref part="U$5" gate="G$1" pin="2DIN"/>
<wire x1="147.32" y1="76.2" x2="142.24" y2="81.28" width="0.1524" layer="91"/>
<wire x1="142.24" y1="81.28" x2="142.24" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="4DOUT"/>
<pinref part="U$4" gate="G$1" pin="2DIN"/>
<wire x1="116.84" y1="76.2" x2="111.76" y2="81.28" width="0.1524" layer="91"/>
<wire x1="111.76" y1="81.28" x2="111.76" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="4DOUT"/>
<pinref part="U$3" gate="G$1" pin="2DIN"/>
<wire x1="86.36" y1="76.2" x2="81.28" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="4DOUT"/>
<pinref part="U$2" gate="G$1" pin="2DIN"/>
<wire x1="55.88" y1="76.2" x2="50.8" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="4DOUT"/>
<pinref part="U$1" gate="G$1" pin="2DIN"/>
<wire x1="25.4" y1="76.2" x2="20.32" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="4DOUT"/>
<pinref part="U$12" gate="G$1" pin="2DIN"/>
<wire x1="12.7" y1="43.18" x2="2.54" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="U$12" gate="G$1" pin="4DOUT"/>
<pinref part="U$15" gate="G$1" pin="2DIN"/>
<wire x1="12.7" y1="12.7" x2="2.54" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="U$15" gate="G$1" pin="4DOUT"/>
<pinref part="U$14" gate="G$1" pin="2DIN"/>
<wire x1="12.7" y1="-17.78" x2="2.54" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="U$14" gate="G$1" pin="4DOUT"/>
<pinref part="U$13" gate="G$1" pin="2DIN"/>
<wire x1="12.7" y1="-48.26" x2="2.54" y2="-53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="U$25" gate="G$1" pin="4DOUT"/>
<pinref part="U$24" gate="G$1" pin="2DIN"/>
<wire x1="20.32" y1="-86.36" x2="25.4" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="U$24" gate="G$1" pin="4DOUT"/>
<pinref part="U$23" gate="G$1" pin="2DIN"/>
<wire x1="50.8" y1="-86.36" x2="55.88" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="U$23" gate="G$1" pin="4DOUT"/>
<pinref part="U$22" gate="G$1" pin="2DIN"/>
<wire x1="81.28" y1="-86.36" x2="86.36" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="U$22" gate="G$1" pin="4DOUT"/>
<pinref part="U$21" gate="G$1" pin="2DIN"/>
<wire x1="111.76" y1="-86.36" x2="116.84" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="U$21" gate="G$1" pin="4DOUT"/>
<pinref part="U$20" gate="G$1" pin="2DIN"/>
<wire x1="142.24" y1="-86.36" x2="147.32" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="U$20" gate="G$1" pin="4DOUT"/>
<pinref part="U$19" gate="G$1" pin="2DIN"/>
<wire x1="172.72" y1="-86.36" x2="177.8" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="U$19" gate="G$1" pin="4DOUT"/>
<pinref part="U$18" gate="G$1" pin="2DIN"/>
<wire x1="203.2" y1="-86.36" x2="208.28" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="U$18" gate="G$1" pin="4DOUT"/>
<pinref part="U$17" gate="G$1" pin="2DIN"/>
<wire x1="233.68" y1="-86.36" x2="238.76" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="4DOUT"/>
<pinref part="U$16" gate="G$1" pin="2DIN"/>
<wire x1="264.16" y1="-86.36" x2="269.24" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="U$27" gate="G$1" pin="2DIN"/>
<wire x1="276.86" y1="-50.8" x2="287.02" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="U$26" gate="G$1" pin="4DOUT"/>
<wire x1="276.86" y1="-53.34" x2="276.86" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="U$30" gate="G$1" pin="2DIN"/>
<pinref part="U$27" gate="G$1" pin="4DOUT"/>
<wire x1="287.02" y1="-17.78" x2="276.86" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<pinref part="U$30" gate="G$1" pin="4DOUT"/>
<pinref part="U$29" gate="G$1" pin="2DIN"/>
<wire x1="276.86" y1="7.62" x2="287.02" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="U$28" gate="G$1" pin="2DIN"/>
<pinref part="U$29" gate="G$1" pin="4DOUT"/>
<wire x1="287.02" y1="43.18" x2="276.86" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="4DOUT"/>
<pinref part="U$11" gate="G$1" pin="2DIN"/>
<wire x1="-5.08" y1="76.2" x2="2.54" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="U$13" gate="G$1" pin="4DOUT"/>
<wire x1="12.7" y1="-78.74" x2="12.7" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-81.28" x2="-10.16" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-81.28" x2="-10.16" y2="-96.52" width="0.1524" layer="91"/>
<pinref part="U$25" gate="G$1" pin="2DIN"/>
<wire x1="-10.16" y1="-96.52" x2="-5.08" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="4DOUT"/>
<pinref part="U$26" gate="G$1" pin="2DIN"/>
<wire x1="294.64" y1="-86.36" x2="287.02" y2="-78.74" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
