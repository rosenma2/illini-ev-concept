<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.5.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="BMS">
<packages>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="FUSE">
<wire x1="-5.08" y1="0" x2="-3.556" y2="1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="-2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.889" y1="-1.4986" x2="2.4892" y2="0" width="0.254" layer="94"/>
<wire x1="-3.5992" y1="1.4912" x2="-3.048" y2="1.7272" width="0.254" layer="94" curve="-46.337037" cap="flat"/>
<wire x1="-3.048" y1="1.7272" x2="-2.496" y2="1.491" width="0.254" layer="94" curve="-46.403624" cap="flat"/>
<wire x1="0.4572" y1="-1.778" x2="0.8965" y2="-1.4765" width="0.254" layer="94" curve="63.169357" cap="flat"/>
<wire x1="-0.0178" y1="-1.508" x2="0.4572" y2="-1.7778" width="0.254" layer="94" curve="64.986119" cap="flat"/>
<text x="-5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="0" visible="off" length="short" direction="pas" function="dot" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" function="dot" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FUSE3216">
<gates>
<gate name="G$1" symbol="FUSE" x="-10.16" y="5.08"/>
</gates>
<devices>
<device name="" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead" urn="urn:adsk.eagle:library:325">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X01" urn="urn:adsk.eagle:footprint:22382/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="1X01" urn="urn:adsk.eagle:package:22485/2" type="model" library_version="3">
<description>PIN HEADER</description>
</package3d>
</packages3d>
<symbols>
<symbol name="PINHD1" urn="urn:adsk.eagle:symbol:22381/1" library_version="3">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X1" urn="urn:adsk.eagle:component:22540/2" prefix="JP" uservalue="yes" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22485/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$2" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$3" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$4" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$5" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$6" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$7" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$8" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$9" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$10" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$11" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$12" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$13" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$14" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$15" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$16" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$17" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$18" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$19" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$20" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$21" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$22" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$23" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$24" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$25" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$26" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$27" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$28" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$29" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$30" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$31" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$32" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$33" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$34" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$35" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$36" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$37" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$38" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$39" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$40" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$41" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$42" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$43" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$44" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$45" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$46" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$47" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$48" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$49" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$50" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$51" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$52" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$53" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$54" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$55" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$56" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$57" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$58" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$59" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$60" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$61" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$62" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$63" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$64" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$65" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$66" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$67" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$68" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$69" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$70" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$71" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$72" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$73" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$74" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$75" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$76" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$77" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$78" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$79" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$80" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$81" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$82" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$83" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$84" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$85" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$86" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$87" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$88" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$89" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$90" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$91" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$92" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$93" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$94" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$95" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$96" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$97" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="U$98" library="BMS" deviceset="FUSE3216" device="" value="5A"/>
<part name="JP1" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP2" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP3" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP4" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP5" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP6" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP7" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP8" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP9" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP11" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP12" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP13" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP14" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP15" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP16" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP17" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP18" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP19" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP20" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP21" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP22" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP23" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP24" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP25" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP27" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP28" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP29" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP30" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP31" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP32" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP33" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP34" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP35" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP36" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP37" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP38" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP39" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP40" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP42" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP43" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP44" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP45" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP46" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP47" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP48" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP49" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP50" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP51" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP52" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP53" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP54" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP55" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP57" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP58" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP59" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP60" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP61" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP62" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP63" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP64" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP65" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP66" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP67" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP68" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP69" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP70" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP72" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP73" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP74" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP75" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP76" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP77" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP78" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP79" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP80" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP81" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP82" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP83" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP84" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP85" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP87" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP88" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP89" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP90" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP91" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP92" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP93" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP94" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP95" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP96" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP97" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP98" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP99" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP100" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP102" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP103" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP104" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP105" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP106" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP107" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP108" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP109" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP110" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP111" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP112" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP113" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP114" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP116" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP117" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP118" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP119" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="JP120" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="-7.62" y="63.5" rot="R90"/>
<instance part="U$2" gate="G$1" x="-5.08" y="63.5" rot="R90"/>
<instance part="U$3" gate="G$1" x="-2.54" y="63.5" rot="R90"/>
<instance part="U$4" gate="G$1" x="0" y="63.5" rot="R90"/>
<instance part="U$5" gate="G$1" x="2.54" y="63.5" rot="R90"/>
<instance part="U$6" gate="G$1" x="5.08" y="63.5" rot="R90"/>
<instance part="U$7" gate="G$1" x="7.62" y="63.5" rot="R90"/>
<instance part="U$8" gate="G$1" x="10.16" y="63.5" rot="R90"/>
<instance part="U$9" gate="G$1" x="12.7" y="63.5" rot="R90"/>
<instance part="U$10" gate="G$1" x="15.24" y="63.5" rot="R90"/>
<instance part="U$11" gate="G$1" x="17.78" y="63.5" rot="R90"/>
<instance part="U$12" gate="G$1" x="20.32" y="63.5" rot="R90"/>
<instance part="U$13" gate="G$1" x="22.86" y="63.5" rot="R90"/>
<instance part="U$14" gate="G$1" x="25.4" y="63.5" rot="R90"/>
<instance part="U$15" gate="G$1" x="33.02" y="63.5" rot="R90"/>
<instance part="U$16" gate="G$1" x="35.56" y="63.5" rot="R90"/>
<instance part="U$17" gate="G$1" x="38.1" y="63.5" rot="R90"/>
<instance part="U$18" gate="G$1" x="40.64" y="63.5" rot="R90"/>
<instance part="U$19" gate="G$1" x="43.18" y="63.5" rot="R90"/>
<instance part="U$20" gate="G$1" x="45.72" y="63.5" rot="R90"/>
<instance part="U$21" gate="G$1" x="48.26" y="63.5" rot="R90"/>
<instance part="U$22" gate="G$1" x="50.8" y="63.5" rot="R90"/>
<instance part="U$23" gate="G$1" x="53.34" y="63.5" rot="R90"/>
<instance part="U$24" gate="G$1" x="55.88" y="63.5" rot="R90"/>
<instance part="U$25" gate="G$1" x="58.42" y="63.5" rot="R90"/>
<instance part="U$26" gate="G$1" x="60.96" y="63.5" rot="R90"/>
<instance part="U$27" gate="G$1" x="63.5" y="63.5" rot="R90"/>
<instance part="U$28" gate="G$1" x="66.04" y="63.5" rot="R90"/>
<instance part="U$29" gate="G$1" x="76.2" y="63.5" rot="R90"/>
<instance part="U$30" gate="G$1" x="78.74" y="63.5" rot="R90"/>
<instance part="U$31" gate="G$1" x="81.28" y="63.5" rot="R90"/>
<instance part="U$32" gate="G$1" x="83.82" y="63.5" rot="R90"/>
<instance part="U$33" gate="G$1" x="86.36" y="63.5" rot="R90"/>
<instance part="U$34" gate="G$1" x="88.9" y="63.5" rot="R90"/>
<instance part="U$35" gate="G$1" x="91.44" y="63.5" rot="R90"/>
<instance part="U$36" gate="G$1" x="93.98" y="63.5" rot="R90"/>
<instance part="U$37" gate="G$1" x="96.52" y="63.5" rot="R90"/>
<instance part="U$38" gate="G$1" x="99.06" y="63.5" rot="R90"/>
<instance part="U$39" gate="G$1" x="101.6" y="63.5" rot="R90"/>
<instance part="U$40" gate="G$1" x="104.14" y="63.5" rot="R90"/>
<instance part="U$41" gate="G$1" x="106.68" y="63.5" rot="R90"/>
<instance part="U$42" gate="G$1" x="109.22" y="63.5" rot="R90"/>
<instance part="U$43" gate="G$1" x="119.38" y="63.5" rot="R90"/>
<instance part="U$44" gate="G$1" x="121.92" y="63.5" rot="R90"/>
<instance part="U$45" gate="G$1" x="124.46" y="63.5" rot="R90"/>
<instance part="U$46" gate="G$1" x="127" y="63.5" rot="R90"/>
<instance part="U$47" gate="G$1" x="129.54" y="63.5" rot="R90"/>
<instance part="U$48" gate="G$1" x="132.08" y="63.5" rot="R90"/>
<instance part="U$49" gate="G$1" x="134.62" y="63.5" rot="R90"/>
<instance part="U$50" gate="G$1" x="137.16" y="63.5" rot="R90"/>
<instance part="U$51" gate="G$1" x="139.7" y="63.5" rot="R90"/>
<instance part="U$52" gate="G$1" x="142.24" y="63.5" rot="R90"/>
<instance part="U$53" gate="G$1" x="144.78" y="63.5" rot="R90"/>
<instance part="U$54" gate="G$1" x="147.32" y="63.5" rot="R90"/>
<instance part="U$55" gate="G$1" x="149.86" y="63.5" rot="R90"/>
<instance part="U$56" gate="G$1" x="152.4" y="63.5" rot="R90"/>
<instance part="U$57" gate="G$1" x="162.56" y="63.5" rot="R90"/>
<instance part="U$58" gate="G$1" x="165.1" y="63.5" rot="R90"/>
<instance part="U$59" gate="G$1" x="167.64" y="63.5" rot="R90"/>
<instance part="U$60" gate="G$1" x="170.18" y="63.5" rot="R90"/>
<instance part="U$61" gate="G$1" x="172.72" y="63.5" rot="R90"/>
<instance part="U$62" gate="G$1" x="175.26" y="63.5" rot="R90"/>
<instance part="U$63" gate="G$1" x="177.8" y="63.5" rot="R90"/>
<instance part="U$64" gate="G$1" x="180.34" y="63.5" rot="R90"/>
<instance part="U$65" gate="G$1" x="182.88" y="63.5" rot="R90"/>
<instance part="U$66" gate="G$1" x="185.42" y="63.5" rot="R90"/>
<instance part="U$67" gate="G$1" x="187.96" y="63.5" rot="R90"/>
<instance part="U$68" gate="G$1" x="190.5" y="63.5" rot="R90"/>
<instance part="U$69" gate="G$1" x="193.04" y="63.5" rot="R90"/>
<instance part="U$70" gate="G$1" x="195.58" y="63.5" rot="R90"/>
<instance part="U$71" gate="G$1" x="205.74" y="63.5" rot="R90"/>
<instance part="U$72" gate="G$1" x="208.28" y="63.5" rot="R90"/>
<instance part="U$73" gate="G$1" x="210.82" y="63.5" rot="R90"/>
<instance part="U$74" gate="G$1" x="213.36" y="63.5" rot="R90"/>
<instance part="U$75" gate="G$1" x="215.9" y="63.5" rot="R90"/>
<instance part="U$76" gate="G$1" x="218.44" y="63.5" rot="R90"/>
<instance part="U$77" gate="G$1" x="220.98" y="63.5" rot="R90"/>
<instance part="U$78" gate="G$1" x="223.52" y="63.5" rot="R90"/>
<instance part="U$79" gate="G$1" x="226.06" y="63.5" rot="R90"/>
<instance part="U$80" gate="G$1" x="228.6" y="63.5" rot="R90"/>
<instance part="U$81" gate="G$1" x="231.14" y="63.5" rot="R90"/>
<instance part="U$82" gate="G$1" x="233.68" y="63.5" rot="R90"/>
<instance part="U$83" gate="G$1" x="236.22" y="63.5" rot="R90"/>
<instance part="U$84" gate="G$1" x="238.76" y="63.5" rot="R90"/>
<instance part="U$85" gate="G$1" x="248.92" y="63.5" rot="R90"/>
<instance part="U$86" gate="G$1" x="251.46" y="63.5" rot="R90"/>
<instance part="U$87" gate="G$1" x="254" y="63.5" rot="R90"/>
<instance part="U$88" gate="G$1" x="256.54" y="63.5" rot="R90"/>
<instance part="U$89" gate="G$1" x="259.08" y="63.5" rot="R90"/>
<instance part="U$90" gate="G$1" x="261.62" y="63.5" rot="R90"/>
<instance part="U$91" gate="G$1" x="264.16" y="63.5" rot="R90"/>
<instance part="U$92" gate="G$1" x="266.7" y="63.5" rot="R90"/>
<instance part="U$93" gate="G$1" x="269.24" y="63.5" rot="R90"/>
<instance part="U$94" gate="G$1" x="271.78" y="63.5" rot="R90"/>
<instance part="U$95" gate="G$1" x="274.32" y="63.5" rot="R90"/>
<instance part="U$96" gate="G$1" x="276.86" y="63.5" rot="R90"/>
<instance part="U$97" gate="G$1" x="279.4" y="63.5" rot="R90"/>
<instance part="U$98" gate="G$1" x="281.94" y="63.5" rot="R90"/>
<instance part="JP1" gate="G$1" x="-7.62" y="78.74" rot="R90"/>
<instance part="JP2" gate="G$1" x="-5.08" y="78.74" rot="R90"/>
<instance part="JP3" gate="G$1" x="-2.54" y="78.74" rot="R90"/>
<instance part="JP4" gate="G$1" x="0" y="78.74" rot="R90"/>
<instance part="JP5" gate="G$1" x="2.54" y="78.74" rot="R90"/>
<instance part="JP6" gate="G$1" x="5.08" y="78.74" rot="R90"/>
<instance part="JP7" gate="G$1" x="7.62" y="78.74" rot="R90"/>
<instance part="JP8" gate="G$1" x="10.16" y="78.74" rot="R90"/>
<instance part="JP9" gate="G$1" x="12.7" y="78.74" rot="R90"/>
<instance part="JP11" gate="G$1" x="15.24" y="78.74" rot="R90"/>
<instance part="JP12" gate="G$1" x="17.78" y="78.74" rot="R90"/>
<instance part="JP13" gate="G$1" x="20.32" y="78.74" rot="R90"/>
<instance part="JP14" gate="G$1" x="22.86" y="78.74" rot="R90"/>
<instance part="JP15" gate="G$1" x="25.4" y="78.74" rot="R90"/>
<instance part="JP16" gate="G$1" x="33.02" y="76.2" rot="R90"/>
<instance part="JP17" gate="G$1" x="35.56" y="76.2" rot="R90"/>
<instance part="JP18" gate="G$1" x="38.1" y="76.2" rot="R90"/>
<instance part="JP19" gate="G$1" x="40.64" y="76.2" rot="R90"/>
<instance part="JP20" gate="G$1" x="43.18" y="76.2" rot="R90"/>
<instance part="JP21" gate="G$1" x="45.72" y="76.2" rot="R90"/>
<instance part="JP22" gate="G$1" x="48.26" y="76.2" rot="R90"/>
<instance part="JP23" gate="G$1" x="50.8" y="76.2" rot="R90"/>
<instance part="JP24" gate="G$1" x="53.34" y="76.2" rot="R90"/>
<instance part="JP25" gate="G$1" x="55.88" y="76.2" rot="R90"/>
<instance part="JP27" gate="G$1" x="58.42" y="76.2" rot="R90"/>
<instance part="JP28" gate="G$1" x="60.96" y="76.2" rot="R90"/>
<instance part="JP29" gate="G$1" x="63.5" y="76.2" rot="R90"/>
<instance part="JP30" gate="G$1" x="66.04" y="76.2" rot="R90"/>
<instance part="JP31" gate="G$1" x="76.2" y="76.2" rot="R90"/>
<instance part="JP32" gate="G$1" x="78.74" y="76.2" rot="R90"/>
<instance part="JP33" gate="G$1" x="81.28" y="76.2" rot="R90"/>
<instance part="JP34" gate="G$1" x="83.82" y="76.2" rot="R90"/>
<instance part="JP35" gate="G$1" x="86.36" y="76.2" rot="R90"/>
<instance part="JP36" gate="G$1" x="88.9" y="76.2" rot="R90"/>
<instance part="JP37" gate="G$1" x="91.44" y="76.2" rot="R90"/>
<instance part="JP38" gate="G$1" x="93.98" y="76.2" rot="R90"/>
<instance part="JP39" gate="G$1" x="96.52" y="76.2" rot="R90"/>
<instance part="JP40" gate="G$1" x="99.06" y="76.2" rot="R90"/>
<instance part="JP42" gate="G$1" x="101.6" y="76.2" rot="R90"/>
<instance part="JP43" gate="G$1" x="104.14" y="76.2" rot="R90"/>
<instance part="JP44" gate="G$1" x="106.68" y="76.2" rot="R90"/>
<instance part="JP45" gate="G$1" x="109.22" y="76.2" rot="R90"/>
<instance part="JP46" gate="G$1" x="119.38" y="76.2" rot="R90"/>
<instance part="JP47" gate="G$1" x="121.92" y="76.2" rot="R90"/>
<instance part="JP48" gate="G$1" x="124.46" y="76.2" rot="R90"/>
<instance part="JP49" gate="G$1" x="127" y="76.2" rot="R90"/>
<instance part="JP50" gate="G$1" x="129.54" y="76.2" rot="R90"/>
<instance part="JP51" gate="G$1" x="132.08" y="76.2" rot="R90"/>
<instance part="JP52" gate="G$1" x="134.62" y="76.2" rot="R90"/>
<instance part="JP53" gate="G$1" x="137.16" y="76.2" rot="R90"/>
<instance part="JP54" gate="G$1" x="139.7" y="76.2" rot="R90"/>
<instance part="JP55" gate="G$1" x="142.24" y="76.2" rot="R90"/>
<instance part="JP57" gate="G$1" x="144.78" y="76.2" rot="R90"/>
<instance part="JP58" gate="G$1" x="147.32" y="76.2" rot="R90"/>
<instance part="JP59" gate="G$1" x="149.86" y="76.2" rot="R90"/>
<instance part="JP60" gate="G$1" x="152.4" y="76.2" rot="R90"/>
<instance part="JP61" gate="G$1" x="162.56" y="76.2" rot="R90"/>
<instance part="JP62" gate="G$1" x="165.1" y="76.2" rot="R90"/>
<instance part="JP63" gate="G$1" x="167.64" y="76.2" rot="R90"/>
<instance part="JP64" gate="G$1" x="170.18" y="76.2" rot="R90"/>
<instance part="JP65" gate="G$1" x="172.72" y="76.2" rot="R90"/>
<instance part="JP66" gate="G$1" x="175.26" y="76.2" rot="R90"/>
<instance part="JP67" gate="G$1" x="177.8" y="76.2" rot="R90"/>
<instance part="JP68" gate="G$1" x="180.34" y="76.2" rot="R90"/>
<instance part="JP69" gate="G$1" x="182.88" y="76.2" rot="R90"/>
<instance part="JP70" gate="G$1" x="185.42" y="76.2" rot="R90"/>
<instance part="JP72" gate="G$1" x="187.96" y="76.2" rot="R90"/>
<instance part="JP73" gate="G$1" x="190.5" y="76.2" rot="R90"/>
<instance part="JP74" gate="G$1" x="193.04" y="76.2" rot="R90"/>
<instance part="JP75" gate="G$1" x="195.58" y="76.2" rot="R90"/>
<instance part="JP76" gate="G$1" x="205.74" y="76.2" rot="R90"/>
<instance part="JP77" gate="G$1" x="208.28" y="76.2" rot="R90"/>
<instance part="JP78" gate="G$1" x="210.82" y="76.2" rot="R90"/>
<instance part="JP79" gate="G$1" x="213.36" y="76.2" rot="R90"/>
<instance part="JP80" gate="G$1" x="215.9" y="76.2" rot="R90"/>
<instance part="JP81" gate="G$1" x="218.44" y="76.2" rot="R90"/>
<instance part="JP82" gate="G$1" x="220.98" y="76.2" rot="R90"/>
<instance part="JP83" gate="G$1" x="223.52" y="76.2" rot="R90"/>
<instance part="JP84" gate="G$1" x="226.06" y="76.2" rot="R90"/>
<instance part="JP85" gate="G$1" x="228.6" y="76.2" rot="R90"/>
<instance part="JP87" gate="G$1" x="231.14" y="76.2" rot="R90"/>
<instance part="JP88" gate="G$1" x="233.68" y="76.2" rot="R90"/>
<instance part="JP89" gate="G$1" x="236.22" y="76.2" rot="R90"/>
<instance part="JP90" gate="G$1" x="238.76" y="76.2" rot="R90"/>
<instance part="JP91" gate="G$1" x="248.92" y="76.2" rot="R90"/>
<instance part="JP92" gate="G$1" x="251.46" y="76.2" rot="R90"/>
<instance part="JP93" gate="G$1" x="254" y="76.2" rot="R90"/>
<instance part="JP94" gate="G$1" x="256.54" y="76.2" rot="R90"/>
<instance part="JP95" gate="G$1" x="259.08" y="76.2" rot="R90"/>
<instance part="JP96" gate="G$1" x="261.62" y="76.2" rot="R90"/>
<instance part="JP97" gate="G$1" x="264.16" y="76.2" rot="R90"/>
<instance part="JP98" gate="G$1" x="266.7" y="76.2" rot="R90"/>
<instance part="JP99" gate="G$1" x="269.24" y="76.2" rot="R90"/>
<instance part="JP100" gate="G$1" x="271.78" y="76.2" rot="R90"/>
<instance part="JP102" gate="G$1" x="274.32" y="76.2" rot="R90"/>
<instance part="JP103" gate="G$1" x="276.86" y="76.2" rot="R90"/>
<instance part="JP104" gate="G$1" x="279.4" y="76.2" rot="R90"/>
<instance part="JP105" gate="G$1" x="281.94" y="76.2" rot="R90"/>
<instance part="JP106" gate="G$1" x="281.94" y="15.24" rot="R270"/>
<instance part="JP107" gate="G$1" x="279.4" y="15.24" rot="R270"/>
<instance part="JP108" gate="G$1" x="276.86" y="15.24" rot="R270"/>
<instance part="JP109" gate="G$1" x="274.32" y="15.24" rot="R270"/>
<instance part="JP110" gate="G$1" x="271.78" y="15.24" rot="R270"/>
<instance part="JP111" gate="G$1" x="269.24" y="15.24" rot="R270"/>
<instance part="JP112" gate="G$1" x="266.7" y="15.24" rot="R270"/>
<instance part="JP113" gate="G$1" x="264.16" y="15.24" rot="R270"/>
<instance part="JP114" gate="G$1" x="261.62" y="15.24" rot="R270"/>
<instance part="JP116" gate="G$1" x="259.08" y="15.24" rot="R270"/>
<instance part="JP117" gate="G$1" x="256.54" y="15.24" rot="R270"/>
<instance part="JP118" gate="G$1" x="254" y="15.24" rot="R270"/>
<instance part="JP119" gate="G$1" x="251.46" y="15.24" rot="R270"/>
<instance part="JP120" gate="G$1" x="248.92" y="15.24" rot="R270"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$99" class="0">
<segment>
<pinref part="U$43" gate="G$1" pin="1"/>
<wire x1="119.38" y1="55.88" x2="76.2" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$29" gate="G$1" pin="1"/>
<junction x="76.2" y="55.88"/>
<wire x1="76.2" y1="55.88" x2="33.02" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$15" gate="G$1" pin="1"/>
<junction x="33.02" y="55.88"/>
<wire x1="33.02" y1="55.88" x2="-7.62" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="1"/>
<junction x="-7.62" y="55.88"/>
<wire x1="-7.62" y1="55.88" x2="162.56" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$57" gate="G$1" pin="1"/>
<junction x="162.56" y="55.88"/>
<wire x1="162.56" y1="55.88" x2="205.74" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$71" gate="G$1" pin="1"/>
<junction x="205.74" y="55.88"/>
<wire x1="205.74" y1="55.88" x2="248.92" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$85" gate="G$1" pin="1"/>
<junction x="248.92" y="55.88"/>
<wire x1="248.92" y1="55.88" x2="248.92" y2="17.78" width="0.1524" layer="91"/>
<pinref part="JP120" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="U$44" gate="G$1" pin="1"/>
<wire x1="121.92" y1="55.88" x2="78.74" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$30" gate="G$1" pin="1"/>
<junction x="78.74" y="55.88"/>
<wire x1="78.74" y1="55.88" x2="35.56" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$16" gate="G$1" pin="1"/>
<junction x="35.56" y="55.88"/>
<wire x1="35.56" y1="55.88" x2="-5.08" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="1"/>
<junction x="-5.08" y="55.88"/>
<wire x1="-5.08" y1="55.88" x2="165.1" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$58" gate="G$1" pin="1"/>
<junction x="165.1" y="55.88"/>
<wire x1="165.1" y1="55.88" x2="208.28" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$72" gate="G$1" pin="1"/>
<junction x="208.28" y="55.88"/>
<wire x1="208.28" y1="55.88" x2="251.46" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$86" gate="G$1" pin="1"/>
<junction x="251.46" y="55.88"/>
<wire x1="251.46" y1="55.88" x2="251.46" y2="17.78" width="0.1524" layer="91"/>
<pinref part="JP119" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<pinref part="U$45" gate="G$1" pin="1"/>
<wire x1="124.46" y1="55.88" x2="81.28" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$31" gate="G$1" pin="1"/>
<junction x="81.28" y="55.88"/>
<wire x1="81.28" y1="55.88" x2="38.1" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$17" gate="G$1" pin="1"/>
<junction x="38.1" y="55.88"/>
<wire x1="38.1" y1="55.88" x2="-2.54" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="1"/>
<junction x="-2.54" y="55.88"/>
<wire x1="-2.54" y1="55.88" x2="167.64" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$59" gate="G$1" pin="1"/>
<junction x="167.64" y="55.88"/>
<wire x1="167.64" y1="55.88" x2="210.82" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$73" gate="G$1" pin="1"/>
<junction x="210.82" y="55.88"/>
<wire x1="210.82" y1="55.88" x2="254" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$87" gate="G$1" pin="1"/>
<junction x="254" y="55.88"/>
<wire x1="254" y1="55.88" x2="254" y2="17.78" width="0.1524" layer="91"/>
<pinref part="JP118" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<pinref part="U$46" gate="G$1" pin="1"/>
<wire x1="127" y1="55.88" x2="83.82" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$32" gate="G$1" pin="1"/>
<junction x="83.82" y="55.88"/>
<wire x1="83.82" y1="55.88" x2="40.64" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$18" gate="G$1" pin="1"/>
<junction x="40.64" y="55.88"/>
<wire x1="40.64" y1="55.88" x2="0" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="1"/>
<junction x="0" y="55.88"/>
<wire x1="0" y1="55.88" x2="170.18" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$60" gate="G$1" pin="1"/>
<junction x="170.18" y="55.88"/>
<wire x1="170.18" y1="55.88" x2="213.36" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$74" gate="G$1" pin="1"/>
<junction x="213.36" y="55.88"/>
<wire x1="213.36" y1="55.88" x2="256.54" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$88" gate="G$1" pin="1"/>
<junction x="256.54" y="55.88"/>
<wire x1="256.54" y1="55.88" x2="256.54" y2="17.78" width="0.1524" layer="91"/>
<pinref part="JP117" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$103" class="0">
<segment>
<pinref part="U$47" gate="G$1" pin="1"/>
<wire x1="129.54" y1="55.88" x2="86.36" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$33" gate="G$1" pin="1"/>
<junction x="86.36" y="55.88"/>
<wire x1="86.36" y1="55.88" x2="43.18" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$19" gate="G$1" pin="1"/>
<junction x="43.18" y="55.88"/>
<wire x1="43.18" y1="55.88" x2="2.54" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="1"/>
<junction x="2.54" y="55.88"/>
<wire x1="2.54" y1="55.88" x2="172.72" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$61" gate="G$1" pin="1"/>
<junction x="172.72" y="55.88"/>
<wire x1="172.72" y1="55.88" x2="215.9" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$75" gate="G$1" pin="1"/>
<junction x="215.9" y="55.88"/>
<wire x1="215.9" y1="55.88" x2="259.08" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$89" gate="G$1" pin="1"/>
<junction x="259.08" y="55.88"/>
<wire x1="259.08" y1="55.88" x2="259.08" y2="17.78" width="0.1524" layer="91"/>
<pinref part="JP116" gate="G$1" pin="1"/>
<junction x="259.08" y="17.78"/>
</segment>
</net>
<net name="N$104" class="0">
<segment>
<pinref part="U$48" gate="G$1" pin="1"/>
<wire x1="132.08" y1="55.88" x2="88.9" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$34" gate="G$1" pin="1"/>
<junction x="88.9" y="55.88"/>
<wire x1="88.9" y1="55.88" x2="45.72" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$20" gate="G$1" pin="1"/>
<junction x="45.72" y="55.88"/>
<wire x1="45.72" y1="55.88" x2="5.08" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="1"/>
<junction x="5.08" y="55.88"/>
<wire x1="5.08" y1="55.88" x2="175.26" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$62" gate="G$1" pin="1"/>
<junction x="175.26" y="55.88"/>
<wire x1="175.26" y1="55.88" x2="218.44" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$76" gate="G$1" pin="1"/>
<junction x="218.44" y="55.88"/>
<wire x1="218.44" y1="55.88" x2="261.62" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$90" gate="G$1" pin="1"/>
<junction x="261.62" y="55.88"/>
<wire x1="261.62" y1="55.88" x2="261.62" y2="17.78" width="0.1524" layer="91"/>
<pinref part="JP114" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$105" class="0">
<segment>
<pinref part="U$49" gate="G$1" pin="1"/>
<wire x1="134.62" y1="55.88" x2="91.44" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$35" gate="G$1" pin="1"/>
<junction x="91.44" y="55.88"/>
<wire x1="91.44" y1="55.88" x2="48.26" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$21" gate="G$1" pin="1"/>
<junction x="48.26" y="55.88"/>
<wire x1="48.26" y1="55.88" x2="7.62" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$7" gate="G$1" pin="1"/>
<junction x="7.62" y="55.88"/>
<wire x1="7.62" y1="55.88" x2="177.8" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$63" gate="G$1" pin="1"/>
<junction x="177.8" y="55.88"/>
<wire x1="177.8" y1="55.88" x2="220.98" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$77" gate="G$1" pin="1"/>
<junction x="220.98" y="55.88"/>
<wire x1="220.98" y1="55.88" x2="264.16" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$91" gate="G$1" pin="1"/>
<junction x="264.16" y="55.88"/>
<wire x1="264.16" y1="55.88" x2="264.16" y2="17.78" width="0.1524" layer="91"/>
<pinref part="JP113" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<pinref part="U$50" gate="G$1" pin="1"/>
<wire x1="137.16" y1="55.88" x2="93.98" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$36" gate="G$1" pin="1"/>
<junction x="93.98" y="55.88"/>
<wire x1="93.98" y1="55.88" x2="50.8" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$22" gate="G$1" pin="1"/>
<junction x="50.8" y="55.88"/>
<wire x1="50.8" y1="55.88" x2="10.16" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="1"/>
<junction x="10.16" y="55.88"/>
<wire x1="10.16" y1="55.88" x2="180.34" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$64" gate="G$1" pin="1"/>
<junction x="180.34" y="55.88"/>
<wire x1="180.34" y1="55.88" x2="223.52" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$78" gate="G$1" pin="1"/>
<junction x="223.52" y="55.88"/>
<wire x1="223.52" y1="55.88" x2="266.7" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$92" gate="G$1" pin="1"/>
<junction x="266.7" y="55.88"/>
<wire x1="266.7" y1="55.88" x2="266.7" y2="17.78" width="0.1524" layer="91"/>
<pinref part="JP112" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<pinref part="U$51" gate="G$1" pin="1"/>
<wire x1="139.7" y1="55.88" x2="96.52" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$37" gate="G$1" pin="1"/>
<junction x="96.52" y="55.88"/>
<wire x1="96.52" y1="55.88" x2="53.34" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$23" gate="G$1" pin="1"/>
<junction x="53.34" y="55.88"/>
<wire x1="53.34" y1="55.88" x2="12.7" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$9" gate="G$1" pin="1"/>
<junction x="12.7" y="55.88"/>
<wire x1="12.7" y1="55.88" x2="182.88" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$65" gate="G$1" pin="1"/>
<junction x="182.88" y="55.88"/>
<wire x1="182.88" y1="55.88" x2="226.06" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$79" gate="G$1" pin="1"/>
<junction x="226.06" y="55.88"/>
<wire x1="226.06" y1="55.88" x2="269.24" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$93" gate="G$1" pin="1"/>
<junction x="269.24" y="55.88"/>
<wire x1="269.24" y1="55.88" x2="269.24" y2="17.78" width="0.1524" layer="91"/>
<pinref part="JP111" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$108" class="0">
<segment>
<pinref part="U$52" gate="G$1" pin="1"/>
<wire x1="142.24" y1="55.88" x2="99.06" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$38" gate="G$1" pin="1"/>
<junction x="99.06" y="55.88"/>
<wire x1="99.06" y1="55.88" x2="55.88" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$24" gate="G$1" pin="1"/>
<junction x="55.88" y="55.88"/>
<wire x1="55.88" y1="55.88" x2="15.24" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$10" gate="G$1" pin="1"/>
<junction x="15.24" y="55.88"/>
<wire x1="15.24" y1="55.88" x2="185.42" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$66" gate="G$1" pin="1"/>
<junction x="185.42" y="55.88"/>
<wire x1="185.42" y1="55.88" x2="228.6" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$80" gate="G$1" pin="1"/>
<junction x="228.6" y="55.88"/>
<wire x1="228.6" y1="55.88" x2="271.78" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$94" gate="G$1" pin="1"/>
<junction x="271.78" y="55.88"/>
<wire x1="271.78" y1="55.88" x2="271.78" y2="17.78" width="0.1524" layer="91"/>
<pinref part="JP110" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$109" class="0">
<segment>
<pinref part="U$53" gate="G$1" pin="1"/>
<wire x1="144.78" y1="55.88" x2="101.6" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$39" gate="G$1" pin="1"/>
<junction x="101.6" y="55.88"/>
<wire x1="101.6" y1="55.88" x2="58.42" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$25" gate="G$1" pin="1"/>
<junction x="58.42" y="55.88"/>
<wire x1="58.42" y1="55.88" x2="17.78" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$11" gate="G$1" pin="1"/>
<junction x="17.78" y="55.88"/>
<wire x1="17.78" y1="55.88" x2="187.96" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$67" gate="G$1" pin="1"/>
<junction x="187.96" y="55.88"/>
<wire x1="187.96" y1="55.88" x2="231.14" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$81" gate="G$1" pin="1"/>
<junction x="231.14" y="55.88"/>
<wire x1="231.14" y1="55.88" x2="274.32" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$95" gate="G$1" pin="1"/>
<junction x="274.32" y="55.88"/>
<wire x1="274.32" y1="55.88" x2="274.32" y2="17.78" width="0.1524" layer="91"/>
<pinref part="JP109" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$110" class="0">
<segment>
<pinref part="U$54" gate="G$1" pin="1"/>
<wire x1="147.32" y1="55.88" x2="104.14" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$40" gate="G$1" pin="1"/>
<junction x="104.14" y="55.88"/>
<wire x1="104.14" y1="55.88" x2="60.96" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$26" gate="G$1" pin="1"/>
<junction x="60.96" y="55.88"/>
<wire x1="60.96" y1="55.88" x2="20.32" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="1"/>
<junction x="20.32" y="55.88"/>
<wire x1="20.32" y1="55.88" x2="190.5" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$68" gate="G$1" pin="1"/>
<junction x="190.5" y="55.88"/>
<wire x1="190.5" y1="55.88" x2="233.68" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$82" gate="G$1" pin="1"/>
<junction x="233.68" y="55.88"/>
<wire x1="233.68" y1="55.88" x2="276.86" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$96" gate="G$1" pin="1"/>
<junction x="276.86" y="55.88"/>
<wire x1="276.86" y1="55.88" x2="276.86" y2="17.78" width="0.1524" layer="91"/>
<pinref part="JP108" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$111" class="0">
<segment>
<pinref part="U$55" gate="G$1" pin="1"/>
<wire x1="149.86" y1="55.88" x2="106.68" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$41" gate="G$1" pin="1"/>
<junction x="106.68" y="55.88"/>
<wire x1="106.68" y1="55.88" x2="63.5" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$27" gate="G$1" pin="1"/>
<junction x="63.5" y="55.88"/>
<wire x1="63.5" y1="55.88" x2="22.86" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$13" gate="G$1" pin="1"/>
<junction x="22.86" y="55.88"/>
<wire x1="22.86" y1="55.88" x2="193.04" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$69" gate="G$1" pin="1"/>
<junction x="193.04" y="55.88"/>
<wire x1="193.04" y1="55.88" x2="236.22" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$83" gate="G$1" pin="1"/>
<junction x="236.22" y="55.88"/>
<wire x1="236.22" y1="55.88" x2="279.4" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$97" gate="G$1" pin="1"/>
<junction x="279.4" y="55.88"/>
<wire x1="279.4" y1="55.88" x2="279.4" y2="17.78" width="0.1524" layer="91"/>
<pinref part="JP107" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$112" class="0">
<segment>
<pinref part="U$56" gate="G$1" pin="1"/>
<wire x1="152.4" y1="55.88" x2="109.22" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$42" gate="G$1" pin="1"/>
<junction x="109.22" y="55.88"/>
<wire x1="109.22" y1="55.88" x2="66.04" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$28" gate="G$1" pin="1"/>
<junction x="66.04" y="55.88"/>
<wire x1="66.04" y1="55.88" x2="25.4" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$14" gate="G$1" pin="1"/>
<junction x="25.4" y="55.88"/>
<wire x1="25.4" y1="55.88" x2="195.58" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$70" gate="G$1" pin="1"/>
<junction x="195.58" y="55.88"/>
<wire x1="195.58" y1="55.88" x2="238.76" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$84" gate="G$1" pin="1"/>
<junction x="238.76" y="55.88"/>
<wire x1="238.76" y1="55.88" x2="281.94" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$98" gate="G$1" pin="1"/>
<junction x="281.94" y="55.88"/>
<wire x1="281.94" y1="55.88" x2="281.94" y2="17.78" width="0.1524" layer="91"/>
<pinref part="JP106" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U$85" gate="G$1" pin="2"/>
<pinref part="JP91" gate="G$1" pin="1"/>
<wire x1="248.92" y1="73.66" x2="248.92" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U$86" gate="G$1" pin="2"/>
<pinref part="JP92" gate="G$1" pin="1"/>
<wire x1="251.46" y1="73.66" x2="251.46" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U$87" gate="G$1" pin="2"/>
<pinref part="JP93" gate="G$1" pin="1"/>
<wire x1="254" y1="73.66" x2="254" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U$88" gate="G$1" pin="2"/>
<pinref part="JP94" gate="G$1" pin="1"/>
<wire x1="256.54" y1="73.66" x2="256.54" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U$89" gate="G$1" pin="2"/>
<pinref part="JP95" gate="G$1" pin="1"/>
<wire x1="259.08" y1="73.66" x2="259.08" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U$90" gate="G$1" pin="2"/>
<pinref part="JP96" gate="G$1" pin="1"/>
<wire x1="261.62" y1="73.66" x2="261.62" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U$91" gate="G$1" pin="2"/>
<pinref part="JP97" gate="G$1" pin="1"/>
<wire x1="264.16" y1="73.66" x2="264.16" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U$92" gate="G$1" pin="2"/>
<pinref part="JP98" gate="G$1" pin="1"/>
<wire x1="266.7" y1="73.66" x2="266.7" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U$93" gate="G$1" pin="2"/>
<pinref part="JP99" gate="G$1" pin="1"/>
<wire x1="269.24" y1="73.66" x2="269.24" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U$95" gate="G$1" pin="2"/>
<pinref part="JP102" gate="G$1" pin="1"/>
<wire x1="274.32" y1="73.66" x2="274.32" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U$96" gate="G$1" pin="2"/>
<pinref part="JP103" gate="G$1" pin="1"/>
<wire x1="276.86" y1="73.66" x2="276.86" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="U$97" gate="G$1" pin="2"/>
<pinref part="JP104" gate="G$1" pin="1"/>
<wire x1="279.4" y1="73.66" x2="279.4" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="U$98" gate="G$1" pin="2"/>
<pinref part="JP105" gate="G$1" pin="1"/>
<wire x1="281.94" y1="73.66" x2="281.94" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="2"/>
<pinref part="JP1" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="76.2" x2="-7.62" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="2"/>
<pinref part="JP2" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="76.2" x2="-5.08" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="2"/>
<pinref part="JP3" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="76.2" x2="-2.54" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="2"/>
<pinref part="JP4" gate="G$1" pin="1"/>
<wire x1="0" y1="76.2" x2="0" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="2"/>
<pinref part="JP5" gate="G$1" pin="1"/>
<wire x1="2.54" y1="76.2" x2="2.54" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="U$6" gate="G$1" pin="2"/>
<pinref part="JP6" gate="G$1" pin="1"/>
<wire x1="5.08" y1="76.2" x2="5.08" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="2"/>
<pinref part="JP7" gate="G$1" pin="1"/>
<wire x1="7.62" y1="76.2" x2="7.62" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="2"/>
<pinref part="JP8" gate="G$1" pin="1"/>
<wire x1="10.16" y1="76.2" x2="10.16" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="U$9" gate="G$1" pin="2"/>
<pinref part="JP9" gate="G$1" pin="1"/>
<wire x1="12.7" y1="76.2" x2="12.7" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="2"/>
<pinref part="JP12" gate="G$1" pin="1"/>
<wire x1="17.78" y1="76.2" x2="17.78" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="U$12" gate="G$1" pin="2"/>
<pinref part="JP13" gate="G$1" pin="1"/>
<wire x1="20.32" y1="76.2" x2="20.32" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="U$13" gate="G$1" pin="2"/>
<pinref part="JP14" gate="G$1" pin="1"/>
<wire x1="22.86" y1="76.2" x2="22.86" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="U$14" gate="G$1" pin="2"/>
<pinref part="JP15" gate="G$1" pin="1"/>
<wire x1="25.4" y1="76.2" x2="25.4" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="U$15" gate="G$1" pin="2"/>
<pinref part="JP16" gate="G$1" pin="1"/>
<wire x1="33.02" y1="73.66" x2="33.02" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="2"/>
<pinref part="JP17" gate="G$1" pin="1"/>
<wire x1="35.56" y1="73.66" x2="35.56" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="2"/>
<pinref part="JP18" gate="G$1" pin="1"/>
<wire x1="38.1" y1="73.66" x2="38.1" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="U$18" gate="G$1" pin="2"/>
<pinref part="JP19" gate="G$1" pin="1"/>
<wire x1="40.64" y1="73.66" x2="40.64" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="U$19" gate="G$1" pin="2"/>
<pinref part="JP20" gate="G$1" pin="1"/>
<wire x1="43.18" y1="73.66" x2="43.18" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="U$20" gate="G$1" pin="2"/>
<pinref part="JP21" gate="G$1" pin="1"/>
<wire x1="45.72" y1="73.66" x2="45.72" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="U$21" gate="G$1" pin="2"/>
<pinref part="JP22" gate="G$1" pin="1"/>
<wire x1="48.26" y1="73.66" x2="48.26" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="U$22" gate="G$1" pin="2"/>
<pinref part="JP23" gate="G$1" pin="1"/>
<wire x1="50.8" y1="73.66" x2="50.8" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="U$23" gate="G$1" pin="2"/>
<pinref part="JP24" gate="G$1" pin="1"/>
<wire x1="53.34" y1="73.66" x2="53.34" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="U$25" gate="G$1" pin="2"/>
<pinref part="JP27" gate="G$1" pin="1"/>
<wire x1="58.42" y1="73.66" x2="58.42" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="U$26" gate="G$1" pin="2"/>
<pinref part="JP28" gate="G$1" pin="1"/>
<wire x1="60.96" y1="73.66" x2="60.96" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="U$27" gate="G$1" pin="2"/>
<pinref part="JP29" gate="G$1" pin="1"/>
<wire x1="63.5" y1="73.66" x2="63.5" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="U$28" gate="G$1" pin="2"/>
<pinref part="JP30" gate="G$1" pin="1"/>
<wire x1="66.04" y1="73.66" x2="66.04" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="U$29" gate="G$1" pin="2"/>
<pinref part="JP31" gate="G$1" pin="1"/>
<wire x1="76.2" y1="73.66" x2="76.2" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="U$30" gate="G$1" pin="2"/>
<pinref part="JP32" gate="G$1" pin="1"/>
<wire x1="78.74" y1="73.66" x2="78.74" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="U$31" gate="G$1" pin="2"/>
<pinref part="JP33" gate="G$1" pin="1"/>
<wire x1="81.28" y1="73.66" x2="81.28" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="U$32" gate="G$1" pin="2"/>
<pinref part="JP34" gate="G$1" pin="1"/>
<wire x1="83.82" y1="73.66" x2="83.82" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="U$33" gate="G$1" pin="2"/>
<pinref part="JP35" gate="G$1" pin="1"/>
<wire x1="86.36" y1="73.66" x2="86.36" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="U$34" gate="G$1" pin="2"/>
<pinref part="JP36" gate="G$1" pin="1"/>
<wire x1="88.9" y1="73.66" x2="88.9" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="U$35" gate="G$1" pin="2"/>
<pinref part="JP37" gate="G$1" pin="1"/>
<wire x1="91.44" y1="73.66" x2="91.44" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="U$36" gate="G$1" pin="2"/>
<pinref part="JP38" gate="G$1" pin="1"/>
<wire x1="93.98" y1="73.66" x2="93.98" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="U$37" gate="G$1" pin="2"/>
<pinref part="JP39" gate="G$1" pin="1"/>
<wire x1="96.52" y1="73.66" x2="96.52" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="U$39" gate="G$1" pin="2"/>
<pinref part="JP42" gate="G$1" pin="1"/>
<wire x1="101.6" y1="73.66" x2="101.6" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="U$40" gate="G$1" pin="2"/>
<pinref part="JP43" gate="G$1" pin="1"/>
<wire x1="104.14" y1="73.66" x2="104.14" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="U$41" gate="G$1" pin="2"/>
<pinref part="JP44" gate="G$1" pin="1"/>
<wire x1="106.68" y1="73.66" x2="106.68" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="U$42" gate="G$1" pin="2"/>
<pinref part="JP45" gate="G$1" pin="1"/>
<wire x1="109.22" y1="73.66" x2="109.22" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="U$43" gate="G$1" pin="2"/>
<pinref part="JP46" gate="G$1" pin="1"/>
<wire x1="119.38" y1="73.66" x2="119.38" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="U$44" gate="G$1" pin="2"/>
<pinref part="JP47" gate="G$1" pin="1"/>
<wire x1="121.92" y1="73.66" x2="121.92" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="U$45" gate="G$1" pin="2"/>
<pinref part="JP48" gate="G$1" pin="1"/>
<wire x1="124.46" y1="73.66" x2="124.46" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="U$46" gate="G$1" pin="2"/>
<pinref part="JP49" gate="G$1" pin="1"/>
<wire x1="127" y1="73.66" x2="127" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="U$47" gate="G$1" pin="2"/>
<pinref part="JP50" gate="G$1" pin="1"/>
<wire x1="129.54" y1="73.66" x2="129.54" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="U$48" gate="G$1" pin="2"/>
<pinref part="JP51" gate="G$1" pin="1"/>
<wire x1="132.08" y1="73.66" x2="132.08" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="U$49" gate="G$1" pin="2"/>
<pinref part="JP52" gate="G$1" pin="1"/>
<wire x1="134.62" y1="73.66" x2="134.62" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="U$50" gate="G$1" pin="2"/>
<pinref part="JP53" gate="G$1" pin="1"/>
<wire x1="137.16" y1="73.66" x2="137.16" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<pinref part="U$51" gate="G$1" pin="2"/>
<pinref part="JP54" gate="G$1" pin="1"/>
<wire x1="139.7" y1="73.66" x2="139.7" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="U$53" gate="G$1" pin="2"/>
<pinref part="JP57" gate="G$1" pin="1"/>
<wire x1="144.78" y1="73.66" x2="144.78" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="U$54" gate="G$1" pin="2"/>
<pinref part="JP58" gate="G$1" pin="1"/>
<wire x1="147.32" y1="73.66" x2="147.32" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="U$55" gate="G$1" pin="2"/>
<pinref part="JP59" gate="G$1" pin="1"/>
<wire x1="149.86" y1="73.66" x2="149.86" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="U$56" gate="G$1" pin="2"/>
<pinref part="JP60" gate="G$1" pin="1"/>
<wire x1="152.4" y1="73.66" x2="152.4" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<pinref part="U$57" gate="G$1" pin="2"/>
<pinref part="JP61" gate="G$1" pin="1"/>
<wire x1="162.56" y1="73.66" x2="162.56" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="U$58" gate="G$1" pin="2"/>
<pinref part="JP62" gate="G$1" pin="1"/>
<wire x1="165.1" y1="73.66" x2="165.1" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="U$59" gate="G$1" pin="2"/>
<pinref part="JP63" gate="G$1" pin="1"/>
<wire x1="167.64" y1="73.66" x2="167.64" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="U$60" gate="G$1" pin="2"/>
<pinref part="JP64" gate="G$1" pin="1"/>
<wire x1="170.18" y1="73.66" x2="170.18" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<pinref part="U$61" gate="G$1" pin="2"/>
<pinref part="JP65" gate="G$1" pin="1"/>
<wire x1="172.72" y1="73.66" x2="172.72" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="U$62" gate="G$1" pin="2"/>
<pinref part="JP66" gate="G$1" pin="1"/>
<wire x1="175.26" y1="73.66" x2="175.26" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="U$63" gate="G$1" pin="2"/>
<pinref part="JP67" gate="G$1" pin="1"/>
<wire x1="177.8" y1="73.66" x2="177.8" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<pinref part="U$64" gate="G$1" pin="2"/>
<pinref part="JP68" gate="G$1" pin="1"/>
<wire x1="180.34" y1="73.66" x2="180.34" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="U$65" gate="G$1" pin="2"/>
<pinref part="JP69" gate="G$1" pin="1"/>
<wire x1="182.88" y1="73.66" x2="182.88" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="U$67" gate="G$1" pin="2"/>
<pinref part="JP72" gate="G$1" pin="1"/>
<wire x1="187.96" y1="73.66" x2="187.96" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="U$68" gate="G$1" pin="2"/>
<pinref part="JP73" gate="G$1" pin="1"/>
<wire x1="190.5" y1="73.66" x2="190.5" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<pinref part="U$69" gate="G$1" pin="2"/>
<pinref part="JP74" gate="G$1" pin="1"/>
<wire x1="193.04" y1="73.66" x2="193.04" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<pinref part="U$70" gate="G$1" pin="2"/>
<pinref part="JP75" gate="G$1" pin="1"/>
<wire x1="195.58" y1="73.66" x2="195.58" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<pinref part="U$71" gate="G$1" pin="2"/>
<pinref part="JP76" gate="G$1" pin="1"/>
<wire x1="205.74" y1="73.66" x2="205.74" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<pinref part="U$72" gate="G$1" pin="2"/>
<pinref part="JP77" gate="G$1" pin="1"/>
<wire x1="208.28" y1="73.66" x2="208.28" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<pinref part="U$73" gate="G$1" pin="2"/>
<pinref part="JP78" gate="G$1" pin="1"/>
<wire x1="210.82" y1="73.66" x2="210.82" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<pinref part="U$74" gate="G$1" pin="2"/>
<pinref part="JP79" gate="G$1" pin="1"/>
<wire x1="213.36" y1="73.66" x2="213.36" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="U$75" gate="G$1" pin="2"/>
<pinref part="JP80" gate="G$1" pin="1"/>
<wire x1="215.9" y1="73.66" x2="215.9" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<pinref part="U$76" gate="G$1" pin="2"/>
<pinref part="JP81" gate="G$1" pin="1"/>
<wire x1="218.44" y1="73.66" x2="218.44" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<pinref part="U$77" gate="G$1" pin="2"/>
<pinref part="JP82" gate="G$1" pin="1"/>
<wire x1="220.98" y1="73.66" x2="220.98" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<pinref part="U$78" gate="G$1" pin="2"/>
<pinref part="JP83" gate="G$1" pin="1"/>
<wire x1="223.52" y1="73.66" x2="223.52" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<pinref part="U$79" gate="G$1" pin="2"/>
<pinref part="JP84" gate="G$1" pin="1"/>
<wire x1="226.06" y1="73.66" x2="226.06" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<pinref part="U$81" gate="G$1" pin="2"/>
<pinref part="JP87" gate="G$1" pin="1"/>
<wire x1="231.14" y1="73.66" x2="231.14" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<pinref part="U$82" gate="G$1" pin="2"/>
<pinref part="JP88" gate="G$1" pin="1"/>
<wire x1="233.68" y1="73.66" x2="233.68" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<pinref part="U$83" gate="G$1" pin="2"/>
<pinref part="JP89" gate="G$1" pin="1"/>
<wire x1="236.22" y1="73.66" x2="236.22" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="U$84" gate="G$1" pin="2"/>
<pinref part="JP90" gate="G$1" pin="1"/>
<wire x1="238.76" y1="73.66" x2="238.76" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U$10" gate="G$1" pin="2"/>
<pinref part="JP11" gate="G$1" pin="1"/>
<wire x1="15.24" y1="76.2" x2="15.24" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U$24" gate="G$1" pin="2"/>
<pinref part="JP25" gate="G$1" pin="1"/>
<wire x1="55.88" y1="73.66" x2="55.88" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U$38" gate="G$1" pin="2"/>
<pinref part="JP40" gate="G$1" pin="1"/>
<wire x1="99.06" y1="73.66" x2="99.06" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U$52" gate="G$1" pin="2"/>
<pinref part="JP55" gate="G$1" pin="1"/>
<wire x1="142.24" y1="73.66" x2="142.24" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U$66" gate="G$1" pin="2"/>
<pinref part="JP70" gate="G$1" pin="1"/>
<wire x1="185.42" y1="73.66" x2="185.42" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U$80" gate="G$1" pin="2"/>
<pinref part="JP85" gate="G$1" pin="1"/>
<wire x1="228.6" y1="73.66" x2="228.6" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U$94" gate="G$1" pin="2"/>
<pinref part="JP100" gate="G$1" pin="1"/>
<wire x1="271.78" y1="73.66" x2="271.78" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
