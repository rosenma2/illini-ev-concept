<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.1.3">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="BMS" urn="urn:adsk.eagle:library:6828203">
<packages>
<package name="QFP80_SOT496-1" urn="urn:adsk.eagle:footprint:6828271/1" library_version="1">
<description>&lt;b&gt;Plastic Quad Flat Package SOT496&lt;/b&gt; 80 leads &lt;p&gt;
source: http://www.semiconductors.philips.com/&lt;p&gt;
LQFP-MSQFP-QFP-SQFP-TQFP-REFLOW.pdf</description>
<wire x1="-0.25" y1="0.25" x2="11.75" y2="0.25" width="0.127" layer="21"/>
<wire x1="11.75" y1="0.25" x2="11.75" y2="12.25" width="0.127" layer="21"/>
<wire x1="11.75" y1="12.25" x2="-0.25" y2="12.25" width="0.127" layer="21"/>
<wire x1="-0.25" y1="12.25" x2="-0.25" y2="0.25" width="0.127" layer="21"/>
<smd name="P$1" x="1" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$2" x="1.5" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$3" x="2" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$4" x="2.5" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$5" x="3" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$6" x="3.5" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$7" x="4" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$8" x="4.5" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$9" x="5" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$10" x="5.5" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$11" x="6" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$12" x="6.5" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$13" x="7" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$14" x="7.5" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$15" x="8" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$16" x="8.5" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$17" x="9" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$18" x="9.5" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$19" x="10" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$20" x="10.5" y="-1" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$21" x="13" y="1.5" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$22" x="13" y="2" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$23" x="13" y="2.5" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$24" x="13" y="3" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$25" x="13" y="3.5" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$26" x="13" y="4" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$27" x="13" y="4.5" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$28" x="13" y="5" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$29" x="13" y="5.5" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$30" x="13" y="6" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$31" x="13" y="6.5" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$32" x="13" y="7" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$33" x="13" y="7.5" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$34" x="13" y="8" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$35" x="13" y="8.5" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$36" x="13" y="9" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$37" x="13" y="9.5" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$38" x="13" y="10" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$39" x="13" y="10.5" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$40" x="13" y="11" dx="1.75" dy="0.25" layer="1"/>
<smd name="P$41" x="10.5" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$42" x="10" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$43" x="9.5" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$44" x="9" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$45" x="8.5" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$46" x="8" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$47" x="7.5" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$48" x="7" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$49" x="6.5" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$50" x="6" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$51" x="5.5" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$52" x="5" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$53" x="4.5" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$54" x="4" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$55" x="3.5" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$56" x="3" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$57" x="2.5" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$58" x="2" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$59" x="1.5" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$60" x="1" y="13.5" dx="1.75" dy="0.25" layer="1" rot="R90"/>
<smd name="P$61" x="-1.5" y="11" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$62" x="-1.5" y="10.5" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$63" x="-1.5" y="10" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$64" x="-1.5" y="9.5" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$65" x="-1.5" y="9" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$66" x="-1.5" y="8.5" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$67" x="-1.5" y="8" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$68" x="-1.5" y="7.5" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$69" x="-1.5" y="7" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$70" x="-1.5" y="6.5" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$71" x="-1.5" y="6" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$72" x="-1.5" y="5.5" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$73" x="-1.5" y="5" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$74" x="-1.5" y="4.5" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$75" x="-1.5" y="4" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$76" x="-1.5" y="3.5" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$77" x="-1.5" y="3" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$78" x="-1.5" y="2.5" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$79" x="-1.5" y="2" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<smd name="P$80" x="-1.5" y="1.5" dx="1.75" dy="0.25" layer="1" rot="R180"/>
<text x="0.75" y="9" size="1.27" layer="25">BQ76PL455A</text>
<circle x="1" y="1.5" radius="0.5" width="0.127" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="QFP80_SOT496-1" urn="urn:adsk.eagle:package:6828346/1" type="box" library_version="1">
<description>&lt;b&gt;Plastic Quad Flat Package SOT496&lt;/b&gt; 80 leads &lt;p&gt;
source: http://www.semiconductors.philips.com/&lt;p&gt;
LQFP-MSQFP-QFP-SQFP-TQFP-REFLOW.pdf</description>
<packageinstances>
<packageinstance name="QFP80_SOT496-1"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="BQ76PL455A" urn="urn:adsk.eagle:symbol:6828232/1" library_version="1">
<pin name="P$1" x="-5.08" y="106.68" length="middle"/>
<pin name="P$2" x="-5.08" y="101.6" length="middle"/>
<pin name="P$3" x="-5.08" y="96.52" length="middle"/>
<pin name="P$4" x="-5.08" y="91.44" length="middle"/>
<pin name="P$5" x="-5.08" y="86.36" length="middle"/>
<pin name="P$6" x="-5.08" y="81.28" length="middle"/>
<pin name="P$7" x="-5.08" y="76.2" length="middle"/>
<pin name="P$8" x="-5.08" y="71.12" length="middle"/>
<pin name="P$9" x="-5.08" y="66.04" length="middle"/>
<pin name="P$10" x="-5.08" y="60.96" length="middle"/>
<pin name="P$11" x="-5.08" y="55.88" length="middle"/>
<pin name="P$12" x="-5.08" y="50.8" length="middle"/>
<pin name="P$13" x="-5.08" y="45.72" length="middle"/>
<pin name="P$14" x="-5.08" y="40.64" length="middle"/>
<pin name="P$15" x="-5.08" y="35.56" length="middle"/>
<pin name="P$16" x="-5.08" y="30.48" length="middle"/>
<pin name="P$17" x="-5.08" y="25.4" length="middle"/>
<pin name="P$18" x="-5.08" y="20.32" length="middle"/>
<pin name="P$19" x="-5.08" y="15.24" length="middle"/>
<pin name="P$20" x="-5.08" y="10.16" length="middle"/>
<pin name="P$21" x="10.16" y="-5.08" length="middle" rot="R90"/>
<pin name="P$22" x="15.24" y="-5.08" length="middle" rot="R90"/>
<pin name="P$23" x="20.32" y="-5.08" length="middle" rot="R90"/>
<pin name="P$24" x="25.4" y="-5.08" length="middle" rot="R90"/>
<pin name="P$25" x="30.48" y="-5.08" length="middle" rot="R90"/>
<pin name="P$26" x="35.56" y="-5.08" length="middle" rot="R90"/>
<pin name="P$27" x="40.64" y="-5.08" length="middle" rot="R90"/>
<pin name="P$28" x="45.72" y="-5.08" length="middle" rot="R90"/>
<pin name="P$29" x="50.8" y="-5.08" length="middle" rot="R90"/>
<pin name="P$30" x="55.88" y="-5.08" length="middle" rot="R90"/>
<pin name="P$31" x="60.96" y="-5.08" length="middle" rot="R90"/>
<pin name="P$32" x="66.04" y="-5.08" length="middle" rot="R90"/>
<pin name="P$33" x="71.12" y="-5.08" length="middle" rot="R90"/>
<pin name="P$34" x="76.2" y="-5.08" length="middle" rot="R90"/>
<pin name="P$35" x="81.28" y="-5.08" length="middle" rot="R90"/>
<pin name="P$36" x="86.36" y="-5.08" length="middle" rot="R90"/>
<pin name="P$37" x="91.44" y="-5.08" length="middle" rot="R90"/>
<pin name="P$38" x="96.52" y="-5.08" length="middle" rot="R90"/>
<pin name="P$39" x="101.6" y="-5.08" length="middle" rot="R90"/>
<pin name="P$40" x="106.68" y="-5.08" length="middle" rot="R90"/>
<pin name="P$41" x="121.92" y="10.16" length="middle" rot="R180"/>
<pin name="P$42" x="121.92" y="15.24" length="middle" rot="R180"/>
<pin name="P$43" x="121.92" y="20.32" length="middle" rot="R180"/>
<pin name="P$44" x="121.92" y="25.4" length="middle" rot="R180"/>
<pin name="P$45" x="121.92" y="30.48" length="middle" rot="R180"/>
<pin name="P$46" x="121.92" y="35.56" length="middle" rot="R180"/>
<pin name="P$47" x="121.92" y="40.64" length="middle" rot="R180"/>
<pin name="P$48" x="121.92" y="45.72" length="middle" rot="R180"/>
<pin name="P$49" x="121.92" y="50.8" length="middle" rot="R180"/>
<pin name="P$50" x="121.92" y="55.88" length="middle" rot="R180"/>
<pin name="P$51" x="121.92" y="60.96" length="middle" rot="R180"/>
<pin name="P$52" x="121.92" y="66.04" length="middle" rot="R180"/>
<pin name="P$53" x="121.92" y="71.12" length="middle" rot="R180"/>
<pin name="P$54" x="121.92" y="76.2" length="middle" rot="R180"/>
<pin name="P$55" x="121.92" y="81.28" length="middle" rot="R180"/>
<pin name="P$56" x="121.92" y="86.36" length="middle" rot="R180"/>
<pin name="P$57" x="121.92" y="91.44" length="middle" rot="R180"/>
<pin name="P$58" x="121.92" y="96.52" length="middle" rot="R180"/>
<pin name="P$59" x="121.92" y="101.6" length="middle" rot="R180"/>
<pin name="P$60" x="121.92" y="106.68" length="middle" rot="R180"/>
<pin name="P$61" x="106.68" y="121.92" length="middle" rot="R270"/>
<pin name="P$62" x="101.6" y="121.92" length="middle" rot="R270"/>
<pin name="P$63" x="96.52" y="121.92" length="middle" rot="R270"/>
<pin name="P$64" x="91.44" y="121.92" length="middle" rot="R270"/>
<pin name="P$65" x="86.36" y="121.92" length="middle" rot="R270"/>
<pin name="P$66" x="81.28" y="121.92" length="middle" rot="R270"/>
<pin name="P$67" x="76.2" y="121.92" length="middle" rot="R270"/>
<pin name="P$68" x="71.12" y="121.92" length="middle" rot="R270"/>
<pin name="P$69" x="66.04" y="121.92" length="middle" rot="R270"/>
<pin name="P$70" x="60.96" y="121.92" length="middle" rot="R270"/>
<pin name="P$71" x="55.88" y="121.92" length="middle" rot="R270"/>
<pin name="P$72" x="50.8" y="121.92" length="middle" rot="R270"/>
<pin name="P$73" x="45.72" y="121.92" length="middle" rot="R270"/>
<pin name="P$74" x="40.64" y="121.92" length="middle" rot="R270"/>
<pin name="P$75" x="35.56" y="121.92" length="middle" rot="R270"/>
<pin name="P$76" x="30.48" y="121.92" length="middle" rot="R270"/>
<pin name="P$77" x="25.4" y="121.92" length="middle" rot="R270"/>
<pin name="P$78" x="20.32" y="121.92" length="middle" rot="R270"/>
<pin name="P$79" x="15.24" y="121.92" length="middle" rot="R270"/>
<pin name="P$80" x="10.16" y="121.92" length="middle" rot="R270"/>
<wire x1="0" y1="116.84" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="116.84" y2="0" width="0.254" layer="94"/>
<wire x1="116.84" y1="0" x2="116.84" y2="116.84" width="0.254" layer="94"/>
<wire x1="116.84" y1="116.84" x2="0" y2="116.84" width="0.254" layer="94"/>
<text x="38.1" y="58.42" size="5.08" layer="94">BQ76PL455A</text>
<circle x="7.62" y="109.22" radius="2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BQ76PL455ATPFCRQ1-NEW" urn="urn:adsk.eagle:component:6828411/1" library_version="1">
<gates>
<gate name="G$1" symbol="BQ76PL455A" x="88.9" y="-73.66"/>
</gates>
<devices>
<device name="" package="QFP80_SOT496-1">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$10" pad="P$10"/>
<connect gate="G$1" pin="P$11" pad="P$11"/>
<connect gate="G$1" pin="P$12" pad="P$12"/>
<connect gate="G$1" pin="P$13" pad="P$13"/>
<connect gate="G$1" pin="P$14" pad="P$14"/>
<connect gate="G$1" pin="P$15" pad="P$15"/>
<connect gate="G$1" pin="P$16" pad="P$16"/>
<connect gate="G$1" pin="P$17" pad="P$17"/>
<connect gate="G$1" pin="P$18" pad="P$18"/>
<connect gate="G$1" pin="P$19" pad="P$19"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$20" pad="P$20"/>
<connect gate="G$1" pin="P$21" pad="P$21"/>
<connect gate="G$1" pin="P$22" pad="P$22"/>
<connect gate="G$1" pin="P$23" pad="P$23"/>
<connect gate="G$1" pin="P$24" pad="P$24"/>
<connect gate="G$1" pin="P$25" pad="P$25"/>
<connect gate="G$1" pin="P$26" pad="P$26"/>
<connect gate="G$1" pin="P$27" pad="P$27"/>
<connect gate="G$1" pin="P$28" pad="P$28"/>
<connect gate="G$1" pin="P$29" pad="P$29"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$30" pad="P$30"/>
<connect gate="G$1" pin="P$31" pad="P$31"/>
<connect gate="G$1" pin="P$32" pad="P$32"/>
<connect gate="G$1" pin="P$33" pad="P$33"/>
<connect gate="G$1" pin="P$34" pad="P$34"/>
<connect gate="G$1" pin="P$35" pad="P$35"/>
<connect gate="G$1" pin="P$36" pad="P$36"/>
<connect gate="G$1" pin="P$37" pad="P$37"/>
<connect gate="G$1" pin="P$38" pad="P$38"/>
<connect gate="G$1" pin="P$39" pad="P$39"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
<connect gate="G$1" pin="P$40" pad="P$40"/>
<connect gate="G$1" pin="P$41" pad="P$41"/>
<connect gate="G$1" pin="P$42" pad="P$42"/>
<connect gate="G$1" pin="P$43" pad="P$43"/>
<connect gate="G$1" pin="P$44" pad="P$44"/>
<connect gate="G$1" pin="P$45" pad="P$45"/>
<connect gate="G$1" pin="P$46" pad="P$46"/>
<connect gate="G$1" pin="P$47" pad="P$47"/>
<connect gate="G$1" pin="P$48" pad="P$48"/>
<connect gate="G$1" pin="P$49" pad="P$49"/>
<connect gate="G$1" pin="P$5" pad="P$5"/>
<connect gate="G$1" pin="P$50" pad="P$50"/>
<connect gate="G$1" pin="P$51" pad="P$51"/>
<connect gate="G$1" pin="P$52" pad="P$52"/>
<connect gate="G$1" pin="P$53" pad="P$53"/>
<connect gate="G$1" pin="P$54" pad="P$54"/>
<connect gate="G$1" pin="P$55" pad="P$55"/>
<connect gate="G$1" pin="P$56" pad="P$56"/>
<connect gate="G$1" pin="P$57" pad="P$57"/>
<connect gate="G$1" pin="P$58" pad="P$58"/>
<connect gate="G$1" pin="P$59" pad="P$59"/>
<connect gate="G$1" pin="P$6" pad="P$6"/>
<connect gate="G$1" pin="P$60" pad="P$60"/>
<connect gate="G$1" pin="P$61" pad="P$61"/>
<connect gate="G$1" pin="P$62" pad="P$62"/>
<connect gate="G$1" pin="P$63" pad="P$63"/>
<connect gate="G$1" pin="P$64" pad="P$64"/>
<connect gate="G$1" pin="P$65" pad="P$65"/>
<connect gate="G$1" pin="P$66" pad="P$66"/>
<connect gate="G$1" pin="P$67" pad="P$67"/>
<connect gate="G$1" pin="P$68" pad="P$68"/>
<connect gate="G$1" pin="P$69" pad="P$69"/>
<connect gate="G$1" pin="P$7" pad="P$7"/>
<connect gate="G$1" pin="P$70" pad="P$70"/>
<connect gate="G$1" pin="P$71" pad="P$71"/>
<connect gate="G$1" pin="P$72" pad="P$72"/>
<connect gate="G$1" pin="P$73" pad="P$73"/>
<connect gate="G$1" pin="P$74" pad="P$74"/>
<connect gate="G$1" pin="P$75" pad="P$75"/>
<connect gate="G$1" pin="P$76" pad="P$76"/>
<connect gate="G$1" pin="P$77" pad="P$77"/>
<connect gate="G$1" pin="P$78" pad="P$78"/>
<connect gate="G$1" pin="P$79" pad="P$79"/>
<connect gate="G$1" pin="P$8" pad="P$8"/>
<connect gate="G$1" pin="P$80" pad="P$80"/>
<connect gate="G$1" pin="P$9" pad="P$9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6828346/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="BMS" library_urn="urn:adsk.eagle:library:6828203" deviceset="BQ76PL455ATPFCRQ1-NEW" device="" package3d_urn="urn:adsk.eagle:package:6828346/1"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="0" y="0"/>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
