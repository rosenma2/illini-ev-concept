<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.5.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.05" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
</parts>
<sheets>
<sheet>
<plain>
<text x="46.99" y="50.8" size="1.778" layer="95">RegFile</text>
<text x="43.18" y="58.42" size="1.778" layer="95">SR1</text>
<text x="44.45" y="45.72" size="1.778" layer="95">SR2</text>
<text x="53.34" y="43.18" size="1.778" layer="95">SR2
OUT</text>
<text x="53.34" y="55.88" size="1.778" layer="95">SR1
OUT</text>
<wire x1="-8.89" y1="184.15" x2="-8.89" y2="168.91" width="0.1524" layer="94"/>
<wire x1="-8.89" y1="168.91" x2="-3.81" y2="171.45" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="171.45" x2="-3.81" y2="181.61" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="181.61" x2="-8.89" y2="184.15" width="0.1524" layer="94"/>
<wire x1="43.18" y1="66.04" x2="43.18" y2="38.1" width="0.1524" layer="94"/>
<wire x1="43.18" y1="38.1" x2="58.42" y2="38.1" width="0.1524" layer="94"/>
<wire x1="58.42" y1="38.1" x2="58.42" y2="66.04" width="0.1524" layer="94"/>
<wire x1="58.42" y1="66.04" x2="43.18" y2="66.04" width="0.1524" layer="94"/>
<text x="-7.62" y="172.72" size="1.778" layer="95">0

1</text>
<text x="15.875" y="176.53" size="3.556" layer="95">+</text>
<wire x1="15.24" y1="185.42" x2="15.24" y2="170.18" width="0.1524" layer="94"/>
<wire x1="15.24" y1="170.18" x2="20.32" y2="172.72" width="0.1524" layer="94"/>
<wire x1="20.32" y1="172.72" x2="20.32" y2="182.88" width="0.1524" layer="94"/>
<wire x1="20.32" y1="182.88" x2="15.24" y2="185.42" width="0.1524" layer="94"/>
<wire x1="24.13" y1="66.04" x2="24.13" y2="50.8" width="0.1524" layer="94"/>
<wire x1="24.13" y1="50.8" x2="29.21" y2="53.34" width="0.1524" layer="94"/>
<wire x1="29.21" y1="53.34" x2="29.21" y2="63.5" width="0.1524" layer="94"/>
<wire x1="29.21" y1="63.5" x2="24.13" y2="66.04" width="0.1524" layer="94"/>
<text x="25.4" y="54.61" size="1.778" layer="95">0

1</text>
<wire x1="0" y1="66.04" x2="0" y2="26.67" width="0.1524" layer="94"/>
<wire x1="0" y1="26.67" x2="-16.51" y2="26.67" width="0.1524" layer="94"/>
<wire x1="-16.51" y1="26.67" x2="-16.51" y2="66.04" width="0.1524" layer="94"/>
<wire x1="-16.51" y1="66.04" x2="0" y2="66.04" width="0.1524" layer="94"/>
<text x="-10.16" y="48.26" size="1.778" layer="95">IR</text>
<text x="-6.35" y="59.69" size="1.778" layer="95">SR1</text>
<text x="-6.35" y="45.72" size="1.778" layer="95">SR2</text>
<text x="-6.35" y="40.64" size="1.778" layer="95">dest</text>
<text x="20.32" y="71.12" size="1.778" layer="95">storemux_sel</text>
<text x="44.45" y="40.64" size="1.778" layer="95">dest</text>
<wire x1="21.59" y1="26.67" x2="21.59" y2="20.32" width="0.1524" layer="94"/>
<wire x1="21.59" y1="20.32" x2="34.29" y2="20.32" width="0.1524" layer="94"/>
<wire x1="34.29" y1="20.32" x2="34.29" y2="26.67" width="0.1524" layer="94"/>
<wire x1="34.29" y1="26.67" x2="21.59" y2="26.67" width="0.1524" layer="94"/>
<text x="25.4" y="22.86" size="1.778" layer="95">ADJ6</text>
<text x="6.35" y="30.48" size="1.778" layer="95">offset6</text>
<wire x1="83.82" y1="48.26" x2="83.82" y2="33.02" width="0.1524" layer="94"/>
<wire x1="83.82" y1="33.02" x2="88.9" y2="35.56" width="0.1524" layer="94"/>
<wire x1="88.9" y1="35.56" x2="88.9" y2="45.72" width="0.1524" layer="94"/>
<wire x1="88.9" y1="45.72" x2="83.82" y2="48.26" width="0.1524" layer="94"/>
<text x="85.09" y="36.83" size="1.778" layer="95">0

1</text>
<wire x1="96.52" y1="60.96" x2="96.52" y2="55.88" width="0.1524" layer="95"/>
<wire x1="96.52" y1="55.88" x2="100.33" y2="53.34" width="0.1524" layer="95"/>
<wire x1="95.25" y1="45.72" x2="95.25" y2="40.64" width="0.1524" layer="95"/>
<wire x1="100.33" y1="48.26" x2="100.33" y2="53.34" width="0.1524" layer="95"/>
<wire x1="95.25" y1="40.64" x2="105.41" y2="46.99" width="0.1524" layer="95"/>
<wire x1="96.52" y1="60.96" x2="105.41" y2="55.88" width="0.1524" layer="95"/>
<wire x1="105.41" y1="46.99" x2="105.41" y2="55.88" width="0.1524" layer="95"/>
<wire x1="95.25" y1="45.72" x2="100.33" y2="48.26" width="0.1524" layer="95"/>
<text x="100.33" y="63.5" size="1.778" layer="95">aluop</text>
<text x="80.01" y="52.07" size="1.778" layer="95">alumux_sel</text>
<wire x1="43.18" y1="-12.7" x2="58.42" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="58.42" y1="-12.7" x2="55.88" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="55.88" y1="-7.62" x2="45.72" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="45.72" y1="-7.62" x2="43.18" y2="-12.7" width="0.1524" layer="94"/>
<text x="54.61" y="-11.43" size="1.778" layer="95" rot="R90">0

1</text>
<wire x1="64.77" y1="-1.27" x2="64.77" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="64.77" y1="-8.89" x2="77.47" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="77.47" y1="-8.89" x2="77.47" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="77.47" y1="-1.27" x2="64.77" y2="-1.27" width="0.1524" layer="94"/>
<text x="71.12" y="-5.08" size="1.778" layer="95" align="center">GENCC</text>
<wire x1="29.21" y1="176.53" x2="29.21" y2="170.18" width="0.1524" layer="94"/>
<wire x1="29.21" y1="170.18" x2="41.91" y2="170.18" width="0.1524" layer="94"/>
<wire x1="41.91" y1="170.18" x2="41.91" y2="176.53" width="0.1524" layer="94"/>
<wire x1="41.91" y1="176.53" x2="29.21" y2="176.53" width="0.1524" layer="94"/>
<text x="33.02" y="172.72" size="1.778" layer="95">ADJ6</text>
<wire x1="85.09" y1="-1.27" x2="85.09" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="85.09" y1="-8.89" x2="97.79" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="97.79" y1="-8.89" x2="97.79" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="97.79" y1="-1.27" x2="85.09" y2="-1.27" width="0.1524" layer="94"/>
<text x="91.44" y="-5.08" size="1.778" layer="95" align="center">CC</text>
<wire x1="107.95" y1="3.81" x2="107.95" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="107.95" y1="-8.89" x2="120.65" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="120.65" y1="-8.89" x2="120.65" y2="3.81" width="0.1524" layer="94"/>
<wire x1="120.65" y1="3.81" x2="107.95" y2="3.81" width="0.1524" layer="94"/>
<text x="114.3" y="-2.54" size="1.778" layer="95" align="center">CCCOMP</text>
<text x="22.86" y="-11.43" size="1.778" layer="95">regfilemux_sel</text>
<text x="-25.4" y="46.99" size="1.778" layer="95">opcode</text>
<text x="-25.4" y="53.34" size="1.778" layer="95">offset9</text>
<wire x1="-46.99" y1="71.12" x2="-46.99" y2="64.77" width="0.1524" layer="94"/>
<wire x1="-46.99" y1="64.77" x2="-34.29" y2="64.77" width="0.1524" layer="94"/>
<wire x1="-34.29" y1="64.77" x2="-34.29" y2="71.12" width="0.1524" layer="94"/>
<wire x1="-34.29" y1="71.12" x2="-46.99" y2="71.12" width="0.1524" layer="94"/>
<text x="-43.18" y="67.31" size="1.778" layer="95">ADJ9</text>
<text x="4.445" y="118.11" size="3.556" layer="95">+</text>
<wire x1="3.81" y1="127" x2="3.81" y2="111.76" width="0.1524" layer="94"/>
<wire x1="3.81" y1="111.76" x2="8.89" y2="114.3" width="0.1524" layer="94"/>
<wire x1="8.89" y1="114.3" x2="8.89" y2="124.46" width="0.1524" layer="94"/>
<wire x1="8.89" y1="124.46" x2="3.81" y2="127" width="0.1524" layer="94"/>
<wire x1="-16.51" y1="129.54" x2="-16.51" y2="114.3" width="0.1524" layer="94"/>
<wire x1="-16.51" y1="114.3" x2="-11.43" y2="116.84" width="0.1524" layer="94"/>
<wire x1="-11.43" y1="116.84" x2="-11.43" y2="127" width="0.1524" layer="94"/>
<wire x1="-11.43" y1="127" x2="-16.51" y2="129.54" width="0.1524" layer="94"/>
<text x="-15.24" y="118.11" size="1.778" layer="95">0

1</text>
<wire x1="44.45" y1="124.46" x2="44.45" y2="119.38" width="0.1524" layer="94"/>
<wire x1="44.45" y1="119.38" x2="49.53" y2="119.38" width="0.1524" layer="94"/>
<wire x1="49.53" y1="119.38" x2="49.53" y2="124.46" width="0.1524" layer="94"/>
<wire x1="49.53" y1="124.46" x2="44.45" y2="124.46" width="0.1524" layer="94"/>
<text x="46.99" y="121.92" size="1.778" layer="95" align="center">PC</text>
<wire x1="54.61" y1="175.26" x2="54.61" y2="170.18" width="0.1524" layer="94"/>
<wire x1="54.61" y1="170.18" x2="59.69" y2="170.18" width="0.1524" layer="94"/>
<wire x1="59.69" y1="170.18" x2="59.69" y2="175.26" width="0.1524" layer="94"/>
<wire x1="59.69" y1="175.26" x2="54.61" y2="175.26" width="0.1524" layer="94"/>
<text x="57.15" y="172.72" size="1.778" layer="95" align="center">PC</text>
<text x="43.18" y="129.54" size="1.778" layer="95">load_pc</text>
<text x="11.43" y="134.62" size="1.778" layer="95">pcmux_sel</text>
<wire x1="13.97" y1="129.54" x2="13.97" y2="114.3" width="0.1524" layer="94"/>
<wire x1="13.97" y1="114.3" x2="19.05" y2="116.84" width="0.1524" layer="94"/>
<wire x1="19.05" y1="116.84" x2="19.05" y2="127" width="0.1524" layer="94"/>
<wire x1="19.05" y1="127" x2="13.97" y2="129.54" width="0.1524" layer="94"/>
<text x="15.24" y="118.11" size="1.778" layer="95">0

1</text>
<wire x1="19.05" y1="147.32" x2="19.05" y2="142.24" width="0.1524" layer="94"/>
<wire x1="19.05" y1="142.24" x2="24.13" y2="142.24" width="0.1524" layer="94"/>
<wire x1="24.13" y1="142.24" x2="24.13" y2="147.32" width="0.1524" layer="94"/>
<wire x1="24.13" y1="147.32" x2="19.05" y2="147.32" width="0.1524" layer="94"/>
<text x="21.59" y="144.78" size="1.778" layer="95" align="center">+2</text>
<text x="-11.43" y="63.5" size="1.778" layer="95">load_ir</text>
<wire x1="-33.02" y1="71.12" x2="-33.02" y2="64.77" width="0.1524" layer="94"/>
<wire x1="-33.02" y1="64.77" x2="-20.32" y2="64.77" width="0.1524" layer="94"/>
<wire x1="-20.32" y1="64.77" x2="-20.32" y2="71.12" width="0.1524" layer="94"/>
<wire x1="-20.32" y1="71.12" x2="-33.02" y2="71.12" width="0.1524" layer="94"/>
<text x="-30.48" y="67.31" size="1.778" layer="95">ADJ11</text>
<text x="-25.4" y="59.69" size="1.778" layer="95">offset11</text>
<text x="-20.32" y="134.62" size="1.778" layer="95">pc_offset_sel</text>
<wire x1="102.87" y1="148.59" x2="102.87" y2="133.35" width="0.1524" layer="94"/>
<wire x1="102.87" y1="133.35" x2="107.95" y2="135.89" width="0.1524" layer="94"/>
<wire x1="107.95" y1="135.89" x2="107.95" y2="146.05" width="0.1524" layer="94"/>
<wire x1="107.95" y1="146.05" x2="102.87" y2="148.59" width="0.1524" layer="94"/>
<text x="104.14" y="137.16" size="1.778" layer="95">0

1</text>
<wire x1="91.44" y1="125.73" x2="91.44" y2="110.49" width="0.1524" layer="94"/>
<wire x1="91.44" y1="110.49" x2="96.52" y2="113.03" width="0.1524" layer="94"/>
<wire x1="96.52" y1="113.03" x2="96.52" y2="123.19" width="0.1524" layer="94"/>
<wire x1="96.52" y1="123.19" x2="91.44" y2="125.73" width="0.1524" layer="94"/>
<text x="92.71" y="114.3" size="1.778" layer="95">0

1</text>
<wire x1="113.03" y1="143.51" x2="113.03" y2="138.43" width="0.1524" layer="94"/>
<wire x1="113.03" y1="138.43" x2="118.11" y2="138.43" width="0.1524" layer="94"/>
<wire x1="118.11" y1="138.43" x2="118.11" y2="143.51" width="0.1524" layer="94"/>
<wire x1="118.11" y1="143.51" x2="113.03" y2="143.51" width="0.1524" layer="94"/>
<text x="115.57" y="140.97" size="1.778" layer="95" align="center">MAR</text>
<wire x1="116.84" y1="120.65" x2="116.84" y2="115.57" width="0.1524" layer="94"/>
<wire x1="116.84" y1="115.57" x2="121.92" y2="115.57" width="0.1524" layer="94"/>
<wire x1="121.92" y1="115.57" x2="121.92" y2="120.65" width="0.1524" layer="94"/>
<wire x1="121.92" y1="120.65" x2="116.84" y2="120.65" width="0.1524" layer="94"/>
<text x="119.38" y="118.11" size="1.778" layer="95" align="center">MDR</text>
<text x="110.49" y="148.59" size="1.778" layer="95">load_mar</text>
<text x="114.3" y="125.73" size="1.778" layer="95">load_mdr</text>
<text x="59.69" y="115.57" size="1.778" layer="95">mem_rdata</text>
<text x="132.08" y="142.24" size="1.778" layer="95">mem_address</text>
<text x="153.67" y="119.38" size="1.778" layer="95">mem_wdata</text>
<text x="123.19" y="-1.27" size="1.778" layer="95">branch_enable</text>
<text x="-8.89" y="27.94" size="1.778" layer="95">in</text>
<text x="100.33" y="156.21" size="1.778" layer="95">marmux_sel</text>
<text x="92.71" y="125.73" size="1.778" layer="95">mdrmux_sel</text>
<wire x1="73.66" y1="50.8" x2="73.66" y2="35.56" width="0.1524" layer="94"/>
<wire x1="73.66" y1="35.56" x2="78.74" y2="38.1" width="0.1524" layer="94"/>
<wire x1="78.74" y1="38.1" x2="78.74" y2="48.26" width="0.1524" layer="94"/>
<wire x1="78.74" y1="48.26" x2="73.66" y2="50.8" width="0.1524" layer="94"/>
<text x="74.93" y="39.37" size="1.778" layer="95">0

1</text>
<wire x1="53.34" y1="36.83" x2="53.34" y2="31.75" width="0.1524" layer="94"/>
<wire x1="53.34" y1="31.75" x2="66.04" y2="31.75" width="0.1524" layer="94"/>
<wire x1="66.04" y1="31.75" x2="66.04" y2="36.83" width="0.1524" layer="94"/>
<wire x1="66.04" y1="36.83" x2="53.34" y2="36.83" width="0.1524" layer="94"/>
<text x="55.88" y="33.02" size="1.778" layer="95">SEXT</text>
<text x="6.35" y="34.29" size="1.778" layer="95">offset5</text>
<wire x1="34.29" y1="129.54" x2="34.29" y2="114.3" width="0.1524" layer="94"/>
<wire x1="34.29" y1="114.3" x2="39.37" y2="116.84" width="0.1524" layer="94"/>
<wire x1="39.37" y1="116.84" x2="39.37" y2="127" width="0.1524" layer="94"/>
<wire x1="39.37" y1="127" x2="34.29" y2="129.54" width="0.1524" layer="94"/>
<text x="35.56" y="118.11" size="1.778" layer="95">0

1</text>
<text x="30.48" y="134.62" size="1.778" layer="95">adder_reg_sel</text>
<text x="69.85" y="55.88" size="1.778" layer="95">imm5_sr2_sel</text>
<text x="45.72" y="71.12" size="1.778" layer="95">load_regfile</text>
<wire x1="33.02" y1="44.45" x2="33.02" y2="36.83" width="0.1524" layer="94"/>
<wire x1="33.02" y1="36.83" x2="38.1" y2="39.37" width="0.1524" layer="94"/>
<wire x1="38.1" y1="39.37" x2="38.1" y2="41.91" width="0.1524" layer="94"/>
<wire x1="38.1" y1="41.91" x2="33.02" y2="44.45" width="0.1524" layer="94"/>
<text x="34.29" y="38.1" size="1.778" layer="95">0
1</text>
<text x="31.75" y="44.45" size="0.889" layer="95">TRAP_REG_SEL</text>
<text x="22.86" y="38.1" size="1.778" layer="95">111</text>
<wire x1="88.9" y1="154.94" x2="88.9" y2="139.7" width="0.1524" layer="94"/>
<wire x1="88.9" y1="139.7" x2="93.98" y2="142.24" width="0.1524" layer="94"/>
<wire x1="93.98" y1="142.24" x2="93.98" y2="152.4" width="0.1524" layer="94"/>
<wire x1="93.98" y1="152.4" x2="88.9" y2="154.94" width="0.1524" layer="94"/>
<text x="90.17" y="143.51" size="1.778" layer="95">0

1</text>
<text x="85.09" y="160.02" size="1.778" layer="95">trap_mar_sel</text>
<wire x1="83.82" y1="140.97" x2="83.82" y2="125.73" width="0.1524" layer="94"/>
<wire x1="83.82" y1="125.73" x2="88.9" y2="128.27" width="0.1524" layer="94"/>
<wire x1="88.9" y1="128.27" x2="88.9" y2="138.43" width="0.1524" layer="94"/>
<wire x1="88.9" y1="138.43" x2="83.82" y2="140.97" width="0.1524" layer="94"/>
<text x="85.09" y="129.54" size="1.778" layer="95">0

1</text>
<text x="59.69" y="142.24" size="1.778" layer="95">marmux_mdr_sel</text>
<wire x1="45.72" y1="-34.29" x2="60.96" y2="-34.29" width="0.1524" layer="94"/>
<wire x1="60.96" y1="-34.29" x2="58.42" y2="-29.21" width="0.1524" layer="94"/>
<wire x1="58.42" y1="-29.21" x2="48.26" y2="-29.21" width="0.1524" layer="94"/>
<wire x1="48.26" y1="-29.21" x2="45.72" y2="-34.29" width="0.1524" layer="94"/>
<text x="57.15" y="-33.02" size="1.778" layer="95" rot="R90">0

1</text>
<text x="27.94" y="-33.02" size="1.778" layer="95">lea_mux_sel</text>
<wire x1="29.21" y1="-24.13" x2="44.45" y2="-24.13" width="0.1524" layer="94"/>
<wire x1="44.45" y1="-24.13" x2="41.91" y2="-19.05" width="0.1524" layer="94"/>
<wire x1="41.91" y1="-19.05" x2="31.75" y2="-19.05" width="0.1524" layer="94"/>
<wire x1="31.75" y1="-19.05" x2="29.21" y2="-24.13" width="0.1524" layer="94"/>
<text x="40.64" y="-22.86" size="1.778" layer="95" rot="R90">0

1</text>
<text x="16.51" y="-20.32" size="1.778" layer="95">jsr_mux_sel</text>
<text x="-15.24" y="35.56" size="1.778" layer="95">IR5</text>
<text x="-15.24" y="31.75" size="1.778" layer="95">IR11</text>
<text x="-36.83" y="35.56" size="1.778" layer="95">imm5_sr2_sel</text>
<text x="-38.1" y="31.75" size="1.778" layer="95">jsr_mux_sel</text>
<wire x1="21.59" y1="19.05" x2="21.59" y2="13.97" width="0.1524" layer="94"/>
<wire x1="21.59" y1="13.97" x2="34.29" y2="13.97" width="0.1524" layer="94"/>
<wire x1="34.29" y1="13.97" x2="34.29" y2="19.05" width="0.1524" layer="94"/>
<wire x1="34.29" y1="19.05" x2="21.59" y2="19.05" width="0.1524" layer="94"/>
<text x="25.4" y="15.24" size="1.778" layer="95">SEXT</text>
<wire x1="73.66" y1="27.94" x2="73.66" y2="12.7" width="0.1524" layer="94"/>
<wire x1="73.66" y1="12.7" x2="78.74" y2="15.24" width="0.1524" layer="94"/>
<wire x1="78.74" y1="15.24" x2="78.74" y2="25.4" width="0.1524" layer="94"/>
<wire x1="78.74" y1="25.4" x2="73.66" y2="27.94" width="0.1524" layer="94"/>
<text x="74.93" y="16.51" size="1.778" layer="95">0

1</text>
<text x="68.58" y="33.02" size="1.27" layer="95">offset6_mux_sel</text>
<wire x1="46.99" y1="-54.61" x2="53.34" y2="-54.61" width="0.1524" layer="94"/>
<wire x1="53.34" y1="-54.61" x2="53.34" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="53.34" y1="-41.91" x2="46.99" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="46.99" y1="-41.91" x2="46.99" y2="-54.61" width="0.1524" layer="94"/>
<text x="50.8" y="-52.07" size="1.778" layer="95" rot="R90">ZMASK</text>
<text x="63.5" y="-48.26" size="1.778" layer="95">Nzmask_en</text>
<wire x1="24.13" y1="92.71" x2="39.37" y2="92.71" width="0.1524" layer="94"/>
<wire x1="39.37" y1="92.71" x2="36.83" y2="97.79" width="0.1524" layer="94"/>
<wire x1="36.83" y1="97.79" x2="26.67" y2="97.79" width="0.1524" layer="94"/>
<wire x1="26.67" y1="97.79" x2="24.13" y2="92.71" width="0.1524" layer="94"/>
<text x="35.56" y="93.98" size="1.778" layer="95" rot="R90">0

1</text>
<text x="19.05" y="87.63" size="1.778" layer="95" rot="R90">trap_pc_sel</text>
<text x="49.53" y="39.37" size="1.778" layer="95">IN</text>
<text x="-46.99" y="40.64" size="1.778" layer="95">TRAP_VECTOR</text>
<wire x1="21.59" y1="11.43" x2="21.59" y2="6.35" width="0.1524" layer="94"/>
<wire x1="21.59" y1="6.35" x2="34.29" y2="6.35" width="0.1524" layer="94"/>
<wire x1="34.29" y1="6.35" x2="34.29" y2="11.43" width="0.1524" layer="94"/>
<wire x1="34.29" y1="11.43" x2="21.59" y2="11.43" width="0.1524" layer="94"/>
<text x="25.4" y="7.62" size="1.778" layer="95">ZEXT</text>
<wire x1="58.42" y1="17.78" x2="58.42" y2="2.54" width="0.1524" layer="94"/>
<wire x1="58.42" y1="2.54" x2="63.5" y2="5.08" width="0.1524" layer="94"/>
<wire x1="63.5" y1="5.08" x2="63.5" y2="15.24" width="0.1524" layer="94"/>
<wire x1="63.5" y1="15.24" x2="58.42" y2="17.78" width="0.1524" layer="94"/>
<text x="59.69" y="6.35" size="1.778" layer="95">0

1</text>
<text x="55.88" y="20.32" size="1.778" layer="95">imm_4_sel</text>
<text x="0" y="10.16" size="1.778" layer="95">imm4</text>
<wire x1="106.68" y1="125.73" x2="106.68" y2="110.49" width="0.1524" layer="94"/>
<wire x1="106.68" y1="110.49" x2="111.76" y2="113.03" width="0.1524" layer="94"/>
<wire x1="111.76" y1="113.03" x2="111.76" y2="123.19" width="0.1524" layer="94"/>
<wire x1="111.76" y1="123.19" x2="106.68" y2="125.73" width="0.1524" layer="94"/>
<text x="107.95" y="114.3" size="1.778" layer="95">0

1</text>
<wire x1="104.14" y1="111.76" x2="104.14" y2="116.84" width="0.1524" layer="94"/>
<wire x1="104.14" y1="116.84" x2="99.06" y2="116.84" width="0.1524" layer="94"/>
<wire x1="99.06" y1="116.84" x2="99.06" y2="111.76" width="0.1524" layer="94"/>
<wire x1="99.06" y1="111.76" x2="104.14" y2="111.76" width="0.1524" layer="94"/>
<text x="101.6" y="114.3" size="1.778" layer="95" rot="R180" align="center">&lt;&lt;8</text>
<text x="111.76" y="132.08" size="1.778" layer="97">shift_MDR_sel</text>
<polygon width="0.1524" layer="91">
<vertex x="21.59" y="8.89"/>
<vertex x="20.32" y="10.16"/>
<vertex x="20.32" y="7.62"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="21.59" y="16.51"/>
<vertex x="20.32" y="17.78"/>
<vertex x="20.32" y="15.24"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="21.59" y="24.13"/>
<vertex x="20.32" y="25.4"/>
<vertex x="20.32" y="22.86"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="58.42" y="7.62"/>
<vertex x="57.15" y="8.89"/>
<vertex x="57.15" y="6.35"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="58.42" y="12.7"/>
<vertex x="57.15" y="13.97"/>
<vertex x="57.15" y="11.43"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="73.66" y="17.78"/>
<vertex x="72.39" y="19.05"/>
<vertex x="72.39" y="16.51"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="73.66" y="22.86"/>
<vertex x="72.39" y="24.13"/>
<vertex x="72.39" y="21.59"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="83.82" y="38.1"/>
<vertex x="82.55" y="39.37"/>
<vertex x="82.55" y="36.83"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="83.82" y="43.18"/>
<vertex x="82.55" y="44.45"/>
<vertex x="82.55" y="41.91"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="73.66" y="40.64"/>
<vertex x="72.39" y="41.91"/>
<vertex x="72.39" y="39.37"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="73.66" y="45.72"/>
<vertex x="72.39" y="46.99"/>
<vertex x="72.39" y="44.45"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="33.02" y="39.37"/>
<vertex x="31.75" y="40.64"/>
<vertex x="31.75" y="38.1"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="33.02" y="41.91"/>
<vertex x="31.75" y="43.18"/>
<vertex x="31.75" y="40.64"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="53.34" y="34.29"/>
<vertex x="52.07" y="35.56"/>
<vertex x="52.07" y="33.02"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="95.25" y="43.18"/>
<vertex x="93.98" y="44.45"/>
<vertex x="93.98" y="41.91"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="96.52" y="58.42"/>
<vertex x="95.25" y="59.69"/>
<vertex x="95.25" y="57.15"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="91.44" y="120.65"/>
<vertex x="90.17" y="121.92"/>
<vertex x="90.17" y="119.38"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="91.44" y="115.57"/>
<vertex x="90.17" y="116.84"/>
<vertex x="90.17" y="114.3"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="99.06" y="114.3"/>
<vertex x="97.79" y="115.57"/>
<vertex x="97.79" y="113.03"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="106.68" y="120.65"/>
<vertex x="105.41" y="121.92"/>
<vertex x="105.41" y="119.38"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="106.68" y="115.57"/>
<vertex x="105.41" y="116.84"/>
<vertex x="105.41" y="114.3"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="102.87" y="138.43"/>
<vertex x="101.6" y="139.7"/>
<vertex x="101.6" y="137.16"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="102.87" y="142.24"/>
<vertex x="101.6" y="143.51"/>
<vertex x="101.6" y="140.97"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="88.9" y="144.78"/>
<vertex x="87.63" y="146.05"/>
<vertex x="87.63" y="143.51"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="88.9" y="149.86"/>
<vertex x="87.63" y="151.13"/>
<vertex x="87.63" y="148.59"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="83.82" y="129.54"/>
<vertex x="82.55" y="130.81"/>
<vertex x="82.55" y="128.27"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="83.82" y="135.89"/>
<vertex x="82.55" y="137.16"/>
<vertex x="82.55" y="134.62"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="44.45" y="121.92"/>
<vertex x="43.18" y="123.19"/>
<vertex x="43.18" y="120.65"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="34.29" y="119.38"/>
<vertex x="33.02" y="120.65"/>
<vertex x="33.02" y="118.11"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="34.29" y="124.46"/>
<vertex x="33.02" y="125.73"/>
<vertex x="33.02" y="123.19"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="13.97" y="119.38"/>
<vertex x="12.7" y="120.65"/>
<vertex x="12.7" y="118.11"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="13.97" y="124.46"/>
<vertex x="12.7" y="125.73"/>
<vertex x="12.7" y="123.19"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="3.81" y="121.92"/>
<vertex x="2.54" y="123.19"/>
<vertex x="2.54" y="120.65"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="3.81" y="116.84"/>
<vertex x="2.54" y="118.11"/>
<vertex x="2.54" y="115.57"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="-16.51" y="118.11"/>
<vertex x="-17.78" y="119.38"/>
<vertex x="-17.78" y="116.84"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="-16.51" y="124.46"/>
<vertex x="-17.78" y="125.73"/>
<vertex x="-17.78" y="123.19"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="-26.67" y="64.77"/>
<vertex x="-27.94" y="63.5"/>
<vertex x="-25.4" y="63.5"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="-40.64" y="64.77"/>
<vertex x="-41.91" y="63.5"/>
<vertex x="-39.37" y="63.5"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="113.03" y="140.97"/>
<vertex x="111.76" y="142.24"/>
<vertex x="111.76" y="139.7"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="116.84" y="118.11"/>
<vertex x="115.57" y="119.38"/>
<vertex x="115.57" y="116.84"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="49.53" y="-54.61"/>
<vertex x="48.26" y="-55.88"/>
<vertex x="50.8" y="-55.88"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="55.88" y="-34.29"/>
<vertex x="54.61" y="-35.56"/>
<vertex x="57.15" y="-35.56"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="49.53" y="-34.29"/>
<vertex x="48.26" y="-35.56"/>
<vertex x="50.8" y="-35.56"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="39.37" y="-24.13"/>
<vertex x="38.1" y="-25.4"/>
<vertex x="40.64" y="-25.4"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="34.29" y="-24.13"/>
<vertex x="33.02" y="-25.4"/>
<vertex x="35.56" y="-25.4"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="48.26" y="-12.7"/>
<vertex x="46.99" y="-13.97"/>
<vertex x="49.53" y="-13.97"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="53.34" y="-12.7"/>
<vertex x="52.07" y="-13.97"/>
<vertex x="54.61" y="-13.97"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="64.77" y="-5.08"/>
<vertex x="63.5" y="-3.81"/>
<vertex x="63.5" y="-6.35"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="85.09" y="-5.08"/>
<vertex x="83.82" y="-3.81"/>
<vertex x="83.82" y="-6.35"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="107.95" y="-5.08"/>
<vertex x="106.68" y="-3.81"/>
<vertex x="106.68" y="-6.35"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="107.95" y="0"/>
<vertex x="106.68" y="1.27"/>
<vertex x="106.68" y="-1.27"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="-8.89" y="26.67"/>
<vertex x="-10.16" y="25.4"/>
<vertex x="-7.62" y="25.4"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="43.18" y="46.99"/>
<vertex x="41.91" y="48.26"/>
<vertex x="41.91" y="45.72"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="43.18" y="40.64"/>
<vertex x="41.91" y="41.91"/>
<vertex x="41.91" y="39.37"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="43.18" y="58.42"/>
<vertex x="41.91" y="59.69"/>
<vertex x="41.91" y="57.15"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="24.13" y="55.88"/>
<vertex x="22.86" y="57.15"/>
<vertex x="22.86" y="54.61"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="24.13" y="60.96"/>
<vertex x="22.86" y="62.23"/>
<vertex x="22.86" y="59.69"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="144.78" y="140.97"/>
<vertex x="143.51" y="142.24"/>
<vertex x="143.51" y="139.7"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="170.18" y="118.11"/>
<vertex x="168.91" y="119.38"/>
<vertex x="168.91" y="116.84"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="-24.13" y="33.02"/>
<vertex x="-22.86" y="31.75"/>
<vertex x="-22.86" y="34.29"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="-20.32" y="36.83"/>
<vertex x="-19.05" y="35.56"/>
<vertex x="-19.05" y="38.1"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="-30.48" y="45.72"/>
<vertex x="-29.21" y="44.45"/>
<vertex x="-29.21" y="46.99"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="142.24" y="-2.54"/>
<vertex x="140.97" y="-1.27"/>
<vertex x="140.97" y="-3.81"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="24.13" y="144.78"/>
<vertex x="25.4" y="143.51"/>
<vertex x="25.4" y="146.05"/>
</polygon>
</plain>
<instances>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<wire x1="-8.89" y1="179.07" x2="-13.97" y2="179.07" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="-8.89" y1="173.99" x2="-13.97" y2="173.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="-3.81" y1="176.53" x2="1.27" y2="176.53" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="-6.35" y1="182.88" x2="-6.35" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="15.24" y1="180.34" x2="10.16" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="15.24" y1="175.26" x2="10.16" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="20.32" y1="177.8" x2="25.4" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="24.13" y1="60.96" x2="-1.27" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="24.13" y1="55.88" x2="19.05" y2="55.88" width="0.1524" layer="91"/>
<wire x1="0" y1="41.91" x2="19.05" y2="41.91" width="0.1524" layer="91"/>
<wire x1="19.05" y1="55.88" x2="19.05" y2="41.91" width="0.1524" layer="91"/>
<junction x="19.05" y="41.91"/>
<wire x1="107.95" y1="0" x2="38.1" y2="0" width="0.1524" layer="91"/>
<wire x1="19.05" y1="36.83" x2="19.05" y2="41.91" width="0.1524" layer="91"/>
<wire x1="19.05" y1="41.91" x2="33.02" y2="41.91" width="0.1524" layer="91"/>
<wire x1="38.1" y1="0" x2="38.1" y2="36.83" width="0.1524" layer="91"/>
<wire x1="38.1" y1="36.83" x2="19.05" y2="36.83" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="29.21" y1="58.42" x2="43.18" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="26.67" y1="64.77" x2="26.67" y2="69.85" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<wire x1="0" y1="46.99" x2="43.18" y2="46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="21.59" y1="24.13" x2="19.05" y2="24.13" width="0.1524" layer="91"/>
<wire x1="19.05" y1="24.13" x2="19.05" y2="16.51" width="0.1524" layer="91"/>
<wire x1="19.05" y1="16.51" x2="21.59" y2="16.51" width="0.1524" layer="91"/>
<wire x1="0" y1="30.48" x2="19.05" y2="30.48" width="0.1524" layer="91"/>
<wire x1="19.05" y1="30.48" x2="19.05" y2="24.13" width="0.1524" layer="91"/>
<junction x="19.05" y="24.13"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<wire x1="92.71" y1="43.18" x2="95.25" y2="43.18" width="0.1524" layer="91"/>
<wire x1="92.71" y1="40.64" x2="92.71" y2="43.18" width="0.1524" layer="91"/>
<wire x1="88.9" y1="40.64" x2="92.71" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<wire x1="102.87" y1="57.15" x2="102.87" y2="62.23" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<wire x1="43.18" y1="-26.67" x2="149.86" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="81.28" y1="149.86" x2="81.28" y2="120.65" width="0.1524" layer="91"/>
<wire x1="81.28" y1="120.65" x2="91.44" y2="120.65" width="0.1524" layer="91"/>
<wire x1="105.41" y1="52.07" x2="111.76" y2="52.07" width="0.1524" layer="91"/>
<wire x1="111.76" y1="52.07" x2="111.76" y2="67.31" width="0.1524" layer="91"/>
<wire x1="111.76" y1="67.31" x2="81.28" y2="67.31" width="0.1524" layer="91"/>
<wire x1="81.28" y1="67.31" x2="81.28" y2="120.65" width="0.1524" layer="91"/>
<junction x="81.28" y="120.65"/>
<wire x1="111.76" y1="52.07" x2="111.76" y2="36.83" width="0.1524" layer="91"/>
<junction x="111.76" y="52.07"/>
<wire x1="111.76" y1="36.83" x2="149.86" y2="36.83" width="0.1524" layer="91"/>
<wire x1="149.86" y1="36.83" x2="149.86" y2="-26.67" width="0.1524" layer="91"/>
<wire x1="88.9" y1="149.86" x2="81.28" y2="149.86" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-26.67" x2="43.18" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-30.48" x2="34.29" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="34.29" y1="-24.13" x2="34.29" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<wire x1="50.8" y1="-7.62" x2="50.8" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-5.08" x2="64.77" y2="-5.08" width="0.1524" layer="91"/>
<junction x="50.8" y="-5.08"/>
<wire x1="50.8" y1="-5.08" x2="50.8" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<wire x1="44.45" y1="-10.16" x2="39.37" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<wire x1="77.47" y1="-5.08" x2="85.09" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<wire x1="97.79" y1="-5.08" x2="107.95" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<wire x1="-40.64" y1="64.77" x2="-40.64" y2="52.07" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="52.07" x2="-16.51" y2="52.07" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<wire x1="3.81" y1="121.92" x2="-11.43" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<wire x1="-13.97" y1="128.27" x2="-13.97" y2="133.35" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<wire x1="46.99" y1="124.46" x2="46.99" y2="128.27" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<wire x1="13.97" y1="124.46" x2="10.16" y2="124.46" width="0.1524" layer="91"/>
<wire x1="10.16" y1="124.46" x2="10.16" y2="144.78" width="0.1524" layer="91"/>
<wire x1="10.16" y1="144.78" x2="19.05" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<wire x1="16.51" y1="128.27" x2="16.51" y2="133.35" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<wire x1="57.15" y1="175.26" x2="57.15" y2="179.07" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<wire x1="-16.51" y1="45.72" x2="-29.21" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<wire x1="-16.51" y1="124.46" x2="-40.64" y2="124.46" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="124.46" x2="-40.64" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<wire x1="-26.67" y1="64.77" x2="-26.67" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-26.67" y1="58.42" x2="-16.51" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<wire x1="105.41" y1="147.32" x2="105.41" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<wire x1="91.44" y1="115.57" x2="72.39" y2="115.57" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<wire x1="93.98" y1="124.46" x2="93.98" y2="125.73" width="0.1524" layer="91"/>
<wire x1="93.98" y1="125.73" x2="100.33" y2="125.73" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<wire x1="115.57" y1="143.51" x2="115.57" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<wire x1="119.38" y1="120.65" x2="119.38" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<wire x1="118.11" y1="140.97" x2="143.51" y2="140.97" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<wire x1="120.65" y1="-2.54" x2="140.97" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="73.66" y1="45.72" x2="58.42" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<wire x1="73.66" y1="40.64" x2="68.58" y2="40.64" width="0.1524" layer="91"/>
<wire x1="68.58" y1="40.64" x2="68.58" y2="34.29" width="0.1524" layer="91"/>
<wire x1="68.58" y1="34.29" x2="66.04" y2="34.29" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<wire x1="78.74" y1="43.18" x2="83.82" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<wire x1="76.2" y1="49.53" x2="76.2" y2="54.61" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<wire x1="52.07" y1="34.29" x2="0" y2="34.29" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<wire x1="-16.51" y1="118.11" x2="-26.67" y2="118.11" width="0.1524" layer="91"/>
<wire x1="-26.67" y1="118.11" x2="-26.67" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<wire x1="107.95" y1="140.97" x2="113.03" y2="140.97" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<wire x1="34.29" y1="124.46" x2="26.67" y2="124.46" width="0.1524" layer="91"/>
<wire x1="19.05" y1="121.92" x2="26.67" y2="121.92" width="0.1524" layer="91"/>
<wire x1="26.67" y1="121.92" x2="26.67" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<wire x1="29.21" y1="77.47" x2="66.04" y2="77.47" width="0.1524" layer="91"/>
<wire x1="58.42" y1="58.42" x2="66.04" y2="58.42" width="0.1524" layer="91"/>
<wire x1="66.04" y1="58.42" x2="96.52" y2="58.42" width="0.1524" layer="91"/>
<wire x1="66.04" y1="77.47" x2="66.04" y2="58.42" width="0.1524" layer="91"/>
<junction x="66.04" y="58.42"/>
<wire x1="29.21" y1="77.47" x2="29.21" y2="92.71" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<wire x1="39.37" y1="121.92" x2="44.45" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<wire x1="36.83" y1="128.27" x2="36.83" y2="133.35" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="86.36" y1="50.8" x2="86.36" y2="46.99" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<wire x1="50.8" y1="66.04" x2="50.8" y2="69.85" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<wire x1="60.96" y1="135.89" x2="60.96" y2="121.92" width="0.1524" layer="91"/>
<wire x1="3.81" y1="116.84" x2="-1.27" y2="116.84" width="0.1524" layer="91"/>
<wire x1="-1.27" y1="116.84" x2="-1.27" y2="109.22" width="0.1524" layer="91"/>
<wire x1="-1.27" y1="109.22" x2="13.97" y2="109.22" width="0.1524" layer="91"/>
<wire x1="13.97" y1="109.22" x2="53.34" y2="109.22" width="0.1524" layer="91"/>
<wire x1="53.34" y1="109.22" x2="53.34" y2="121.92" width="0.1524" layer="91"/>
<wire x1="53.34" y1="121.92" x2="49.53" y2="121.92" width="0.1524" layer="91"/>
<wire x1="53.34" y1="121.92" x2="53.34" y2="144.78" width="0.1524" layer="91"/>
<junction x="53.34" y="121.92"/>
<wire x1="53.34" y1="144.78" x2="24.13" y2="144.78" width="0.1524" layer="91"/>
<wire x1="60.96" y1="121.92" x2="53.34" y2="121.92" width="0.1524" layer="91"/>
<wire x1="83.82" y1="135.89" x2="60.96" y2="135.89" width="0.1524" layer="91"/>
<wire x1="39.37" y1="-24.13" x2="39.37" y2="-29.21" width="0.1524" layer="91"/>
<wire x1="13.97" y1="109.22" x2="13.97" y2="-29.21" width="0.1524" layer="91"/>
<wire x1="13.97" y1="-29.21" x2="39.37" y2="-29.21" width="0.1524" layer="91"/>
<junction x="13.97" y="109.22"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<wire x1="38.1" y1="40.64" x2="43.18" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<wire x1="35.56" y1="43.18" x2="35.56" y2="44.45" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<wire x1="33.02" y1="39.37" x2="27.94" y2="39.37" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<wire x1="-16.51" y1="39.37" x2="-57.15" y2="39.37" width="0.1524" layer="91"/>
<wire x1="-57.15" y1="39.37" x2="-57.15" y2="152.4" width="0.1524" layer="91"/>
<wire x1="-57.15" y1="152.4" x2="69.85" y2="152.4" width="0.1524" layer="91"/>
<wire x1="69.85" y1="152.4" x2="69.85" y2="144.78" width="0.1524" layer="91"/>
<wire x1="69.85" y1="144.78" x2="88.9" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<wire x1="93.98" y1="147.32" x2="99.06" y2="147.32" width="0.1524" layer="91"/>
<wire x1="99.06" y1="147.32" x2="99.06" y2="142.24" width="0.1524" layer="91"/>
<wire x1="99.06" y1="142.24" x2="102.87" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<wire x1="91.44" y1="153.67" x2="91.44" y2="158.75" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<wire x1="88.9" y1="133.35" x2="93.98" y2="133.35" width="0.1524" layer="91"/>
<wire x1="102.87" y1="138.43" x2="93.98" y2="138.43" width="0.1524" layer="91"/>
<wire x1="93.98" y1="138.43" x2="93.98" y2="133.35" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<wire x1="86.36" y1="139.7" x2="86.36" y2="142.24" width="0.1524" layer="91"/>
<wire x1="86.36" y1="142.24" x2="72.39" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<wire x1="55.88" y1="-34.29" x2="55.88" y2="-39.37" width="0.1524" layer="91"/>
<wire x1="13.97" y1="119.38" x2="10.16" y2="119.38" width="0.1524" layer="91"/>
<wire x1="10.16" y1="119.38" x2="8.89" y2="119.38" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-39.37" x2="10.16" y2="-39.37" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-39.37" x2="10.16" y2="119.38" width="0.1524" layer="91"/>
<junction x="10.16" y="119.38"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<wire x1="53.34" y1="-29.21" x2="53.34" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<wire x1="46.99" y1="-31.75" x2="41.91" y2="-31.75" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<wire x1="36.83" y1="-19.05" x2="36.83" y2="-13.97" width="0.1524" layer="91"/>
<wire x1="36.83" y1="-13.97" x2="48.26" y2="-13.97" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-13.97" x2="48.26" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<wire x1="30.48" y1="-21.59" x2="25.4" y2="-21.59" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<wire x1="-16.51" y1="36.83" x2="-20.32" y2="36.83" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<wire x1="-16.51" y1="33.02" x2="-24.13" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="83.82" y1="38.1" x2="81.28" y2="38.1" width="0.1524" layer="91"/>
<wire x1="81.28" y1="38.1" x2="81.28" y2="20.32" width="0.1524" layer="91"/>
<wire x1="81.28" y1="20.32" x2="78.74" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="73.66" y1="22.86" x2="34.29" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<wire x1="76.2" y1="26.67" x2="76.2" y2="31.75" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<wire x1="53.34" y1="-48.26" x2="62.23" y2="-48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<wire x1="49.53" y1="-34.29" x2="49.53" y2="-41.91" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<wire x1="34.29" y1="92.71" x2="34.29" y2="87.63" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="26.67" x2="-8.89" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="-60.96" x2="49.53" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="49.53" y1="-60.96" x2="154.94" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="49.53" y1="-54.61" x2="49.53" y2="-60.96" width="0.1524" layer="91"/>
<junction x="49.53" y="-60.96"/>
<wire x1="83.82" y1="129.54" x2="82.55" y2="129.54" width="0.1524" layer="91"/>
<wire x1="82.55" y1="129.54" x2="82.55" y2="124.46" width="0.1524" layer="91"/>
<wire x1="82.55" y1="124.46" x2="90.17" y2="124.46" width="0.1524" layer="91"/>
<wire x1="90.17" y1="124.46" x2="90.17" y2="128.27" width="0.1524" layer="91"/>
<wire x1="142.24" y1="128.27" x2="90.17" y2="128.27" width="0.1524" layer="91"/>
<wire x1="121.92" y1="118.11" x2="142.24" y2="118.11" width="0.1524" layer="91"/>
<wire x1="142.24" y1="118.11" x2="142.24" y2="128.27" width="0.1524" layer="91"/>
<junction x="142.24" y="118.11"/>
<wire x1="142.24" y1="118.11" x2="154.94" y2="118.11" width="0.1524" layer="91"/>
<wire x1="154.94" y1="118.11" x2="168.91" y2="118.11" width="0.1524" layer="91"/>
<wire x1="154.94" y1="-60.96" x2="154.94" y2="87.63" width="0.1524" layer="91"/>
<junction x="154.94" y="118.11"/>
<wire x1="154.94" y1="87.63" x2="154.94" y2="118.11" width="0.1524" layer="91"/>
<wire x1="34.29" y1="87.63" x2="154.94" y2="87.63" width="0.1524" layer="91"/>
<junction x="154.94" y="87.63"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<wire x1="34.29" y1="119.38" x2="31.75" y2="119.38" width="0.1524" layer="91"/>
<wire x1="31.75" y1="97.79" x2="31.75" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<wire x1="25.4" y1="95.25" x2="20.32" y2="95.25" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<wire x1="-2.54" y1="26.67" x2="-2.54" y2="8.89" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="8.89" x2="21.59" y2="8.89" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<wire x1="19.05" y1="8.89" x2="21.59" y2="8.89" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<wire x1="58.42" y1="12.7" x2="53.34" y2="12.7" width="0.1524" layer="91"/>
<wire x1="34.29" y1="16.51" x2="53.34" y2="16.51" width="0.1524" layer="91"/>
<wire x1="53.34" y1="16.51" x2="53.34" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<wire x1="58.42" y1="7.62" x2="53.34" y2="7.62" width="0.1524" layer="91"/>
<wire x1="34.29" y1="8.89" x2="53.34" y2="8.89" width="0.1524" layer="91"/>
<wire x1="53.34" y1="8.89" x2="53.34" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<wire x1="63.5" y1="10.16" x2="68.58" y2="10.16" width="0.1524" layer="91"/>
<wire x1="68.58" y1="10.16" x2="68.58" y2="17.78" width="0.1524" layer="91"/>
<wire x1="73.66" y1="17.78" x2="68.58" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<wire x1="60.96" y1="16.51" x2="60.96" y2="19.05" width="0.1524" layer="91"/>
<wire x1="60.96" y1="19.05" x2="63.5" y2="19.05" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<wire x1="106.68" y1="120.65" x2="101.6" y2="120.65" width="0.1524" layer="91"/>
<wire x1="96.52" y1="118.11" x2="97.79" y2="118.11" width="0.1524" layer="91"/>
<wire x1="97.79" y1="118.11" x2="101.6" y2="118.11" width="0.1524" layer="91"/>
<wire x1="101.6" y1="118.11" x2="101.6" y2="120.65" width="0.1524" layer="91"/>
<wire x1="99.06" y1="114.3" x2="97.79" y2="114.3" width="0.1524" layer="91"/>
<wire x1="97.79" y1="114.3" x2="97.79" y2="118.11" width="0.1524" layer="91"/>
<junction x="97.79" y="118.11"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<wire x1="111.76" y1="118.11" x2="116.84" y2="118.11" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<wire x1="109.22" y1="124.46" x2="109.22" y2="130.81" width="0.1524" layer="91"/>
<wire x1="109.22" y1="130.81" x2="114.3" y2="130.81" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<wire x1="104.14" y1="114.3" x2="105.41" y2="114.3" width="0.1524" layer="91"/>
<wire x1="105.41" y1="114.3" x2="105.41" y2="115.57" width="0.1524" layer="91"/>
<wire x1="105.41" y1="115.57" x2="106.68" y2="115.57" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
