<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.1.3">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Mechanical" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Gehäuse" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Mittellin" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="Accent_neu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="NXP" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="NXP_2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="KASTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="KASTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="FRNTTEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="BACKMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="BIFRNTTEK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="BIFRNTMAT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="BottomExtra" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="OrgLBR" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="BMS" urn="urn:adsk.eagle:library:6828203">
<packages>
<package name="BATTERY-HOLDER-QUAD" urn="urn:adsk.eagle:footprint:6828288/1" library_version="1">
<wire x1="0" y1="0" x2="0" y2="20" width="0.127" layer="21"/>
<wire x1="0" y1="20" x2="0" y2="39" width="0.127" layer="21"/>
<wire x1="0" y1="39" x2="0" y2="59" width="0.127" layer="21"/>
<wire x1="0" y1="59" x2="0" y2="78.74" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="77.98" y2="0" width="0.127" layer="21"/>
<wire x1="77.98" y1="78.84" x2="77.98" y2="0.1" width="0.127" layer="21"/>
<wire x1="77.98" y1="78.84" x2="0" y2="78.84" width="0.127" layer="21"/>
<wire x1="0" y1="20" x2="78" y2="20" width="0.127" layer="21"/>
<wire x1="0" y1="39" x2="78" y2="39" width="0.127" layer="21"/>
<wire x1="0" y1="59" x2="78" y2="59" width="0.127" layer="21"/>
<pad name="P$9" x="2.5" y="10.35" drill="1.8" shape="long"/>
<pad name="P$10" x="2.5" y="29.65" drill="1.8" shape="long"/>
<pad name="P$11" x="2.5" y="48.95" drill="1.8" shape="long"/>
<pad name="P$12" x="2.5" y="68.25" drill="1.8" shape="long"/>
<pad name="P$13" x="75.4" y="10.35" drill="1.9" shape="long"/>
<pad name="P$14" x="75.4" y="29.65" drill="1.8" shape="long"/>
<pad name="P$15" x="75.4" y="48.95" drill="1.8" shape="long"/>
<pad name="P$16" x="75.4" y="68.25" drill="1.8" shape="long"/>
<hole x="11.22" y="10.31" drill="3.2"/>
<hole x="66.83" y="10.31" drill="3.2"/>
<hole x="11.22" y="29.61" drill="3.2"/>
<hole x="66.83" y="29.61" drill="3.2"/>
<hole x="11.22" y="48.91" drill="3.2"/>
<hole x="66.83" y="48.91" drill="3.2"/>
<hole x="11.22" y="68.21" drill="3.2"/>
<hole x="66.83" y="68.21" drill="3.2"/>
<text x="-0.3" y="-6.2" size="3.81" layer="21">+</text>
<text x="76.2" y="-7.4" size="3.81" layer="21">-</text>
</package>
</packages>
<packages3d>
<package3d name="BATTERY-HOLDER-QUAD" urn="urn:adsk.eagle:package:6828330/1" type="box" library_version="1">
<packageinstances>
<packageinstance name="BATTERY-HOLDER-QUAD"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="QUADBATTERY" urn="urn:adsk.eagle:symbol:6828249/1" library_version="1">
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="10.16" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-17.78" x2="-2.54" y2="-22.86" width="0.254" layer="94"/>
<wire x1="0" y1="-15.24" x2="0" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-30.48" x2="-2.54" y2="-35.56" width="0.254" layer="94"/>
<wire x1="0" y1="-27.94" x2="0" y2="-38.1" width="0.254" layer="94"/>
<pin name="P$1" x="-7.62" y="5.08" length="middle"/>
<pin name="P$2" x="5.08" y="5.08" length="middle" rot="R180"/>
<pin name="P$3" x="-7.62" y="-7.62" length="middle"/>
<pin name="P$4" x="5.08" y="-7.62" length="middle" rot="R180"/>
<pin name="P$5" x="-7.62" y="-20.32" length="middle"/>
<pin name="P$6" x="5.08" y="-20.32" length="middle" rot="R180"/>
<pin name="P$7" x="-7.62" y="-33.02" length="middle"/>
<pin name="P$8" x="5.08" y="-33.02" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BATTERYQUAD" urn="urn:adsk.eagle:component:6828430/1" library_version="1">
<gates>
<gate name="G$1" symbol="QUADBATTERY" x="0" y="33.02"/>
</gates>
<devices>
<device name="" package="BATTERY-HOLDER-QUAD">
<connects>
<connect gate="G$1" pin="P$1" pad="P$13"/>
<connect gate="G$1" pin="P$2" pad="P$9"/>
<connect gate="G$1" pin="P$3" pad="P$14"/>
<connect gate="G$1" pin="P$4" pad="P$10"/>
<connect gate="G$1" pin="P$5" pad="P$15"/>
<connect gate="G$1" pin="P$6" pad="P$11"/>
<connect gate="G$1" pin="P$7" pad="P$16"/>
<connect gate="G$1" pin="P$8" pad="P$12"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6828330/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BMS">
<packages>
<package name="2410">
<smd name="P$1" x="-1.27" y="0" dx="3.175" dy="2.54" layer="1" rot="R90"/>
<smd name="P$2" x="4.318" y="0" dx="3.175" dy="2.54" layer="1" rot="R90"/>
<wire x1="0" y1="-1.524" x2="3.048" y2="-1.524" width="0.127" layer="21"/>
<wire x1="0" y1="1.524" x2="3.048" y2="1.524" width="0.127" layer="21"/>
</package>
<package name="XT60PW-M">
<pad name="P$1" x="-3.6" y="0" drill="2.7" shape="square"/>
<pad name="P$2" x="3.6" y="0" drill="2.7" shape="square"/>
<pad name="P$3" x="-6.75" y="6" drill="1.7" shape="square"/>
<pad name="P$4" x="6.75" y="6" drill="1.7" shape="square"/>
<wire x1="-7.75" y1="-1.85" x2="7.75" y2="-1.85" width="0.127" layer="21"/>
<wire x1="-7.75" y1="16.35" x2="-7.75" y2="-1.85" width="0.127" layer="21"/>
<wire x1="7.75" y1="16.35" x2="7.75" y2="-1.85" width="0.127" layer="21"/>
<wire x1="-7.75" y1="16.35" x2="7.75" y2="16.35" width="0.127" layer="21"/>
</package>
<package name="PHOENIX-2PIN">
<pad name="P$1" x="0" y="0" drill="1.6" shape="long" rot="R90"/>
<pad name="P$2" x="5" y="0" drill="1.6" shape="long" rot="R90"/>
<wire x1="-2.5" y1="3" x2="-2.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-3" x2="7.5" y2="-3" width="0.127" layer="21"/>
<wire x1="7.5" y1="-3" x2="7.5" y2="3" width="0.127" layer="21"/>
<wire x1="7.5" y1="3" x2="-2.5" y2="3" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="FUSE">
<wire x1="-5.08" y1="0" x2="-3.556" y2="1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="-2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.889" y1="-1.4986" x2="2.4892" y2="0" width="0.254" layer="94"/>
<wire x1="-3.5992" y1="1.4912" x2="-3.048" y2="1.7272" width="0.254" layer="94" curve="-46.337037" cap="flat"/>
<wire x1="-3.048" y1="1.7272" x2="-2.496" y2="1.491" width="0.254" layer="94" curve="-46.403624" cap="flat"/>
<wire x1="0.4572" y1="-1.778" x2="0.8965" y2="-1.4765" width="0.254" layer="94" curve="63.169357" cap="flat"/>
<wire x1="-0.0178" y1="-1.508" x2="0.4572" y2="-1.7778" width="0.254" layer="94" curve="64.986119" cap="flat"/>
<text x="-5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="0" visible="off" length="short" direction="pas" function="dot" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" function="dot" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="MEGA-FIT-2">
<pin name="P$1" x="0" y="-7.62" length="middle" rot="R90"/>
<pin name="P$2" x="5.08" y="-7.62" length="middle" rot="R90"/>
<polygon width="0.254" layer="94">
<vertex x="1.27" y="-3.81"/>
<vertex x="1.27" y="-1.27"/>
<vertex x="-1.27" y="-1.27"/>
<vertex x="-1.27" y="-3.81"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="6.35" y="-1.27"/>
<vertex x="3.81" y="-1.27"/>
<vertex x="3.81" y="-3.81"/>
<vertex x="6.35" y="-3.81"/>
</polygon>
<text x="7.62" y="-2.54" size="1.778" layer="97">+</text>
</symbol>
<symbol name="PINHD2">
<wire x1="1.27" y1="0" x2="8.89" y2="0" width="0.4064" layer="94"/>
<wire x1="8.89" y1="0" x2="8.89" y2="7.62" width="0.4064" layer="94"/>
<wire x1="8.89" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="7.62" x2="1.27" y2="0" width="0.4064" layer="94"/>
<text x="1.27" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="1.27" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FUSE2410" uservalue="yes">
<gates>
<gate name="G$1" symbol="FUSE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2410">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XT60PW-M">
<gates>
<gate name="G$1" symbol="MEGA-FIT-2" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="XT60PW-M">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PHOENIX-2PIN">
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PHOENIX-2PIN">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$10" library="BMS" library_urn="urn:adsk.eagle:library:6828203" deviceset="BATTERYQUAD" device="" package3d_urn="urn:adsk.eagle:package:6828330/1"/>
<part name="U$3" library="BMS" deviceset="FUSE2410" device=""/>
<part name="U$1" library="BMS" deviceset="FUSE2410" device=""/>
<part name="U$2" library="BMS" deviceset="FUSE2410" device=""/>
<part name="U$4" library="BMS" deviceset="FUSE2410" device=""/>
<part name="U$5" library="BMS" library_urn="urn:adsk.eagle:library:6828203" deviceset="BATTERYQUAD" device="" package3d_urn="urn:adsk.eagle:package:6828330/1"/>
<part name="U$6" library="BMS" deviceset="FUSE2410" device=""/>
<part name="U$7" library="BMS" deviceset="FUSE2410" device=""/>
<part name="U$8" library="BMS" deviceset="FUSE2410" device=""/>
<part name="U$9" library="BMS" deviceset="FUSE2410" device=""/>
<part name="U$11" library="BMS" library_urn="urn:adsk.eagle:library:6828203" deviceset="BATTERYQUAD" device="" package3d_urn="urn:adsk.eagle:package:6828330/1"/>
<part name="U$12" library="BMS" deviceset="FUSE2410" device=""/>
<part name="U$13" library="BMS" deviceset="FUSE2410" device=""/>
<part name="U$14" library="BMS" deviceset="FUSE2410" device=""/>
<part name="U$15" library="BMS" deviceset="FUSE2410" device=""/>
<part name="U$16" library="BMS" library_urn="urn:adsk.eagle:library:6828203" deviceset="BATTERYQUAD" device="" package3d_urn="urn:adsk.eagle:package:6828330/1"/>
<part name="U$17" library="BMS" deviceset="FUSE2410" device=""/>
<part name="U$18" library="BMS" deviceset="FUSE2410" device=""/>
<part name="U$19" library="BMS" deviceset="FUSE2410" device=""/>
<part name="U$20" library="BMS" deviceset="FUSE2410" device=""/>
<part name="U$21" library="BMS" deviceset="XT60PW-M" device=""/>
<part name="U$22" library="BMS" deviceset="XT60PW-M" device=""/>
<part name="U$23" library="BMS" deviceset="PHOENIX-2PIN" device=""/>
<part name="U$24" library="BMS" deviceset="PHOENIX-2PIN" device=""/>
<part name="U$25" library="BMS" deviceset="PHOENIX-2PIN" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$10" gate="G$1" x="60.96" y="53.34"/>
<instance part="U$3" gate="G$1" x="71.12" y="58.42" rot="R180"/>
<instance part="U$1" gate="G$1" x="71.12" y="45.72" rot="R180"/>
<instance part="U$2" gate="G$1" x="71.12" y="33.02" rot="R180"/>
<instance part="U$4" gate="G$1" x="71.12" y="20.32" rot="R180"/>
<instance part="U$5" gate="G$1" x="33.02" y="53.34"/>
<instance part="U$6" gate="G$1" x="43.18" y="58.42" rot="R180"/>
<instance part="U$7" gate="G$1" x="43.18" y="45.72" rot="R180"/>
<instance part="U$8" gate="G$1" x="43.18" y="33.02" rot="R180"/>
<instance part="U$9" gate="G$1" x="43.18" y="20.32" rot="R180"/>
<instance part="U$11" gate="G$1" x="5.08" y="53.34"/>
<instance part="U$12" gate="G$1" x="15.24" y="58.42" rot="R180"/>
<instance part="U$13" gate="G$1" x="15.24" y="45.72" rot="R180"/>
<instance part="U$14" gate="G$1" x="15.24" y="33.02" rot="R180"/>
<instance part="U$15" gate="G$1" x="15.24" y="20.32" rot="R180"/>
<instance part="U$16" gate="G$1" x="-22.86" y="53.34"/>
<instance part="U$17" gate="G$1" x="-12.7" y="58.42" rot="R180"/>
<instance part="U$18" gate="G$1" x="-12.7" y="45.72" rot="R180"/>
<instance part="U$19" gate="G$1" x="-12.7" y="33.02" rot="R180"/>
<instance part="U$20" gate="G$1" x="-12.7" y="20.32" rot="R180"/>
<instance part="U$21" gate="G$1" x="-40.64" y="58.42" rot="R90"/>
<instance part="U$22" gate="G$1" x="88.9" y="63.5" rot="R270"/>
<instance part="U$23" gate="G$1" x="-7.62" y="10.16" rot="R270"/>
<instance part="U$24" gate="G$1" x="-33.02" y="10.16" rot="R270"/>
<instance part="U$25" gate="G$1" x="55.88" y="10.16" rot="R270"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$8" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="2"/>
<pinref part="U$10" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="2"/>
<pinref part="U$10" gate="G$1" pin="P$4"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="2"/>
<pinref part="U$10" gate="G$1" pin="P$6"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="2"/>
<pinref part="U$10" gate="G$1" pin="P$8"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U$6" gate="G$1" pin="2"/>
<pinref part="U$5" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="2"/>
<pinref part="U$5" gate="G$1" pin="P$4"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="2"/>
<pinref part="U$5" gate="G$1" pin="P$6"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U$9" gate="G$1" pin="2"/>
<pinref part="U$5" gate="G$1" pin="P$8"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U$12" gate="G$1" pin="2"/>
<pinref part="U$11" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U$13" gate="G$1" pin="2"/>
<pinref part="U$11" gate="G$1" pin="P$4"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U$14" gate="G$1" pin="2"/>
<pinref part="U$11" gate="G$1" pin="P$6"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="U$15" gate="G$1" pin="2"/>
<pinref part="U$11" gate="G$1" pin="P$8"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="2"/>
<pinref part="U$16" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="U$18" gate="G$1" pin="2"/>
<pinref part="U$16" gate="G$1" pin="P$4"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="U$19" gate="G$1" pin="2"/>
<pinref part="U$16" gate="G$1" pin="P$6"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="U$20" gate="G$1" pin="2"/>
<pinref part="U$16" gate="G$1" pin="P$8"/>
</segment>
</net>
<net name="BATTERY1+" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="1"/>
<pinref part="U$11" gate="G$1" pin="P$1"/>
<wire x1="-5.08" y1="58.42" x2="-2.54" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U$20" gate="G$1" pin="1"/>
<pinref part="U$11" gate="G$1" pin="P$7"/>
<wire x1="-5.08" y1="20.32" x2="-2.54" y2="20.32" width="0.1524" layer="91"/>
<pinref part="U$19" gate="G$1" pin="1"/>
<pinref part="U$11" gate="G$1" pin="P$5"/>
<wire x1="-5.08" y1="33.02" x2="-2.54" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="20.32" x2="-2.54" y2="33.02" width="0.1524" layer="91"/>
<junction x="-2.54" y="20.32"/>
<junction x="-2.54" y="33.02"/>
<pinref part="U$18" gate="G$1" pin="1"/>
<pinref part="U$11" gate="G$1" pin="P$3"/>
<wire x1="-5.08" y1="45.72" x2="-2.54" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="33.02" x2="-2.54" y2="45.72" width="0.1524" layer="91"/>
<junction x="-2.54" y="45.72"/>
<wire x1="-2.54" y1="58.42" x2="-2.54" y2="45.72" width="0.1524" layer="91"/>
<junction x="-2.54" y="58.42"/>
<pinref part="U$23" gate="G$1" pin="2"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="20.32" width="0.1524" layer="91"/>
<junction x="-5.08" y="20.32"/>
</segment>
</net>
<net name="BATTERY2+" class="0">
<segment>
<pinref part="U$15" gate="G$1" pin="1"/>
<pinref part="U$5" gate="G$1" pin="P$7"/>
<wire x1="22.86" y1="20.32" x2="25.4" y2="20.32" width="0.1524" layer="91"/>
<pinref part="U$23" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="10.16" x2="25.4" y2="10.16" width="0.1524" layer="91"/>
<wire x1="25.4" y1="10.16" x2="25.4" y2="20.32" width="0.1524" layer="91"/>
<junction x="25.4" y="20.32"/>
<pinref part="U$14" gate="G$1" pin="1"/>
<pinref part="U$5" gate="G$1" pin="P$5"/>
<wire x1="22.86" y1="33.02" x2="25.4" y2="33.02" width="0.1524" layer="91"/>
<wire x1="25.4" y1="20.32" x2="25.4" y2="33.02" width="0.1524" layer="91"/>
<junction x="25.4" y="33.02"/>
<pinref part="U$13" gate="G$1" pin="1"/>
<pinref part="U$5" gate="G$1" pin="P$3"/>
<wire x1="22.86" y1="45.72" x2="25.4" y2="45.72" width="0.1524" layer="91"/>
<wire x1="25.4" y1="33.02" x2="25.4" y2="45.72" width="0.1524" layer="91"/>
<junction x="25.4" y="45.72"/>
<pinref part="U$12" gate="G$1" pin="1"/>
<pinref part="U$5" gate="G$1" pin="P$1"/>
<wire x1="22.86" y1="58.42" x2="25.4" y2="58.42" width="0.1524" layer="91"/>
<wire x1="25.4" y1="45.72" x2="25.4" y2="58.42" width="0.1524" layer="91"/>
<junction x="25.4" y="58.42"/>
</segment>
</net>
<net name="BATTERY3+" class="0">
<segment>
<pinref part="U$9" gate="G$1" pin="1"/>
<pinref part="U$10" gate="G$1" pin="P$7"/>
<wire x1="50.8" y1="20.32" x2="53.34" y2="20.32" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="1"/>
<pinref part="U$10" gate="G$1" pin="P$5"/>
<wire x1="50.8" y1="33.02" x2="53.34" y2="33.02" width="0.1524" layer="91"/>
<wire x1="53.34" y1="20.32" x2="53.34" y2="33.02" width="0.1524" layer="91"/>
<junction x="53.34" y="20.32"/>
<junction x="53.34" y="33.02"/>
<pinref part="U$7" gate="G$1" pin="1"/>
<pinref part="U$10" gate="G$1" pin="P$3"/>
<wire x1="50.8" y1="45.72" x2="53.34" y2="45.72" width="0.1524" layer="91"/>
<wire x1="53.34" y1="33.02" x2="53.34" y2="45.72" width="0.1524" layer="91"/>
<junction x="53.34" y="45.72"/>
<pinref part="U$10" gate="G$1" pin="P$1"/>
<pinref part="U$6" gate="G$1" pin="1"/>
<wire x1="53.34" y1="58.42" x2="50.8" y2="58.42" width="0.1524" layer="91"/>
<wire x1="53.34" y1="45.72" x2="53.34" y2="58.42" width="0.1524" layer="91"/>
<junction x="53.34" y="58.42"/>
<pinref part="U$25" gate="G$1" pin="2"/>
<wire x1="58.42" y1="5.08" x2="58.42" y2="12.7" width="0.1524" layer="91"/>
<wire x1="58.42" y1="12.7" x2="53.34" y2="12.7" width="0.1524" layer="91"/>
<wire x1="53.34" y1="12.7" x2="53.34" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BATTERY4+" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="1"/>
<pinref part="U$1" gate="G$1" pin="1"/>
<wire x1="78.74" y1="58.42" x2="78.74" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="1"/>
<wire x1="78.74" y1="45.72" x2="78.74" y2="33.02" width="0.1524" layer="91"/>
<junction x="78.74" y="45.72"/>
<pinref part="U$22" gate="G$1" pin="P$1"/>
<pinref part="U$22" gate="G$1" pin="P$2"/>
<wire x1="81.28" y1="63.5" x2="81.28" y2="58.42" width="0.1524" layer="91"/>
<wire x1="78.74" y1="58.42" x2="81.28" y2="58.42" width="0.1524" layer="91"/>
<junction x="78.74" y="58.42"/>
<junction x="81.28" y="58.42"/>
<pinref part="U$4" gate="G$1" pin="1"/>
<wire x1="78.74" y1="20.32" x2="78.74" y2="33.02" width="0.1524" layer="91"/>
<junction x="78.74" y="33.02"/>
<pinref part="U$25" gate="G$1" pin="1"/>
<wire x1="60.96" y1="5.08" x2="60.96" y2="10.16" width="0.1524" layer="91"/>
<wire x1="60.96" y1="10.16" x2="78.74" y2="10.16" width="0.1524" layer="91"/>
<wire x1="78.74" y1="10.16" x2="78.74" y2="20.32" width="0.1524" layer="91"/>
<junction x="78.74" y="20.32"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$24" gate="G$1" pin="1"/>
<pinref part="U$24" gate="G$1" pin="2"/>
<wire x1="-27.94" y1="5.08" x2="-30.48" y2="5.08" width="0.1524" layer="91"/>
<pinref part="U$16" gate="G$1" pin="P$1"/>
<pinref part="U$16" gate="G$1" pin="P$3"/>
<wire x1="-30.48" y1="58.42" x2="-30.48" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U$16" gate="G$1" pin="P$5"/>
<wire x1="-30.48" y1="45.72" x2="-30.48" y2="33.02" width="0.1524" layer="91"/>
<junction x="-30.48" y="45.72"/>
<pinref part="U$16" gate="G$1" pin="P$7"/>
<wire x1="-30.48" y1="33.02" x2="-30.48" y2="20.32" width="0.1524" layer="91"/>
<junction x="-30.48" y="33.02"/>
<pinref part="U$21" gate="G$1" pin="P$1"/>
<wire x1="-33.02" y1="58.42" x2="-30.48" y2="58.42" width="0.1524" layer="91"/>
<junction x="-30.48" y="58.42"/>
<pinref part="U$21" gate="G$1" pin="P$2"/>
<wire x1="-33.02" y1="63.5" x2="-33.02" y2="58.42" width="0.1524" layer="91"/>
<junction x="-33.02" y="58.42"/>
<wire x1="-30.48" y1="5.08" x2="-30.48" y2="20.32" width="0.1524" layer="91"/>
<junction x="-30.48" y="5.08"/>
<junction x="-30.48" y="20.32"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
