<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.5.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="BMS">
<packages>
<package name="BATTERY-HOLDER-QUAD">
<wire x1="0" y1="0" x2="0" y2="20" width="0.127" layer="21"/>
<wire x1="0" y1="20" x2="0" y2="39" width="0.127" layer="21"/>
<wire x1="0" y1="39" x2="0" y2="59" width="0.127" layer="21"/>
<wire x1="0" y1="59" x2="0" y2="78.74" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="77.98" y2="0" width="0.127" layer="21"/>
<wire x1="77.98" y1="78.84" x2="77.98" y2="0.1" width="0.127" layer="21"/>
<wire x1="77.98" y1="78.84" x2="0" y2="78.84" width="0.127" layer="21"/>
<wire x1="0" y1="20" x2="78" y2="20" width="0.127" layer="21"/>
<wire x1="0" y1="39" x2="78" y2="39" width="0.127" layer="21"/>
<wire x1="0" y1="59" x2="78" y2="59" width="0.127" layer="21"/>
<pad name="P$9" x="2.5" y="10.35" drill="1.8" shape="long"/>
<pad name="P$10" x="2.5" y="29.65" drill="1.8" shape="long"/>
<pad name="P$11" x="2.5" y="48.95" drill="1.8" shape="long"/>
<pad name="P$12" x="2.5" y="68.25" drill="1.8" shape="long"/>
<pad name="P$13" x="75.4" y="10.35" drill="1.9" shape="long"/>
<pad name="P$14" x="75.4" y="29.65" drill="1.8" shape="long"/>
<pad name="P$15" x="75.4" y="48.95" drill="1.8" shape="long"/>
<pad name="P$16" x="75.4" y="68.25" drill="1.8" shape="long"/>
<hole x="11.22" y="10.31" drill="3.2"/>
<hole x="66.83" y="10.31" drill="3.2"/>
<hole x="11.22" y="29.61" drill="3.2"/>
<hole x="66.83" y="29.61" drill="3.2"/>
<hole x="11.22" y="48.91" drill="3.2"/>
<hole x="66.83" y="48.91" drill="3.2"/>
<hole x="11.22" y="68.21" drill="3.2"/>
<hole x="66.83" y="68.21" drill="3.2"/>
<text x="-0.3" y="-6.2" size="3.81" layer="21">+</text>
<text x="76.2" y="-7.4" size="3.81" layer="21">-</text>
</package>
<package name="PHOENIX-2PIN">
<pad name="P$1" x="0" y="0" drill="1.6" shape="long" rot="R90"/>
<pad name="P$2" x="5" y="0" drill="1.6" shape="long" rot="R90"/>
<wire x1="-2.5" y1="3" x2="-2.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-3" x2="7.5" y2="-3" width="0.127" layer="21"/>
<wire x1="7.5" y1="-3" x2="7.5" y2="3" width="0.127" layer="21"/>
<wire x1="7.5" y1="3" x2="-2.5" y2="3" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="BATTERY">
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<pin name="P$1" x="-5.08" y="0" length="middle"/>
<pin name="P$2" x="7.62" y="0" length="middle" rot="R180"/>
</symbol>
<symbol name="PINHD2">
<wire x1="1.27" y1="0" x2="8.89" y2="0" width="0.4064" layer="94"/>
<wire x1="8.89" y1="0" x2="8.89" y2="7.62" width="0.4064" layer="94"/>
<wire x1="8.89" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="7.62" x2="1.27" y2="0" width="0.4064" layer="94"/>
<text x="1.27" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="1.27" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BATTERY_HOLDER_QUAD">
<gates>
<gate name="G$1" symbol="BATTERY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BATTERY-HOLDER-QUAD">
<connects>
<connect gate="G$1" pin="P$1" pad="P$13 P$14 P$15 P$16"/>
<connect gate="G$1" pin="P$2" pad="P$9 P$10 P$11 P$12"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PHOENIX-2PIN">
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PHOENIX-2PIN">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="BMS" deviceset="BATTERY_HOLDER_QUAD" device=""/>
<part name="U$2" library="BMS" deviceset="BATTERY_HOLDER_QUAD" device=""/>
<part name="U$3" library="BMS" deviceset="BATTERY_HOLDER_QUAD" device=""/>
<part name="U$4" library="BMS" deviceset="BATTERY_HOLDER_QUAD" device=""/>
<part name="U$5" library="BMS" deviceset="PHOENIX-2PIN" device=""/>
<part name="U$6" library="BMS" deviceset="PHOENIX-2PIN" device=""/>
<part name="U$7" library="BMS" deviceset="PHOENIX-2PIN" device=""/>
<part name="U$8" library="BMS" deviceset="PHOENIX-2PIN" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="20.32" y="-10.16" size="1.778" layer="97">battery balancers</text>
<text x="-20.32" y="17.78" size="1.778" layer="97">low voltage</text>
<text x="71.12" y="17.78" size="1.778" layer="97">high voltage</text>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="5.08" y="7.62"/>
<instance part="U$2" gate="G$1" x="20.32" y="7.62"/>
<instance part="U$3" gate="G$1" x="35.56" y="7.62"/>
<instance part="U$4" gate="G$1" x="50.8" y="7.62"/>
<instance part="U$5" gate="G$1" x="7.62" y="-5.08" rot="R270"/>
<instance part="U$6" gate="G$1" x="43.18" y="-5.08" rot="R270"/>
<instance part="U$7" gate="G$1" x="-7.62" y="12.7" rot="R180"/>
<instance part="U$8" gate="G$1" x="71.12" y="5.08"/>
</instances>
<busses>
</busses>
<nets>
<net name="BATTERY1+" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$2"/>
<pinref part="U$2" gate="G$1" pin="P$1"/>
<wire x1="15.24" y1="7.62" x2="12.7" y2="7.62" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="1"/>
<wire x1="12.7" y1="-10.16" x2="12.7" y2="5.08" width="0.1524" layer="91"/>
<wire x1="12.7" y1="5.08" x2="15.24" y2="5.08" width="0.1524" layer="91"/>
<wire x1="15.24" y1="5.08" x2="15.24" y2="7.62" width="0.1524" layer="91"/>
<junction x="15.24" y="7.62"/>
</segment>
</net>
<net name="BATTERY2+" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="P$1"/>
<pinref part="U$2" gate="G$1" pin="P$2"/>
<wire x1="27.94" y1="7.62" x2="30.48" y2="7.62" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="2"/>
<wire x1="45.72" y1="-10.16" x2="45.72" y2="0" width="0.1524" layer="91"/>
<wire x1="45.72" y1="0" x2="30.48" y2="0" width="0.1524" layer="91"/>
<wire x1="30.48" y1="0" x2="30.48" y2="7.62" width="0.1524" layer="91"/>
<junction x="30.48" y="7.62"/>
</segment>
</net>
<net name="BATTERY3+" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="P$1"/>
<pinref part="U$3" gate="G$1" pin="P$2"/>
<wire x1="43.18" y1="7.62" x2="45.72" y2="7.62" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="1"/>
<wire x1="48.26" y1="-10.16" x2="48.26" y2="5.08" width="0.1524" layer="91"/>
<wire x1="48.26" y1="5.08" x2="45.72" y2="5.08" width="0.1524" layer="91"/>
<wire x1="45.72" y1="5.08" x2="45.72" y2="7.62" width="0.1524" layer="91"/>
<junction x="45.72" y="7.62"/>
</segment>
</net>
<net name="BATTERY1-" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="2"/>
<pinref part="U$7" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="10.16" x2="-12.7" y2="7.62" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="P$1"/>
<wire x1="-12.7" y1="7.62" x2="0" y2="7.62" width="0.1524" layer="91"/>
<junction x="-12.7" y="7.62"/>
<pinref part="U$5" gate="G$1" pin="2"/>
<wire x1="10.16" y1="-10.16" x2="0" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="0" y1="-10.16" x2="0" y2="7.62" width="0.1524" layer="91"/>
<junction x="0" y="7.62"/>
</segment>
</net>
<net name="BATTERY4+" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="1"/>
<pinref part="U$8" gate="G$1" pin="2"/>
<wire x1="76.2" y1="10.16" x2="76.2" y2="7.62" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="P$2"/>
<wire x1="76.2" y1="7.62" x2="58.42" y2="7.62" width="0.1524" layer="91"/>
<junction x="76.2" y="7.62"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
