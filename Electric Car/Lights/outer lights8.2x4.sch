<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.5.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="LED BOARD">
<packages>
<package name="SK6812">
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<smd name="P$1" x="-2.6" y="1.6" dx="1.75" dy="1.2" layer="1"/>
<smd name="P$2" x="-2.6" y="-1.6" dx="1.75" dy="1.2" layer="1"/>
<smd name="P$3" x="2.6" y="1.6" dx="1.75" dy="1.2" layer="1"/>
<smd name="P$4" x="2.6" y="-1.6" dx="1.75" dy="1.2" layer="1"/>
<circle x="1.2" y="-2.2" radius="0.2" width="0.127" layer="21"/>
<text x="3.81" y="-2.54" size="1.27" layer="25">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="SK6812">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="4DOUT" x="-12.7" y="-5.08" length="middle"/>
<pin name="3VDD" x="-12.7" y="5.08" length="middle"/>
<pin name="2DIN" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="1VSS" x="12.7" y="-5.08" length="middle" rot="R180"/>
<text x="-3.81" y="0" size="1.778" layer="94">SK6812</text>
<text x="15.24" y="-7.62" size="1.778" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SK6812">
<gates>
<gate name="G$1" symbol="SK6812" x="50.8" y="-12.7"/>
</gates>
<devices>
<device name="" package="SK6812">
<connects>
<connect gate="G$1" pin="1VSS" pad="P$4"/>
<connect gate="G$1" pin="2DIN" pad="P$3"/>
<connect gate="G$1" pin="3VDD" pad="P$1"/>
<connect gate="G$1" pin="4DOUT" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead" urn="urn:adsk.eagle:library:325">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X02" urn="urn:adsk.eagle:footprint:22309/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="1X02/90" urn="urn:adsk.eagle:footprint:22310/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-3.175" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
</package>
<package name="1X01" urn="urn:adsk.eagle:footprint:22382/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="1X02" urn="urn:adsk.eagle:package:22435/2" type="model" library_version="3">
<description>PIN HEADER</description>
</package3d>
<package3d name="1X02/90" urn="urn:adsk.eagle:package:22437/2" type="model" library_version="3">
<description>PIN HEADER</description>
</package3d>
<package3d name="1X01" urn="urn:adsk.eagle:package:22485/2" type="model" library_version="3">
<description>PIN HEADER</description>
</package3d>
</packages3d>
<symbols>
<symbol name="PINHD2" urn="urn:adsk.eagle:symbol:22308/1" library_version="3">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD1" urn="urn:adsk.eagle:symbol:22381/1" library_version="3">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X2" urn="urn:adsk.eagle:component:22516/3" prefix="JP" uservalue="yes" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22435/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X02/90">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22437/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X1" urn="urn:adsk.eagle:component:22540/2" prefix="JP" uservalue="yes" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22485/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$2" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$3" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$4" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$5" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$6" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$7" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$8" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$9" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$10" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$11" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$12" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$13" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$14" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$15" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$16" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$17" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$18" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$19" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$20" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$27" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY1" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY2" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY3" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY4" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY5" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY6" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY7" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY8" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY9" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY10" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY11" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY12" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY13" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY14" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY15" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY16" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY17" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY18" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY19" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY20" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+21" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY21" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="JP1" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X2" device="" package3d_urn="urn:adsk.eagle:package:22435/2"/>
<part name="P+27" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY27" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="JP2" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X1" device="" package3d_urn="urn:adsk.eagle:package:22485/2"/>
<part name="U$21" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$22" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$23" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$24" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$25" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="P+22" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY22" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+23" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY23" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+24" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY24" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+25" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY25" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+26" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY26" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U$26" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$28" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$29" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$30" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$31" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$32" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$33" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$34" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="P+28" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY28" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+29" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY29" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+30" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY30" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+31" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY31" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+32" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY32" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+33" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY33" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+34" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY34" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+35" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY35" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U$35" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$36" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$37" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$38" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$39" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="P+36" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY36" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+37" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY37" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+38" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY38" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+39" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY39" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+40" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY40" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U$40" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$41" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="P+41" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+42" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY41" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U$42" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$43" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$44" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$45" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="U$46" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="P+43" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY42" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+44" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY43" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+45" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY44" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+46" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY45" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+47" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY46" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U$47" library="LED BOARD" deviceset="SK6812" device=""/>
<part name="P+48" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY47" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="15.24" y="50.8"/>
<instance part="U$2" gate="G$1" x="53.34" y="50.8"/>
<instance part="U$3" gate="G$1" x="93.98" y="50.8"/>
<instance part="U$4" gate="G$1" x="132.08" y="50.8"/>
<instance part="U$5" gate="G$1" x="-22.86" y="50.8"/>
<instance part="U$6" gate="G$1" x="-60.96" y="50.8"/>
<instance part="U$7" gate="G$1" x="-99.06" y="50.8"/>
<instance part="U$8" gate="G$1" x="-137.16" y="50.8"/>
<instance part="U$9" gate="G$1" x="-175.26" y="50.8"/>
<instance part="U$10" gate="G$1" x="-213.36" y="50.8"/>
<instance part="U$11" gate="G$1" x="-248.92" y="50.8"/>
<instance part="U$12" gate="G$1" x="-287.02" y="50.8"/>
<instance part="U$13" gate="G$1" x="-325.12" y="50.8"/>
<instance part="U$14" gate="G$1" x="-325.12" y="27.94" rot="R180"/>
<instance part="U$15" gate="G$1" x="-287.02" y="27.94" rot="R180"/>
<instance part="U$16" gate="G$1" x="-248.92" y="27.94" rot="R180"/>
<instance part="U$17" gate="G$1" x="-210.82" y="27.94" rot="R180"/>
<instance part="U$18" gate="G$1" x="-175.26" y="27.94" rot="R180"/>
<instance part="U$19" gate="G$1" x="-137.16" y="27.94" rot="R180"/>
<instance part="U$20" gate="G$1" x="-99.06" y="27.94" rot="R180"/>
<instance part="U$27" gate="G$1" x="-58.42" y="27.94" rot="R180"/>
<instance part="P+1" gate="VCC" x="-337.82" y="58.42"/>
<instance part="SUPPLY1" gate="GND" x="-312.42" y="43.18"/>
<instance part="P+2" gate="VCC" x="-299.72" y="58.42"/>
<instance part="SUPPLY2" gate="GND" x="-274.32" y="43.18"/>
<instance part="P+3" gate="VCC" x="-261.62" y="58.42"/>
<instance part="SUPPLY3" gate="GND" x="-236.22" y="43.18"/>
<instance part="P+4" gate="VCC" x="-226.06" y="58.42"/>
<instance part="SUPPLY4" gate="GND" x="-200.66" y="43.18"/>
<instance part="P+5" gate="VCC" x="-187.96" y="58.42"/>
<instance part="SUPPLY5" gate="GND" x="-162.56" y="43.18"/>
<instance part="P+6" gate="VCC" x="-149.86" y="58.42"/>
<instance part="SUPPLY6" gate="GND" x="-124.46" y="43.18"/>
<instance part="P+7" gate="VCC" x="-111.76" y="58.42"/>
<instance part="SUPPLY7" gate="GND" x="-86.36" y="43.18"/>
<instance part="P+8" gate="VCC" x="-73.66" y="58.42"/>
<instance part="SUPPLY8" gate="GND" x="-48.26" y="43.18"/>
<instance part="P+9" gate="VCC" x="-35.56" y="58.42"/>
<instance part="SUPPLY9" gate="GND" x="-10.16" y="43.18"/>
<instance part="P+10" gate="VCC" x="2.54" y="58.42"/>
<instance part="SUPPLY10" gate="GND" x="27.94" y="43.18"/>
<instance part="P+11" gate="VCC" x="40.64" y="58.42"/>
<instance part="SUPPLY11" gate="GND" x="66.04" y="43.18"/>
<instance part="P+12" gate="VCC" x="81.28" y="58.42"/>
<instance part="SUPPLY12" gate="GND" x="106.68" y="43.18"/>
<instance part="P+13" gate="VCC" x="119.38" y="58.42"/>
<instance part="SUPPLY13" gate="GND" x="144.78" y="43.18"/>
<instance part="P+14" gate="VCC" x="-312.42" y="20.32" rot="R180"/>
<instance part="SUPPLY14" gate="GND" x="-337.82" y="35.56" rot="R180"/>
<instance part="P+15" gate="VCC" x="-274.32" y="20.32" rot="R180"/>
<instance part="SUPPLY15" gate="GND" x="-299.72" y="35.56" rot="R180"/>
<instance part="P+16" gate="VCC" x="-236.22" y="20.32" rot="R180"/>
<instance part="SUPPLY16" gate="GND" x="-261.62" y="35.56" rot="R180"/>
<instance part="P+17" gate="VCC" x="-198.12" y="20.32" rot="R180"/>
<instance part="SUPPLY17" gate="GND" x="-223.52" y="35.56" rot="R180"/>
<instance part="P+18" gate="VCC" x="-162.56" y="20.32" rot="R180"/>
<instance part="SUPPLY18" gate="GND" x="-187.96" y="35.56" rot="R180"/>
<instance part="P+19" gate="VCC" x="-124.46" y="20.32" rot="R180"/>
<instance part="SUPPLY19" gate="GND" x="-149.86" y="35.56" rot="R180"/>
<instance part="P+20" gate="VCC" x="-86.36" y="20.32" rot="R180"/>
<instance part="SUPPLY20" gate="GND" x="-111.76" y="35.56" rot="R180"/>
<instance part="P+21" gate="VCC" x="-45.72" y="20.32" rot="R180"/>
<instance part="SUPPLY21" gate="GND" x="-73.66" y="35.56" rot="R180"/>
<instance part="JP1" gate="G$1" x="-88.9" y="-58.42"/>
<instance part="P+27" gate="VCC" x="-101.6" y="-55.88" rot="R90"/>
<instance part="SUPPLY27" gate="GND" x="-101.6" y="-58.42" rot="R270"/>
<instance part="JP2" gate="G$1" x="154.94" y="55.88"/>
<instance part="U$21" gate="G$1" x="-25.4" y="27.94" rot="R180"/>
<instance part="U$22" gate="G$1" x="12.7" y="27.94" rot="R180"/>
<instance part="U$23" gate="G$1" x="48.26" y="27.94" rot="R180"/>
<instance part="U$24" gate="G$1" x="86.36" y="27.94" rot="R180"/>
<instance part="U$25" gate="G$1" x="124.46" y="27.94" rot="R180"/>
<instance part="P+22" gate="VCC" x="-12.7" y="20.32" rot="R180"/>
<instance part="SUPPLY22" gate="GND" x="-38.1" y="35.56" rot="R180"/>
<instance part="P+23" gate="VCC" x="25.4" y="20.32" rot="R180"/>
<instance part="SUPPLY23" gate="GND" x="0" y="35.56" rot="R180"/>
<instance part="P+24" gate="VCC" x="60.96" y="20.32" rot="R180"/>
<instance part="SUPPLY24" gate="GND" x="35.56" y="35.56" rot="R180"/>
<instance part="P+25" gate="VCC" x="99.06" y="20.32" rot="R180"/>
<instance part="SUPPLY25" gate="GND" x="73.66" y="35.56" rot="R180"/>
<instance part="P+26" gate="VCC" x="137.16" y="20.32" rot="R180"/>
<instance part="SUPPLY26" gate="GND" x="111.76" y="35.56" rot="R180"/>
<instance part="U$26" gate="G$1" x="129.54" y="5.08"/>
<instance part="U$28" gate="G$1" x="91.44" y="5.08"/>
<instance part="U$29" gate="G$1" x="53.34" y="5.08"/>
<instance part="U$30" gate="G$1" x="15.24" y="5.08"/>
<instance part="U$31" gate="G$1" x="-20.32" y="5.08"/>
<instance part="U$32" gate="G$1" x="-58.42" y="5.08"/>
<instance part="U$33" gate="G$1" x="-96.52" y="5.08"/>
<instance part="U$34" gate="G$1" x="-137.16" y="5.08"/>
<instance part="P+28" gate="VCC" x="116.84" y="12.7"/>
<instance part="SUPPLY28" gate="GND" x="142.24" y="-2.54"/>
<instance part="P+29" gate="VCC" x="78.74" y="12.7"/>
<instance part="SUPPLY29" gate="GND" x="104.14" y="-2.54"/>
<instance part="P+30" gate="VCC" x="40.64" y="12.7"/>
<instance part="SUPPLY30" gate="GND" x="66.04" y="-2.54"/>
<instance part="P+31" gate="VCC" x="2.54" y="12.7"/>
<instance part="SUPPLY31" gate="GND" x="27.94" y="-2.54"/>
<instance part="P+32" gate="VCC" x="-33.02" y="12.7"/>
<instance part="SUPPLY32" gate="GND" x="-7.62" y="-2.54"/>
<instance part="P+33" gate="VCC" x="-71.12" y="12.7"/>
<instance part="SUPPLY33" gate="GND" x="-45.72" y="-2.54"/>
<instance part="P+34" gate="VCC" x="-109.22" y="12.7"/>
<instance part="SUPPLY34" gate="GND" x="-83.82" y="-2.54"/>
<instance part="P+35" gate="VCC" x="-149.86" y="12.7"/>
<instance part="SUPPLY35" gate="GND" x="-121.92" y="-2.54"/>
<instance part="U$35" gate="G$1" x="-170.18" y="5.08"/>
<instance part="U$36" gate="G$1" x="-208.28" y="5.08"/>
<instance part="U$37" gate="G$1" x="-243.84" y="5.08"/>
<instance part="U$38" gate="G$1" x="-281.94" y="5.08"/>
<instance part="U$39" gate="G$1" x="-320.04" y="5.08"/>
<instance part="P+36" gate="VCC" x="-182.88" y="12.7"/>
<instance part="SUPPLY36" gate="GND" x="-157.48" y="-2.54"/>
<instance part="P+37" gate="VCC" x="-220.98" y="12.7"/>
<instance part="SUPPLY37" gate="GND" x="-195.58" y="-2.54"/>
<instance part="P+38" gate="VCC" x="-256.54" y="12.7"/>
<instance part="SUPPLY38" gate="GND" x="-231.14" y="-2.54"/>
<instance part="P+39" gate="VCC" x="-294.64" y="12.7"/>
<instance part="SUPPLY39" gate="GND" x="-269.24" y="-2.54"/>
<instance part="P+40" gate="VCC" x="-332.74" y="12.7"/>
<instance part="SUPPLY40" gate="GND" x="-307.34" y="-2.54"/>
<instance part="U$40" gate="G$1" x="-320.04" y="-17.78" rot="R180"/>
<instance part="U$41" gate="G$1" x="-279.4" y="-17.78" rot="R180"/>
<instance part="P+41" gate="VCC" x="-307.34" y="-25.4" rot="R180"/>
<instance part="P+42" gate="VCC" x="-266.7" y="-25.4" rot="R180"/>
<instance part="SUPPLY41" gate="GND" x="-294.64" y="-10.16" rot="R180"/>
<instance part="U$42" gate="G$1" x="-246.38" y="-17.78" rot="R180"/>
<instance part="U$43" gate="G$1" x="-208.28" y="-17.78" rot="R180"/>
<instance part="U$44" gate="G$1" x="-172.72" y="-17.78" rot="R180"/>
<instance part="U$45" gate="G$1" x="-134.62" y="-17.78" rot="R180"/>
<instance part="U$46" gate="G$1" x="-96.52" y="-17.78" rot="R180"/>
<instance part="P+43" gate="VCC" x="-233.68" y="-25.4" rot="R180"/>
<instance part="SUPPLY42" gate="GND" x="-259.08" y="-10.16" rot="R180"/>
<instance part="P+44" gate="VCC" x="-195.58" y="-25.4" rot="R180"/>
<instance part="SUPPLY43" gate="GND" x="-220.98" y="-10.16" rot="R180"/>
<instance part="P+45" gate="VCC" x="-160.02" y="-25.4" rot="R180"/>
<instance part="SUPPLY44" gate="GND" x="-185.42" y="-10.16" rot="R180"/>
<instance part="P+46" gate="VCC" x="-121.92" y="-25.4" rot="R180"/>
<instance part="SUPPLY45" gate="GND" x="-147.32" y="-10.16" rot="R180"/>
<instance part="P+47" gate="VCC" x="-83.82" y="-25.4" rot="R180"/>
<instance part="SUPPLY46" gate="GND" x="-109.22" y="-10.16" rot="R180"/>
<instance part="U$47" gate="G$1" x="-58.42" y="-17.78" rot="R180"/>
<instance part="P+48" gate="VCC" x="-45.72" y="-25.4" rot="R180"/>
<instance part="SUPPLY47" gate="GND" x="-71.12" y="-10.16" rot="R180"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="2DIN"/>
<pinref part="U$2" gate="G$1" pin="4DOUT"/>
<wire x1="27.94" y1="55.88" x2="35.56" y2="45.72" width="0.1524" layer="91"/>
<wire x1="35.56" y1="45.72" x2="40.64" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="2DIN"/>
<pinref part="U$3" gate="G$1" pin="4DOUT"/>
<wire x1="66.04" y1="55.88" x2="73.66" y2="45.72" width="0.1524" layer="91"/>
<wire x1="73.66" y1="45.72" x2="81.28" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="2DIN"/>
<pinref part="U$4" gate="G$1" pin="4DOUT"/>
<wire x1="106.68" y1="55.88" x2="114.3" y2="45.72" width="0.1524" layer="91"/>
<wire x1="114.3" y1="45.72" x2="119.38" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="2DIN"/>
<pinref part="U$1" gate="G$1" pin="4DOUT"/>
<wire x1="-10.16" y1="55.88" x2="-7.62" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="55.88" x2="2.54" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U$6" gate="G$1" pin="2DIN"/>
<pinref part="U$5" gate="G$1" pin="4DOUT"/>
<wire x1="-48.26" y1="55.88" x2="-45.72" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="55.88" x2="-35.56" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="2DIN"/>
<pinref part="U$6" gate="G$1" pin="4DOUT"/>
<wire x1="-86.36" y1="55.88" x2="-76.2" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="45.72" x2="-73.66" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="2DIN"/>
<pinref part="U$7" gate="G$1" pin="4DOUT"/>
<wire x1="-124.46" y1="55.88" x2="-114.3" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="45.72" x2="-111.76" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U$9" gate="G$1" pin="2DIN"/>
<pinref part="U$8" gate="G$1" pin="4DOUT"/>
<wire x1="-162.56" y1="55.88" x2="-152.4" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="45.72" x2="-149.86" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U$10" gate="G$1" pin="2DIN"/>
<pinref part="U$9" gate="G$1" pin="4DOUT"/>
<wire x1="-200.66" y1="55.88" x2="-190.5" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="45.72" x2="-187.96" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="2DIN"/>
<pinref part="U$10" gate="G$1" pin="4DOUT"/>
<wire x1="-236.22" y1="55.88" x2="-226.06" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U$12" gate="G$1" pin="2DIN"/>
<pinref part="U$11" gate="G$1" pin="4DOUT"/>
<wire x1="-274.32" y1="55.88" x2="-264.16" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-264.16" y1="45.72" x2="-261.62" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U$13" gate="G$1" pin="2DIN"/>
<pinref part="U$12" gate="G$1" pin="4DOUT"/>
<wire x1="-312.42" y1="55.88" x2="-302.26" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-302.26" y1="45.72" x2="-299.72" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U$15" gate="G$1" pin="2DIN"/>
<pinref part="U$14" gate="G$1" pin="4DOUT"/>
<wire x1="-299.72" y1="22.86" x2="-309.88" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-309.88" y1="33.02" x2="-312.42" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="2DIN"/>
<pinref part="U$15" gate="G$1" pin="4DOUT"/>
<wire x1="-261.62" y1="22.86" x2="-271.78" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-271.78" y1="33.02" x2="-274.32" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="2DIN"/>
<pinref part="U$16" gate="G$1" pin="4DOUT"/>
<wire x1="-223.52" y1="22.86" x2="-233.68" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="33.02" x2="-236.22" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U$18" gate="G$1" pin="2DIN"/>
<pinref part="U$17" gate="G$1" pin="4DOUT"/>
<wire x1="-187.96" y1="22.86" x2="-198.12" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U$19" gate="G$1" pin="2DIN"/>
<pinref part="U$18" gate="G$1" pin="4DOUT"/>
<wire x1="-149.86" y1="22.86" x2="-160.02" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-160.02" y1="33.02" x2="-162.56" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U$20" gate="G$1" pin="2DIN"/>
<pinref part="U$19" gate="G$1" pin="4DOUT"/>
<wire x1="-111.76" y1="22.86" x2="-121.92" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="33.02" x2="-124.46" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="U$20" gate="G$1" pin="4DOUT"/>
<wire x1="-86.36" y1="33.02" x2="-76.2" y2="22.86" width="0.1524" layer="91"/>
<pinref part="U$27" gate="G$1" pin="2DIN"/>
<wire x1="-76.2" y1="22.86" x2="-71.12" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="U$14" gate="G$1" pin="2DIN"/>
<wire x1="-337.82" y1="22.86" x2="-347.98" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-347.98" y1="33.02" x2="-347.98" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$13" gate="G$1" pin="4DOUT"/>
<wire x1="-347.98" y1="40.64" x2="-342.9" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-342.9" y1="40.64" x2="-337.82" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="U$13" gate="G$1" pin="3VDD"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$12" gate="G$1" pin="3VDD"/>
<pinref part="P+2" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$10" gate="G$1" pin="3VDD"/>
<pinref part="P+4" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$9" gate="G$1" pin="3VDD"/>
<pinref part="P+5" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="3VDD"/>
<pinref part="P+6" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="3VDD"/>
<pinref part="P+7" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="3VDD"/>
<pinref part="P+3" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="3VDD"/>
<pinref part="P+8" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="3VDD"/>
<pinref part="P+9" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="3VDD"/>
<pinref part="P+10" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="3VDD"/>
<pinref part="P+11" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="3VDD"/>
<pinref part="P+12" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="3VDD"/>
<pinref part="P+13" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$14" gate="G$1" pin="3VDD"/>
<pinref part="P+14" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$15" gate="G$1" pin="3VDD"/>
<pinref part="P+15" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$16" gate="G$1" pin="3VDD"/>
<pinref part="P+16" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$17" gate="G$1" pin="3VDD"/>
<pinref part="P+17" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="3VDD"/>
<pinref part="P+18" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$19" gate="G$1" pin="3VDD"/>
<pinref part="P+19" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$20" gate="G$1" pin="3VDD"/>
<pinref part="P+20" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="1"/>
<pinref part="P+27" gate="VCC" pin="VCC"/>
<wire x1="-99.06" y1="-55.88" x2="-91.44" y2="-55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+21" gate="VCC" pin="VCC"/>
<pinref part="U$27" gate="G$1" pin="3VDD"/>
</segment>
<segment>
<pinref part="U$21" gate="G$1" pin="3VDD"/>
<pinref part="P+22" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$22" gate="G$1" pin="3VDD"/>
<pinref part="P+23" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$23" gate="G$1" pin="3VDD"/>
<pinref part="P+24" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$24" gate="G$1" pin="3VDD"/>
<pinref part="P+25" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$25" gate="G$1" pin="3VDD"/>
<pinref part="P+26" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$26" gate="G$1" pin="3VDD"/>
<pinref part="P+28" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$28" gate="G$1" pin="3VDD"/>
<pinref part="P+29" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$29" gate="G$1" pin="3VDD"/>
<pinref part="P+30" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$30" gate="G$1" pin="3VDD"/>
<pinref part="P+31" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$31" gate="G$1" pin="3VDD"/>
<pinref part="P+32" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$32" gate="G$1" pin="3VDD"/>
<pinref part="P+33" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$33" gate="G$1" pin="3VDD"/>
<pinref part="P+34" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="P+35" gate="VCC" pin="VCC"/>
<pinref part="U$34" gate="G$1" pin="3VDD"/>
</segment>
<segment>
<pinref part="U$35" gate="G$1" pin="3VDD"/>
<pinref part="P+36" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$36" gate="G$1" pin="3VDD"/>
<pinref part="P+37" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$37" gate="G$1" pin="3VDD"/>
<pinref part="P+38" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$38" gate="G$1" pin="3VDD"/>
<pinref part="P+39" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$39" gate="G$1" pin="3VDD"/>
<pinref part="P+40" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$40" gate="G$1" pin="3VDD"/>
<pinref part="P+41" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="P+42" gate="VCC" pin="VCC"/>
<pinref part="U$41" gate="G$1" pin="3VDD"/>
</segment>
<segment>
<pinref part="U$42" gate="G$1" pin="3VDD"/>
<pinref part="P+43" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$43" gate="G$1" pin="3VDD"/>
<pinref part="P+44" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$44" gate="G$1" pin="3VDD"/>
<pinref part="P+45" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$45" gate="G$1" pin="3VDD"/>
<pinref part="P+46" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$46" gate="G$1" pin="3VDD"/>
<pinref part="P+47" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="U$47" gate="G$1" pin="3VDD"/>
<pinref part="P+48" gate="VCC" pin="VCC"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$13" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$12" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$10" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$9" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$14" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$15" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$16" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$17" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$19" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$20" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="2"/>
<pinref part="SUPPLY27" gate="GND" pin="GND"/>
<wire x1="-99.06" y1="-58.42" x2="-91.44" y2="-58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
<pinref part="U$27" gate="G$1" pin="1VSS"/>
<wire x1="-71.12" y1="33.02" x2="-73.66" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$21" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$22" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$23" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY24" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$24" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$25" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$26" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$28" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY29" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$29" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY30" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$30" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY31" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$31" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY32" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$32" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY33" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$33" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY34" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY35" gate="GND" pin="GND"/>
<pinref part="U$34" gate="G$1" pin="1VSS"/>
<wire x1="-124.46" y1="0" x2="-121.92" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$35" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY36" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$36" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY37" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$37" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY38" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$38" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY39" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$39" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY40" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY41" gate="GND" pin="GND"/>
<pinref part="U$41" gate="G$1" pin="1VSS"/>
<wire x1="-292.1" y1="-12.7" x2="-294.64" y2="-12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$42" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY42" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$43" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY43" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$44" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY44" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$45" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY45" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$46" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY46" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$47" gate="G$1" pin="1VSS"/>
<pinref part="SUPPLY47" gate="GND" pin="GND"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="2DIN"/>
<pinref part="JP2" gate="G$1" pin="1"/>
<wire x1="144.78" y1="55.88" x2="152.4" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="U$21" gate="G$1" pin="2DIN"/>
<junction x="-38.1" y="22.86"/>
<pinref part="U$27" gate="G$1" pin="4DOUT"/>
<wire x1="-45.72" y1="33.02" x2="-38.1" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="25.4" x2="-38.1" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="U$22" gate="G$1" pin="2DIN"/>
<pinref part="U$21" gate="G$1" pin="4DOUT"/>
<wire x1="0" y1="22.86" x2="-10.16" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="33.02" x2="-12.7" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="U$23" gate="G$1" pin="2DIN"/>
<pinref part="U$22" gate="G$1" pin="4DOUT"/>
<wire x1="35.56" y1="22.86" x2="25.4" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="U$24" gate="G$1" pin="2DIN"/>
<pinref part="U$23" gate="G$1" pin="4DOUT"/>
<wire x1="73.66" y1="22.86" x2="63.5" y2="33.02" width="0.1524" layer="91"/>
<wire x1="63.5" y1="33.02" x2="60.96" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="U$25" gate="G$1" pin="2DIN"/>
<pinref part="U$24" gate="G$1" pin="4DOUT"/>
<wire x1="111.76" y1="22.86" x2="101.6" y2="33.02" width="0.1524" layer="91"/>
<wire x1="101.6" y1="33.02" x2="99.06" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="U$28" gate="G$1" pin="2DIN"/>
<pinref part="U$26" gate="G$1" pin="4DOUT"/>
<wire x1="104.14" y1="10.16" x2="114.3" y2="0" width="0.1524" layer="91"/>
<wire x1="114.3" y1="0" x2="116.84" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="U$29" gate="G$1" pin="2DIN"/>
<pinref part="U$28" gate="G$1" pin="4DOUT"/>
<wire x1="66.04" y1="10.16" x2="76.2" y2="0" width="0.1524" layer="91"/>
<wire x1="76.2" y1="0" x2="78.74" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="U$30" gate="G$1" pin="2DIN"/>
<pinref part="U$29" gate="G$1" pin="4DOUT"/>
<wire x1="27.94" y1="10.16" x2="38.1" y2="0" width="0.1524" layer="91"/>
<wire x1="38.1" y1="0" x2="40.64" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="U$31" gate="G$1" pin="2DIN"/>
<pinref part="U$30" gate="G$1" pin="4DOUT"/>
<wire x1="-7.62" y1="10.16" x2="2.54" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="U$32" gate="G$1" pin="2DIN"/>
<pinref part="U$31" gate="G$1" pin="4DOUT"/>
<wire x1="-45.72" y1="10.16" x2="-35.56" y2="0" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="0" x2="-33.02" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="U$33" gate="G$1" pin="2DIN"/>
<pinref part="U$32" gate="G$1" pin="4DOUT"/>
<wire x1="-83.82" y1="10.16" x2="-73.66" y2="0" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="0" x2="-71.12" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="U$33" gate="G$1" pin="4DOUT"/>
<wire x1="-109.22" y1="0" x2="-119.38" y2="10.16" width="0.1524" layer="91"/>
<pinref part="U$34" gate="G$1" pin="2DIN"/>
<wire x1="-119.38" y1="10.16" x2="-124.46" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="U$35" gate="G$1" pin="2DIN"/>
<junction x="-157.48" y="10.16"/>
<pinref part="U$34" gate="G$1" pin="4DOUT"/>
<wire x1="-149.86" y1="0" x2="-157.48" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="7.62" x2="-157.48" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="U$36" gate="G$1" pin="2DIN"/>
<pinref part="U$35" gate="G$1" pin="4DOUT"/>
<wire x1="-195.58" y1="10.16" x2="-185.42" y2="0" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="0" x2="-182.88" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="U$37" gate="G$1" pin="2DIN"/>
<pinref part="U$36" gate="G$1" pin="4DOUT"/>
<wire x1="-231.14" y1="10.16" x2="-220.98" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="U$38" gate="G$1" pin="2DIN"/>
<pinref part="U$37" gate="G$1" pin="4DOUT"/>
<wire x1="-269.24" y1="10.16" x2="-259.08" y2="0" width="0.1524" layer="91"/>
<wire x1="-259.08" y1="0" x2="-256.54" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="U$39" gate="G$1" pin="2DIN"/>
<pinref part="U$38" gate="G$1" pin="4DOUT"/>
<wire x1="-307.34" y1="10.16" x2="-297.18" y2="0" width="0.1524" layer="91"/>
<wire x1="-297.18" y1="0" x2="-294.64" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="U$25" gate="G$1" pin="4DOUT"/>
<pinref part="U$26" gate="G$1" pin="2DIN"/>
<wire x1="137.16" y1="33.02" x2="142.24" y2="33.02" width="0.1524" layer="91"/>
<wire x1="142.24" y1="33.02" x2="142.24" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="U$40" gate="G$1" pin="4DOUT"/>
<wire x1="-307.34" y1="-12.7" x2="-297.18" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="U$41" gate="G$1" pin="2DIN"/>
<wire x1="-297.18" y1="-22.86" x2="-292.1" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="U$42" gate="G$1" pin="2DIN"/>
<junction x="-259.08" y="-22.86"/>
<pinref part="U$41" gate="G$1" pin="4DOUT"/>
<wire x1="-266.7" y1="-12.7" x2="-259.08" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-259.08" y1="-20.32" x2="-259.08" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="U$43" gate="G$1" pin="2DIN"/>
<pinref part="U$42" gate="G$1" pin="4DOUT"/>
<wire x1="-220.98" y1="-22.86" x2="-231.14" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-231.14" y1="-12.7" x2="-233.68" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="U$44" gate="G$1" pin="2DIN"/>
<pinref part="U$43" gate="G$1" pin="4DOUT"/>
<wire x1="-185.42" y1="-22.86" x2="-195.58" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="U$45" gate="G$1" pin="2DIN"/>
<pinref part="U$44" gate="G$1" pin="4DOUT"/>
<wire x1="-147.32" y1="-22.86" x2="-157.48" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="-12.7" x2="-160.02" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="U$46" gate="G$1" pin="2DIN"/>
<pinref part="U$45" gate="G$1" pin="4DOUT"/>
<wire x1="-109.22" y1="-22.86" x2="-119.38" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-119.38" y1="-12.7" x2="-121.92" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="U$39" gate="G$1" pin="4DOUT"/>
<wire x1="-332.74" y1="0" x2="-340.36" y2="0" width="0.1524" layer="91"/>
<wire x1="-340.36" y1="0" x2="-340.36" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="U$40" gate="G$1" pin="2DIN"/>
<wire x1="-340.36" y1="-22.86" x2="-332.74" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="U$46" gate="G$1" pin="4DOUT"/>
<pinref part="U$47" gate="G$1" pin="2DIN"/>
<wire x1="-83.82" y1="-12.7" x2="-73.66" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="-22.86" x2="-71.12" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
